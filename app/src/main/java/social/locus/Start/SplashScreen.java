package social.locus.Start;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.AnimationDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;

import social.locus.Maps;
import social.locus.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class SplashScreen extends AppCompatActivity {

    private static int SPLASH_TIME_OUT = 3000;
    private FirebaseAuth mAuth;
    FirebaseUser currentUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        overridePendingTransition(0, 0);
        mAuth = FirebaseAuth.getInstance();

        CardView logo_cardview = findViewById(R.id.logo_cardview);

        if(isConnectedToInternet()) {

            ObjectAnimator animator = ObjectAnimator.ofFloat(logo_cardview, "Elevation", pxFromDp(20), pxFromDp(6));
            animator.setDuration(3000);
            animator.start();

//            ObjectAnimator animator = ObjectAnimator.ofFloat(logo_cardview, "Elevation", pxFromDp(20), pxFromDp(5));
//            animator.setDuration(3000);
//            animator.start();

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {

                    try {
                        currentUser = mAuth.getCurrentUser();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (currentUser != null) {
                        SharedPreferences sharedPreferences = getSharedPreferences("my_number_pref",MODE_PRIVATE);
                    String contact = sharedPreferences.getString("my_number", "");

                    if (contact == null || contact.equals("")) {
                        Intent usernameIntent = new Intent(SplashScreen.this, usernameActivity.class);
                        Log.i("INTENT","to the usernameActivity");
                        usernameIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(usernameIntent);
                        overridePendingTransition(0, 0);
                    }
                    else{
//                        Toast.makeText(SplashScreen.this, username, Toast.LENGTH_SHORT).show();
                            Intent mapsIntent = new Intent(SplashScreen.this, Maps.class);
                            Log.i("INTENT", "to the map");
                            mapsIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(mapsIntent);
                            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                        }
                    } else {
                        Intent phoneIntent = new Intent(SplashScreen.this, PhoneVerificationActivity.class);
                        Log.i("INTENT", "to phoneverificationactivity");
                        phoneIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(phoneIntent);
                        overridePendingTransition(0, 0);
                    }

                    finish();
                }
            }, SPLASH_TIME_OUT);
        }

    }

    boolean isConnectedToInternet() {

        ConnectivityManager cm =
                (ConnectivityManager)SplashScreen.this.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();

        return isConnected;

    }

    public Boolean isOnline() {
        try {
            Process p1 = java.lang.Runtime.getRuntime().exec("ping -c 1 www.google.com");
            int returnVal = p1.waitFor();
            boolean reachable = (returnVal==0);
            return reachable;
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return false;
    }

    private float pxFromDp(float dp)
    {
        return dp * getResources().getDisplayMetrics().density;
    }

}