package social.locus.Start;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import social.locus.R;

public class bioActivity extends AppCompatActivity {

    String name;
    TextView head;
    EditText bioE;
    ImageView next;
    private String username;
    private String contact;

    public void nextScreen(){

        String bio = bioE.getText().toString();
        //Toast.makeText(this, ""+name, Toast.LENGTH_SHORT).show();
        if(!(bio.matches(""))) {
            Intent intent = new Intent(getBaseContext(), DOBActivity.class);
            intent.putExtra("NAME_STRING", name);
            intent.putExtra("BIO_STRING", bio);
            intent.putExtra("username", username);
            intent.putExtra("contact", contact);
            startActivity(intent);
        }
        else{
            Toast.makeText(this, "Please enter valid bio!", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bio);
        name = getIntent().getStringExtra("NAME_STRING");
        username = getIntent().getStringExtra("username");
        contact = getIntent().getStringExtra("contact");
        //Toast.makeText(this, ""+name, Toast.LENGTH_SHORT).show();
        head = findViewById(R.id.tview);
        head.setText("Hi "+name+" \uD83D\uDC4B Tell us something about yourself..");
        next = findViewById(R.id.nextB);
        bioE=findViewById(R.id.bioET);
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nextScreen();
            }
        });
    }
}
