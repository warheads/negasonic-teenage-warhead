package social.locus.Start;

import android.app.DatePickerDialog;
import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import social.locus.R;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class DOBActivity extends AppCompatActivity {

    String name;
    String bio;
    TextView dateE;
    ImageView next;
    Calendar myCalendar = Calendar.getInstance();
    private String username;
    private String contact;

    public void nextScreen(){

        String dob = dateE.getText().toString();
        if(!(dob.matches(""))){
//        Log.i("MSGGdob",dob);
        //Toast.makeText(this, ""+name, Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(getBaseContext(), profilePicActivity.class);
        intent.putExtra("NAME_STRING", name);
        intent.putExtra("BIO_STRING",bio);
        intent.putExtra("DOB_STRING",dob);
        intent.putExtra("username", username);
        intent.putExtra("contact", contact);
        startActivity(intent);}
        else{
            Toast.makeText(this, "Please enter valid Birthday!", Toast.LENGTH_SHORT).show();
        }

    }

    private void updateLabel() {
        String myFormat = "dd-MMM-yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        dateE.setText(sdf.format(myCalendar.getTime()));
    }

    DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            // TODO Auto-generated method stub
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            updateLabel();
        }

    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dob);
        name = getIntent().getStringExtra("NAME_STRING");
        bio = getIntent().getStringExtra("BIO_STRING");
        username = getIntent().getStringExtra("username");
        contact = getIntent().getStringExtra("contact");
        dateE= findViewById(R.id.dobET);
        dateE.setInputType(InputType.TYPE_NULL);


//        dateE.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View view, MotionEvent motionEvent) {
//
//                return true;
//            }
//        });

        dateE.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Toast.makeText(DOBActivity.this, "dabaaaaa", Toast.LENGTH_SHORT).show();

                DatePickerDialog datePickerDialog = new DatePickerDialog(DOBActivity.this,R.style.DatePickerTheme, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH));

                long now = System.currentTimeMillis();
                double year_15 = 473354280000.0;
                datePickerDialog.getDatePicker().setMaxDate(now - (long)year_15);
                datePickerDialog.show();

            }
        });



        next = findViewById(R.id.nextB);
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nextScreen();
            }
        });

    }
}
