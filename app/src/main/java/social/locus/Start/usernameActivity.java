package social.locus.Start;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import social.locus.R;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

public class usernameActivity extends AppCompatActivity {


    private String contact;
    private EditText usernameo;
    ImageView next;
    TextView username_avail_info;
    private String id;
    private DatabaseReference user_list;
    private ProgressDialog username_search_dialog;

    public void nextScreen(){

        String username = usernameo.getText().toString();
        Intent intent = new Intent(getBaseContext(), NameActivity.class);
        intent.putExtra("username", username);
        intent.putExtra("contact", contact);
        startActivity(intent);
//        SharedPreferences sharedPreferences = getSharedPreferences("username",MODE_PRIVATE);
//        SharedPreferences.Editor username_edit = sharedPreferences.edit();
//        if (!(username.matches(""))){
//            username_edit.putString("username",username);
//            username_edit.commit();
//        }
//        else{
//            Toast.makeText(this, "Please enter valid username!", Toast.LENGTH_SHORT).show();
//        }
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_username);
        user_list = FirebaseDatabase.getInstance().getReference("User");
        contact = getIntent().getStringExtra("contact");
        usernameo = findViewById(R.id.username_edit_text);
        next = findViewById(R.id.nextB);
        username_avail_info = findViewById(R.id.username_avail_info);
        username_search_dialog = new ProgressDialog(this);
        username_search_dialog.setMessage("Checking for username...");

        id = FirebaseAuth.getInstance().getCurrentUser().getUid();

        usernameo.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                username_avail_info.setVisibility(View.GONE);

            }
        });

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String username_text = usernameo.getText().toString();
                username_search_dialog.show();

//                next.setVisibility(View.GONE);
                if (!username_text.matches("")) {
                    Query query = user_list.orderByChild("username").equalTo(username_text);
                    query.addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot snapshot) {
                            if (snapshot.exists()) {
//                                usernameo.setTextColor(getResources().getColor(R.color.text_gray));
//                                usernameo.setTypeface(null, Typeface.ITALIC);
                                username_search_dialog.dismiss();
                                username_avail_info.setVisibility(View.VISIBLE);
                               // Toast.makeText(usernameActivity.this, "Not Available.", Toast.LENGTH_SHORT).show();
                            } else {
//                                usernameo.setTextColor(getResources().getColor(R.color.black));
//                                usernameo.setTypeface(null, Typeface.NORMAL);
//                                username_avail_info.setVisibility(View.GONE);
//                                Toast.makeText(usernameActivity.this, username_text+" not exists", Toast.LENGTH_SHORT).show();
                                username_search_dialog.dismiss();
                                nextScreen();
                            }

                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError error) {

                        }
                    });
                }
                else{
                    Toast.makeText(usernameActivity.this, "Username should not be empty!", Toast.LENGTH_SHORT).show();
                }

//                nextScreen();
            }
        });
    }
}