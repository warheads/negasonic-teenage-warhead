package social.locus.Start;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.provider.MediaStore;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import social.locus.BuildConfig;
import social.locus.Maps;
import social.locus.R;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;

import id.zelory.compressor.Compressor;

public class profilePicActivity extends AppCompatActivity {

    String name;
    String bio;
    String birthday;
    String id;
    String locstring = "N/A";
    ImageView pp;
    Uri selectedImage;
    private StorageReference mStorageReference;
    ProgressDialog progressBar,progressBar_locality;
    private String username;
    private String contact;
    private long ac_creat_time;
    LinearLayout fetch_locality_layout;
    TextView locality_textView;
    Button doneButton;

    private static final String TAG = Maps.class.getSimpleName();

    private static final int REQUEST_PERMISSIONS_REQUEST_CODE = 34;

    private FusedLocationProviderClient mFusedLocationClient;

    protected Location mLastLocation;

    public void getphoto() {
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, 1);
    }

    private String getLocality(Double lat, Double lon) {
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        String locality="N/A";
        try {
            List<Address> addresses = geocoder.getFromLocation(lat, lon, 1);
            if(addresses.size() > 0) {
                Address obj = addresses.get(0);
                locality = obj.getLocality();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
            return  locality;
    }

    @SuppressWarnings("MissingPermission")
    private void getLastLocation() {
        mFusedLocationClient.getLastLocation()
                .addOnCompleteListener(this, new OnCompleteListener<Location>() {
                    @Override
                    public void onComplete(@NonNull Task<Location> task) {
                        if (task.isSuccessful() && task.getResult() != null) {
                            mLastLocation = task.getResult();
                            locstring=getLocality(mLastLocation.getLatitude(),mLastLocation.getLongitude());
                            if(locstring.matches("N/A")) {
                                Toast.makeText(profilePicActivity.this, "Locality not found. Try setting it later again!", Toast.LENGTH_SHORT).show();
                            }
                            locality_textView.setText(locstring);
                            progressBar_locality.dismiss();
                            doneButton.setVisibility(View.VISIBLE);


                        } else {
                            Log.w(TAG, "getLastLocation:exception", task.getException());
                            showSnackbar("no_location_detected");
                            locality_textView.setText("N/A");
                            progressBar_locality.dismiss();
                        }
                    }
                });
    }

    private void showSnackbar(final String text) {
        View container = findViewById(R.id.profile_layout);
        if (container != null) {
            Snackbar.make(container, text, Snackbar.LENGTH_LONG).setActionTextColor(Color.WHITE).show();
        }
    }

    private void showSnackbar(final int mainTextStringId, final int actionStringId,
                              View.OnClickListener listener) {
        Snackbar.make(findViewById(android.R.id.content),
                getString(mainTextStringId),
                Snackbar.LENGTH_INDEFINITE)
                .setActionTextColor(Color.WHITE)
                .setAction(getString(actionStringId), listener).show();
    }

    private boolean checkPermissions() {
        int permissionState = ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION);
        return permissionState == PackageManager.PERMISSION_GRANTED;
    }

    private void startLocationPermissionRequest() {
        ActivityCompat.requestPermissions(profilePicActivity.this,
                new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                REQUEST_PERMISSIONS_REQUEST_CODE);
    }

    private void requestPermissions() {
        boolean shouldProvideRationale =
                ActivityCompat.shouldShowRequestPermissionRationale(this,
                        Manifest.permission.ACCESS_FINE_LOCATION);

        // Provide an additional rationale to the user. This would happen if the user denied the
        // request previously, but didn't check the "Don't ask again" checkbox.
        if (shouldProvideRationale) {
            Log.i(TAG, "Displaying permission rationale to provide additional context.");

            showSnackbar(R.string.permission_rationale, android.R.string.ok,
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            // Request permission
                            startLocationPermissionRequest();
                        }
                    });

        } else {
            Log.i(TAG, "Requesting permission");
            // Request permission. It's possible this can be auto answered if device policy
            // sets the permission in a given state or the user denied the permission
            // previously and checked "Never ask again".
            startLocationPermissionRequest();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Log.i(TAG, "onRequestPermissionResult");
        if (requestCode == REQUEST_PERMISSIONS_REQUEST_CODE) {
            if (grantResults.length <= 0) {
                // If user interaction was interrupted, the permission request is cancelled and you
                // receive empty arrays.
                Log.i(TAG, "User interaction was cancelled.");
            } else if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permission granted.
                getLastLocation();
            } else {
                // Permission denied.

                // Notify the user via a SnackBar that they have rejected a core permission for the
                // app, which makes the Activity useless. In a real app, core permissions would
                // typically be best requested during a welcome-screen flow.

                // Additionally, it is important to remember that a permission might have been
                // rejected without asking the user for permission (device policy or "Never ask
                // again" prompts). Therefore, a user interface affordance is typically implemented
                // when permissions are denied. Otherwise, your app could appear unresponsive to
                // touches or interactions which have required permissions.
                showSnackbar(R.string.permission_denied_explanation, R.string.settings,
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                // Build intent that displays the App settings screen.
                                Intent intent = new Intent();
                                intent.setAction(
                                        Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                Uri uri = Uri.fromParts("package",
                                        BuildConfig.APPLICATION_ID, null);
                                intent.setData(uri);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                            }
                        });
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        if (requestCode == 1 && resultCode == RESULT_OK && data != null) {

            selectedImage = data.getData();
            CropImage.activity(selectedImage)
                    .setAspectRatio(1, 1)
                    .start(this);

        }

        Uri cropped_image= null;

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                 cropped_image = result.getUri();
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }

            final File thumb_filePath = new File(cropped_image.getPath());

            Bitmap thumb_bitmap = new Compressor(this)
                    .setMaxWidth(200)
                    .setMaxHeight(200)
                    .setQuality(75)
                    .compressToBitmap(thumb_filePath);

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            thumb_bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            final byte[] thumb_byte = baos.toByteArray();


             Bitmap bitmap=null;
            try {
                bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(),cropped_image);
            } catch (IOException e) {
                e.printStackTrace();
            }

            progressBar.setTitle("Setting Profile Picture");
            progressBar.setMessage("Upload in progress");
            progressBar.setCanceledOnTouchOutside(false);
            progressBar.show();

            final StorageReference filepath = mStorageReference.child(id).child(cropped_image.getLastPathSegment());
            final StorageReference thumb_filepath = mStorageReference.child("profile_images").child("thumbs").child(id + ".jpg");

            final Bitmap finalBitmap = bitmap;

            filepath.putFile(cropped_image).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(final UploadTask.TaskSnapshot taskSnapshot) {

                    filepath.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                        @Override
                        public void onSuccess(final Uri uri) {
                            final String download_url = uri.toString();

                            thumb_filepath.putBytes(thumb_byte).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
                                @Override
                                public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {
                                    if(task.isSuccessful()) {
                                        thumb_filepath.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                            @Override
                                            public void onSuccess(Uri uri) {
                                                String thumb_downloadUrl = uri.toString();

                                                FirebaseDatabase.getInstance().getReference("User").child(id).child("profilePicURL").setValue(download_url);
                                                FirebaseDatabase.getInstance().getReference("User").child(id).child("thumb_image").setValue(thumb_downloadUrl);

                                                pp.setImageBitmap(finalBitmap);

                                                Picasso.with(getApplication()).load(uri.toString()).placeholder(R.drawable.ic_dp_icon_male_04).into(pp);
                                                progressBar.dismiss();
                                            }
                                        });
                                    }
                                }
                            });
//                            Toast.makeText(profilePicActivity.this, "Upload Complete..", Toast.LENGTH_SHORT).show();

                        }
                    });

                }
            });
        }
    }

    public void start(View view) {

        if(username.matches("" ) || name.matches("") || birthday.matches("") || bio.matches("") || locstring.matches("")) {

            Toast.makeText(this, "Profile SetUp Failed! Please Retry!", Toast.LENGTH_SHORT).show();

        }
        else
        {
            SharedPreferences sharedPreferences = getSharedPreferences("my_number_pref",MODE_PRIVATE);
            SharedPreferences.Editor contact_edit = sharedPreferences.edit();
            contact_edit.putString("my_number",contact);
            contact_edit.commit();
            FirebaseDatabase.getInstance().getReference("Customs").child(contact).setValue(id);
            FirebaseDatabase.getInstance().getReference("User").child(id).child("name").setValue(name);
            FirebaseDatabase.getInstance().getReference("User").child(id).child("birthday").setValue(birthday);
            FirebaseDatabase.getInstance().getReference("User").child(id).child("bio").setValue(bio);
            FirebaseDatabase.getInstance().getReference("User").child(id).child("Location").setValue(locstring);
            FirebaseDatabase.getInstance().getReference("User").child(id).child("username").setValue(username);
            FirebaseDatabase.getInstance().getReference("User").child(id).child("contact").setValue(contact);
            FirebaseDatabase.getInstance().getReference("User").child(id).child("account_creation_time").setValue(ac_creat_time);
            FirebaseDatabase.getInstance().getReference("User").child(id).child("global_count").setValue(""+0);
            FirebaseDatabase.getInstance().getReference("User").child(id).child("self_count").setValue(""+0);
            Intent i = new Intent(getApplicationContext(), SplashScreen.class);
            finish();
            startActivity(i);

        }

    }

    private String geContactFromStorage() {
        SharedPreferences Preferences = getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        String contact = Preferences.getString("my_phone","");
        return contact;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_pic);

        ac_creat_time=System.currentTimeMillis();

        name = getIntent().getStringExtra("NAME_STRING");
        bio = getIntent().getStringExtra("BIO_STRING");
        birthday = getIntent().getStringExtra("DOB_STRING");
        username = getIntent().getStringExtra("username");
        //contact = getIntent().getStringExtra("contact");

        contact = geContactFromStorage();

        id = FirebaseAuth.getInstance().getCurrentUser().getUid();
        progressBar = new ProgressDialog(this);
        progressBar_locality = new ProgressDialog(this);
        pp= findViewById(R.id.pp);
        pp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getphoto();
            }
        });
        mStorageReference = FirebaseStorage.getInstance().getReference();

        fetch_locality_layout = findViewById(R.id.fetch_locality_layout);
        locality_textView = findViewById(R.id.fetch_locality_textView);
        doneButton = findViewById(R.id.doneButton);

        fetch_locality_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                progressBar_locality.setTitle("Searching for locality");
                progressBar_locality.setCanceledOnTouchOutside(false);
                progressBar_locality.show();

                mFusedLocationClient = LocationServices.getFusedLocationProviderClient(profilePicActivity.this);

                if (!checkPermissions()) {
                    requestPermissions();
                } else {
                    getLastLocation();
                }
            }
        });

    }

}
