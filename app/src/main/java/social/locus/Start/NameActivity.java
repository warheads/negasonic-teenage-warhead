package social.locus.Start;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import social.locus.R;

public class NameActivity extends AppCompatActivity {

    ImageView next;
    EditText nameE;
    public static String username;
    public static String contact;
    public static int xp=0;

    public void nextScreen(){

        String name = nameE.getText().toString();
        //Toast.makeText(this, ""+name, Toast.LENGTH_SHORT).show();
        if(!(name.matches(""))){
        Intent intent = new Intent(getBaseContext(), bioActivity.class);
        intent.putExtra("NAME_STRING", name);
        intent.putExtra("username", username);
        intent.putExtra("contact", contact);
        startActivity(intent);
        }
        else{
            Toast.makeText(this, "Please enter valid Name!", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_name);
        username = getIntent().getStringExtra("username");
        contact = getIntent().getStringExtra("contact");
        Log.i("VALUES",username+"\t"+contact);
        nameE = findViewById(R.id.nameET);
        next = findViewById(R.id.nextB);
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nextScreen();
            }
        });
    }
}
