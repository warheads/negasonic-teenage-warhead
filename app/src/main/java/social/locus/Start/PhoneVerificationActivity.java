package social.locus.Start;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.swiperefreshlayout.widget.CircularProgressDrawable;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.hbb20.CountryCodePicker;
import social.locus.Maps;
import social.locus.R;
import social.locus.Settings.AboutUsActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.FirebaseTooManyRequestsException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.concurrent.TimeUnit;

public class PhoneVerificationActivity extends AppCompatActivity {
    public static final String MyPREFERENCES = "MyPrefs";
    private static final String TAG = "LocusTAG";
    EditText phone_number_edit_text;
    EditText otp_edit_text_e;
    Button send_otp_button;
    Button verify_otp_button;
    private String phone_number_from_edit_text;
    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks;
    private FirebaseAuth mAuth;
    private String mVerificationId;
    private PhoneAuthProvider.ForceResendingToken mResendToken;
    private String otp_string_code;
    DatabaseReference userphonelist;
    CountryCodePicker ccp;
    SharedPreferences sharedpreferences;
    //private ProgressBar progBar;
    private ProgressDialog signin_dialog;
    private TextView countdownotp;
    private CountDownTimer countDownTimer;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phone_verification);
        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        // Initialize Firebase Auth
        mAuth = FirebaseAuth.getInstance();
        //progBar = findViewById(R.id.progressBar2);
        signin_dialog = new ProgressDialog(this);

        userphonelist = FirebaseDatabase.getInstance().getReference();
        phone_number_edit_text=findViewById(R.id.phone_number_edit_text);
        ccp = findViewById(R.id.ccp);
        otp_edit_text_e=findViewById(R.id.otp_edit_text);
        send_otp_button=findViewById(R.id.send_otp_button);
        verify_otp_button=findViewById(R.id.verify_otp_button);
        countdownotp = findViewById(R.id.countdownotp);

        TextView privacy_text = findViewById(R.id.privacy_policy_text);
        if (privacy_text != null) {

            privacy_text.setMovementMethod(LinkMovementMethod.getInstance());
        }

        countDownTimer = new CountDownTimer(59000, 1000) {

            public void onTick(long millisUntilFinished) {
                countdownotp.setVisibility(View.VISIBLE);
                countdownotp.setText(millisUntilFinished / 1000+" seconds");
            }

            public void onFinish() {
                countdownotp.setVisibility(View.GONE);
                send_otp_button.setBackground(getResources().getDrawable(R.drawable.button_round_background));
                send_otp_button.setText("Get OTP");
                send_otp_button.setTextColor(getResources().getColor(R.color.white));
                send_otp_button.setTextSize(15.0f);
                send_otp_button.setClickable(true);
            }
        };

        phone_number_edit_text.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                send_otp_button.setVisibility(View.VISIBLE);
                //progBar.setVisibility(View.INVISIBLE);
                otp_edit_text_e.setVisibility(View.INVISIBLE);
                verify_otp_button.setVisibility(View.INVISIBLE);
                countdownotp.setVisibility(View.GONE);
                countDownTimer.cancel();

            }
        });


        ccp.registerCarrierNumberEditText(phone_number_edit_text);

        ccp.setPhoneNumberValidityChangeListener(new CountryCodePicker.PhoneNumberValidityChangeListener() {
            @Override
            public void onValidityChanged(boolean isValidNumber) {
                if(!phone_number_edit_text.getText().toString().matches("")) {
                    if (isValidNumber) {
                        send_otp_button.setBackground(getResources().getDrawable(R.drawable.button_round_background));
                        send_otp_button.setText("Get OTP");
                        send_otp_button.setTextColor(getResources().getColor(R.color.white));
                        send_otp_button.setTextSize(15.0f);
                        send_otp_button.setClickable(true);
                    } else {
                        //send_otp_button.setBackgroundColor(getResources().getColor(R.color.picture_gray));
                        send_otp_button.setBackground(getResources().getDrawable(R.drawable.button_gray_background));
                        send_otp_button.setTextSize(25.0f);
                        send_otp_button.setText("....");
                        send_otp_button.setTextColor(getResources().getColor(R.color.black));
                        send_otp_button.setClickable(false);
                    }
                }
            }
        });

        send_otp_button.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {

                if (!phone_number_edit_text.getText().toString().matches("")) {
                    send_otp_button.setBackground(getResources().getDrawable(R.drawable.button_gray_background));
                    send_otp_button.setTextColor(getResources().getColor(R.color.black));
                    send_otp_button.setClickable(false);
                    countDownTimer.start();
//                phone_number_from_edit_text =phone_number_edit_text.getText().toString();
                    phone_number_from_edit_text = ccp.getFullNumberWithPlus();
                    Log.i("phone number", ccp.getFullNumberWithPlus());
                    Log.i("country", ccp.getSelectedCountryCode());

                    PhoneAuthProvider.getInstance().verifyPhoneNumber(
                            phone_number_from_edit_text,        // Phone number to verify
                            60,                 // Timeout duration
                            TimeUnit.SECONDS,   // Unit of timeout
                            PhoneVerificationActivity.this,               // Activity (for callback binding)
                            mCallbacks);        // OnVerificationStateChangedCallbacks
                    //progBar.setVisibility(View.VISIBLE);
                }
            }
        });

        verify_otp_button.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                otp_string_code=otp_edit_text_e.getText().toString();
                //Toast.makeText(PhoneVerificationActivity.this, otp_string_code, Toast.LENGTH_SHORT).show();
                if(!otp_string_code.matches("")) {
                    hideKeyboard(PhoneVerificationActivity.this);
                    signin_dialog.setTitle("Signing In...");
                    signin_dialog.setCanceledOnTouchOutside(false);
                    signin_dialog.show();
                    signInWithPhoneAuthCredential(PhoneAuthProvider.getCredential(mVerificationId, otp_string_code));
                }

            }
        });
        mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {

            @Override
            public void onVerificationCompleted(PhoneAuthCredential credential) {
                // This callback will be invoked in two situations:
                // 1 - Instant verification. In some cases the phone number can be instantly
                //     verified without needing to send or enter a verification code.
                // 2 - Auto-retrieval. On some devices Google Play services can automatically
                //     detect the incoming verification SMS and perform verification without
                //     user action.
                otp_edit_text_e.setText(credential.getSmsCode());
                Log.d(TAG, "onVerificationCompleted:" + credential);
//                otp_edit_text_e.setText(credential.toString());
                signInWithPhoneAuthCredential(credential);
            }

            @Override
            public void onVerificationFailed(FirebaseException e) {
                // This callback is invoked in an invalid request for verification is made,
                // for instance if the the phone number format is not valid.
                Log.w(TAG, "onVerificationFailed", e);

                if (e instanceof FirebaseAuthInvalidCredentialsException) {
                    // Invalid request
                    // ...
                } else if (e instanceof FirebaseTooManyRequestsException) {
                    // The SMS quota for the project has been exceeded
                    // ...
                }

                // Show a message and update the UI
                // ...
            }

            @Override
            public void onCodeSent(@NonNull String verificationId,
                                   @NonNull PhoneAuthProvider.ForceResendingToken token) {
                // The SMS verification code has been sent to the provided phone number, we
                // now need to ask the user to enter the code and then construct a credential
                // by combining the code with a verification ID.
                //progBar.setVisibility(View.GONE);

                send_otp_button.setVisibility(View.INVISIBLE);
                verify_otp_button.setVisibility(View.VISIBLE);
                countdownotp.setVisibility(View.GONE);
                countDownTimer.cancel();

//              Log.d(TAG, "onCodeSent:" + verificationId);
                Toast.makeText(PhoneVerificationActivity.this, "Code Sent", Toast.LENGTH_SHORT).show();
                verify_otp_button.setClickable(true);
                otp_edit_text_e.setVisibility(View.VISIBLE);
                otp_edit_text_e.requestFocus();
                
                // Save verification ID and resending token so we can use them later
                mVerificationId = verificationId;
                mResendToken = token;
            }
        };
    }

    @Override
    protected void onStart() {
        super.onStart();
        FirebaseUser currentUser = mAuth.getCurrentUser();
        if(currentUser != null){
            Intent i = new Intent(PhoneVerificationActivity.this, Maps.class);
            Log.i("INTENT","to splashscreen");
            startActivity(i);
        }
//        updateUI(currentUser);
    }

    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential) {
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithCredential:success");
                            hideKeyboard(PhoneVerificationActivity.this);

                            sharedpreferences.edit().putString("my_phone",phone_number_from_edit_text).apply();
                            sharedpreferences.edit().putString("my_phone_country_code",ccp.getSelectedCountryCode()).apply();

                            DatabaseReference userNameRef = userphonelist.child("Customs").child(phone_number_from_edit_text);
                            ValueEventListener eventListener = new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    if(!dataSnapshot.exists()) {
                                        //create new user
                                        String curr_id = FirebaseAuth.getInstance().getCurrentUser().getUid();
                                        if (curr_id!=null) {
                                            //userphonelist.child("Customs").child(phone_number_from_edit_text).setValue(curr_id);
                                            Intent i = new Intent(PhoneVerificationActivity.this,usernameActivity.class);
                                            i.putExtra("contact", phone_number_from_edit_text);
                                            startActivity(i);
                                        }
                                        else{
                                            Toast.makeText(PhoneVerificationActivity.this, "Registration not complete! Try Again", Toast.LENGTH_SHORT).show();
                                        }

                                    }
                                    else
                                    {
                                        SharedPreferences sharedPreferences = getSharedPreferences("my_number_pref",MODE_PRIVATE);
                                        SharedPreferences.Editor contact_edit = sharedPreferences.edit();
                                        contact_edit.putString("my_number",phone_number_from_edit_text);
                                        contact_edit.commit();
                                        Intent j = new Intent(PhoneVerificationActivity.this,Maps.class);
                                        startActivity(j);
                                    }
                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {
                                    Log.d(TAG, databaseError.getMessage()); //Don't ignore errors!
                                }
                            };
                            userNameRef.addListenerForSingleValueEvent(eventListener);
                            FirebaseDatabase.getInstance().getReference("versions")
                                    .child(FirebaseAuth.getInstance().getCurrentUser().getUid()).setValue("10").addOnSuccessListener(new OnSuccessListener<Void>() {
                                @Override
                                public void onSuccess(Void unused) {
                                    userNameRef.addListenerForSingleValueEvent(eventListener);
                                }
                            });
                            //signin_dialog.dismiss();
                            Toast.makeText(PhoneVerificationActivity.this, "SignIn success", Toast.LENGTH_SHORT).show();
                            verify_otp_button.setClickable(false);
                            // ...
                        } else {
                            // Sign in failed, display a message and update the UI
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                            signin_dialog.dismiss();
                            Toast.makeText(PhoneVerificationActivity.this, "SignIn failed", Toast.LENGTH_SHORT).show();
                            if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                                // The verification code entered was invalid
                                Toast.makeText(PhoneVerificationActivity.this, "Invalid Code", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                });
    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

}