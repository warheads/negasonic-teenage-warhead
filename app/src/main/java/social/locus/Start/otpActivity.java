package social.locus.Start;

import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import social.locus.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;

import java.util.concurrent.TimeUnit;

public class otpActivity extends AppCompatActivity {

    EditText phno,otp;
    PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallback;
    String verification_code;
    FirebaseAuth oAuth;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp);

        phno = findViewById(R.id.phnoEditText);
        otp = findViewById(R.id.otpEditText);

        Bundle bundle = getIntent().getExtras();
        String contact = bundle.getString("contact");

        phno.setText(contact);

        //phone number verification

        oAuth = FirebaseAuth.getInstance();mCallback = new PhoneAuthProvider.OnVerificationStateChangedCallbacks(){

            @Override
            public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {

            }

            @Override
            public void onVerificationFailed(FirebaseException e) {

            }

            @Override
            public void onCodeSent(String s, PhoneAuthProvider.ForceResendingToken forceResendingToken) {
                super.onCodeSent(s, forceResendingToken);
                verification_code = s;
                Toast.makeText(getApplicationContext(),"Code sent to the number", Toast.LENGTH_SHORT).show();
            }
        };
    }

    public void send_sms(View v)
    {
        String number = phno.getText().toString();
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                number,60, TimeUnit.SECONDS,this,mCallback
        );
    }

    public void signInWithPhone(PhoneAuthCredential credential)
    {
        oAuth.signInWithCredential(credential).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    // Sign in success, update UI with the signed-in user's information
                    Toast.makeText(getApplicationContext(), "Signup successful!!", Toast.LENGTH_LONG).show();
                    //pref.edit().putBoolean("loggedin",true).apply();
                    Intent i = new Intent(getApplicationContext(),NameActivity.class);
                    startActivity(i);

                } else {
                    // If sign in fails, display a message to the user.
                    Log.w("Msg", "createUserWithEmail:failure", task.getException());
                    Toast.makeText(getApplicationContext(), "Try again!!", Toast.LENGTH_LONG).show();

                }
            }
        });
    }

    public void verify(View v)
    {
        String input_code = otp.getText().toString();
        if(verification_code!=null)
        {
            PhoneAuthCredential credential = PhoneAuthProvider.getCredential(verification_code,input_code);
            signInWithPhone(credential);
        }
    }
}
