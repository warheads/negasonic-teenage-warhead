package social.locus.Chat;

import android.app.Activity;
import android.app.Dialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.PopupMenu;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.security.Key;
import java.security.KeyFactory;
import java.security.spec.KeySpec;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import social.locus.APIService;
import social.locus.Camera.CameraCaptureActivity;
import social.locus.Maps;
import social.locus.Notifications.Client;
import social.locus.Notifications.Data;
import social.locus.Notifications.MyResponse;
import social.locus.Notifications.Sender;
import social.locus.Notifications.Token;
import social.locus.R;

public class ChatActivity extends AppCompatActivity {

    private String mChatUser;
    private Toolbar mChatToolbar;

    private DatabaseReference mRootRef, seen_reference1,seen_reference2 ;

    //private TextView mTitleView;
    private TextView mLastSeenView;
    private CircleImageView mProfileImage;
    private FirebaseAuth mAuth;
    private String mCurrentUserId;

    int seen_flag=0;

    ProgressDialog imageLoading;

    TextView sending;
    private Button mChatSendBtn;
    private EditText mChatMessageView;

    private RecyclerView mMessagesList;
    private SwipeRefreshLayout mRefreshLayout;

    private final List<Messages> messagesList = new ArrayList<>();
    private LinearLayoutManager mLinearLayout;
    private MessageAdapter mAdapter;

    private static final int TOTAL_ITEMS_TO_LOAD = 50;
    private int mCurrentPage = 1;

    private static final int GALLERY_PICK = 1;

    DatabaseReference reference,reference2;
    ValueEventListener seenlistener,seenlistener2;


    // Storage Firebase
    private StorageReference mImageStorage;

    //ImageView onScreen_icon;
    //New Solution
    private int itemPos = 0;
    Bitmap profile_bitmap;

    private String mLastKey = "";
    private String mPrevKey = "";
    String userName;
    Boolean notify = false;
    APIService apiService;
    View view;
    String thumb_string, profile_string,open_from;

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        hideKeyboard(ChatActivity.this);
        if(open_from.equals("maps")) {
            finish();
        } else if(open_from.equals("connections")) {
            finish();
        }
        else {
            finish();
            startActivity(new Intent(ChatActivity.this, Maps.class));
        }
        //startActivity(new Intent(ChatActivity.this, Maps.class));
    }

    @Override
    protected void onResume() {
        super.onResume();
        currentUser(mChatUser);
        FirebaseUser currentuser = FirebaseAuth.getInstance().getCurrentUser();
        seen_flag = 1;
        clearNotifs("",mChatUser);
        if(currentuser != null){

            FirebaseDatabase.getInstance().getReference("User").child(mCurrentUserId).child("chatUser").setValue(mChatUser);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        reference.removeEventListener(seenlistener);
        reference2.removeEventListener(seenlistener2);
        currentUser("none");

        FirebaseUser currentuser = FirebaseAuth.getInstance().getCurrentUser();
        seen_flag = 0;
        if(currentuser != null) {
            FirebaseDatabase.getInstance().getReference("User").child(mCurrentUserId).child("chatUser").setValue("none");
        }
    }

    private void clearNotifs(String text, String user_id) {
        SharedPreferences notifPreferences = getSharedPreferences("NotifPrefs",MODE_PRIVATE);
        SharedPreferences.Editor myEdit = notifPreferences.edit();
        myEdit.putString(user_id, text);
        myEdit.commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate( R.menu.menu_main, menu );
        thumb_string = getIntent().getStringExtra("userprofile_thumb");
        profile_string = getIntent().getStringExtra("userprofile");
       // profile_bitmap = StringToBitMap(profile_string);

        Picasso.with(this)
                    .load(thumb_string)
                    .placeholder(R.drawable.ic_dp_icon_male_04)
                    .into(new Target() {
                        @Override
                        public void onBitmapLoaded ( Bitmap bitmap, Picasso.LoadedFrom from){
                            bitmap = getCroppedBitmap(bitmap);
                            menu.findItem(R.id.profile_menu).setIcon(new BitmapDrawable(getResources(), bitmap));
                            Log.i("NOTIFY BITMAP","BITMAP MADE");
                        }

                        @Override
                        public void onBitmapFailed(Drawable errorDrawable) {
                            Log.e("NOTIFY BITMAP","BITMAP FAILED");
                        }

                        @Override
                        public void onPrepareLoad(Drawable placeHolderDrawable) {

                        }
                    });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        if(id == R.id.profile_menu) {
           // Toast.makeText(ChatActivity.this, profile_string, Toast.LENGTH_SHORT).show();
            showImage(profile_string, thumb_string);
        }
        if(mChatUser != null) {

            if (id == R.id.drop_menu) {
                // Toast.makeText(ChatActivity.this, profile_string, Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(ChatActivity.this, CameraCaptureActivity.class);
                intent.putExtra("quickID", mChatUser);
                startActivity(intent);
                overridePendingTransition(0, 0);
            }
        }

        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        view = findViewById(android.R.id.content);

        seen_flag = 1;

        imageLoading = new ProgressDialog(this);
        sending = findViewById(R.id.sending);
        mChatUser = getIntent().getStringExtra("user_id");
        String receiver_username = getIntent().getStringExtra("username");
        profile_string = getIntent().getStringExtra("userprofile");
        open_from = getIntent().getStringExtra("open_from");

        clearNotifs("",mChatUser);

        mRootRef = FirebaseDatabase.getInstance().getReference();

        apiService = Client.getClient("https://fcm.googleapis.com/").create(APIService.class);

        NotificationManager notificationManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancel(mChatUser.hashCode());

        mChatToolbar = findViewById(R.id.chat_bar);
        setSupportActionBar(mChatToolbar);
        mChatToolbar.setTitle(receiver_username);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back_arrow);
        //getSupportActionBar().setLogo(R.drawable.ic_dp_icon_male_04);
        //getSupportActionBar().setDisplayUseLogoEnabled(true);
        mChatToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(open_from.equals("maps")) {
                    finish();
                } else if(open_from.equals("connections")) {
                    finish();
                }
                else {
                    finish();
                    startActivity(new Intent(ChatActivity.this, Maps.class));
                }

                //startActivity(new Intent(ChatActivity.this, Maps.class));
            }
        });

//        setSupportActionBar(mChatToolbar);
//        ActionBar actionBar = getSupportActionBar();
//        actionBar.setDisplayShowCustomEnabled(true);
//        actionBar.setDisplayShowTitleEnabled(false);
//
//        LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//        View action_bar_view = inflater.inflate(R.layout.chat_custom_bar, null);
//        actionBar.setCustomView(action_bar_view);

        //onScreen_icon = findViewById(R.id.online_icon);
        //mTitleView = findViewById(R.id.custom_bar_title);

//        ImageView add = findViewById(R.id.addConv);
//        add.setImageResource(R.drawable.ic_more_vert_black_24dp);
//        add.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                showMenu(view);
//            }
//        });


        mAuth = FirebaseAuth.getInstance();
        mCurrentUserId = mAuth.getCurrentUser().getUid();
        seen_reference1 = FirebaseDatabase.getInstance().getReference("messages").child(mCurrentUserId).child(mChatUser);
        seen_reference2 = FirebaseDatabase.getInstance().getReference("messages").child(mChatUser).child(mCurrentUserId);

        currentUser(mChatUser);

        mChatSendBtn =  findViewById(R.id.chat_send_btn);
        mChatMessageView = findViewById(R.id.chat_message_view);
        mRefreshLayout = findViewById(R.id.message_swipe_layout);

        mAdapter = new MessageAdapter(messagesList,this, mChatUser, mCurrentUserId);
        mMessagesList = findViewById(R.id.messages_list);

        mLinearLayout = new LinearLayoutManager(this);
        mLinearLayout.setStackFromEnd(true);

        mMessagesList.setHasFixedSize(false);
        mMessagesList.setLayoutManager(mLinearLayout);
        mMessagesList.setAdapter(mAdapter);

        mImageStorage = FirebaseStorage.getInstance().getReference();

        mRootRef.child("Chat").child(mCurrentUserId).child(mChatUser).child("seen").setValue(true);
        mRootRef.child("Chat").child(mCurrentUserId).child(mChatUser).child("timestamp").setValue(ServerValue.TIMESTAMP);

        loadMessages();

//        mRootRef.child("User").child(mChatUser).addValueEventListener(new ValueEventListener() {
//            @Override
//            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//
//                userName = String.valueOf(dataSnapshot.child("username").getValue());
//                mTitleView.setText(userName);
//
//            }
//
//            @Override
//            public void onCancelled(@NonNull DatabaseError databaseError) {
//
//            }
//
//        });

        SharedPreferences preferences = getSharedPreferences("PREFS",MODE_PRIVATE);
        String currentUser = preferences.getString("currentuser","none");
        if(!currentUser.equals("none") && !currentUser.equals(mCurrentUserId)) {
            FirebaseDatabase.getInstance().getReference("User").child(mCurrentUserId).child("chatUser").setValue(currentUser);
        }


        mRootRef.child("User").child(mChatUser).child("chatUser").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()) {
                    String screenUser = dataSnapshot.getValue().toString();

                    if(screenUser.equals(mCurrentUserId)) {
                        //onScreen_icon.setVisibility(View.VISIBLE);
                    } else {
                        //onScreen_icon.setVisibility(View.INVISIBLE);
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


        mRootRef.child("Chat").child(mCurrentUserId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                if(!dataSnapshot.hasChild(mChatUser)){

                    Map chatAddMap = new HashMap();
                    chatAddMap.put("seen", true);
                    chatAddMap.put("timestamp", ServerValue.TIMESTAMP);

                    Map chatUserMap = new HashMap();
                    chatUserMap.put("Chat/" + mCurrentUserId + "/" + mChatUser, chatAddMap);
                    chatUserMap.put("Chat/" + mChatUser + "/" + mCurrentUserId, chatAddMap);

                    mRootRef.updateChildren(chatUserMap, new DatabaseReference.CompletionListener() {
                        @Override
                        public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {

                            if(databaseError != null){

                                Log.d("CHAT_LOG", databaseError.getMessage());

                            }

                        }
                    });

                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        mChatSendBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                notify = true;

                sendMessage(1);
                mAdapter.notifyDataSetChanged();

            }
        });

        mRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                mCurrentPage++;

                itemPos = 0;

                loadMoreMessages();


            }
        });

        seenMessage(mChatUser);
    }

    private void loadMoreMessages() {

        DatabaseReference messageRef = mRootRef.child("messages").child(mCurrentUserId).child(mChatUser);

        Query messageQuery = messageRef.orderByKey().endAt(mLastKey).limitToLast(50);

        messageQuery.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                if(dataSnapshot.exists()) {
                    Messages message = dataSnapshot.getValue(Messages.class);

//                    if(message.getFrom().equals(mChatUser) && !message.isSeen() && seen_flag==1) {
//                        //Toast.makeText(ChatActivity.this, "Setting seen true", Toast.LENGTH_SHORT).show();
//                        message.setSeen(true);
//                        seen_reference1.child(message.getMsg_id()).child("seen").setValue(true);
//                        seen_reference2.child(message.getMsg_id()).child("seen").setValue(true);
//                    }

                    String messageKey = dataSnapshot.getKey();

                    if (!mPrevKey.equals(messageKey)) {

                        messagesList.add(itemPos++, message);

                    } else {

                        mPrevKey = mLastKey;

                    }


                    if (itemPos == 1) {

                        mLastKey = messageKey;

                    }


                    Log.d("TOTALKEYS", "Last Key : " + mLastKey + " | Prev Key : " + mPrevKey + " | Message Key : " + messageKey);

                    mAdapter.notifyDataSetChanged();

                    mRefreshLayout.setRefreshing(false);

                    mLinearLayout.scrollToPosition(itemPos);
                }

                else {
                    Toast.makeText(ChatActivity.this, "No more messages.", Toast.LENGTH_SHORT).show();
                    mRefreshLayout.setRefreshing(false);

                }

            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    private void loadMessages() {

        DatabaseReference messageRef = mRootRef.child("messages").child(mCurrentUserId).child(mChatUser);
        //mRootRef.child("User").child(mCurrentUserId).child("online").setValue(true);

        Query messageQuery = messageRef.limitToLast(mCurrentPage * TOTAL_ITEMS_TO_LOAD);

        messageQuery.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                Messages message = dataSnapshot.getValue(Messages.class);

//                if(message.getFrom().equals(mCurrentUserId) && !message.isSeen()) {
//
//                    //notif_string = notif_string+" "+message.getMessage();
//                }

//                if(message.getFrom().equals(mChatUser) && !message.isSeen() && seen_flag==1) {
//                    //Toast.makeText(ChatActivity.this, "Setting seen true", Toast.LENGTH_SHORT).show();
//                    message.setSeen(true);
//                    seen_reference1.child(message.getMsg_id()).child("seen").setValue(true);
//                    seen_reference2.child(message.getMsg_id()).child("seen").setValue(true);
//                }

                itemPos++;

                if(itemPos == 1){

                    String messageKey = dataSnapshot.getKey();

                    mLastKey = messageKey;
                    mPrevKey = messageKey;

                }

                messagesList.add(message);
                mAdapter.notifyDataSetChanged();
                mMessagesList.scrollToPosition(messagesList.size() - 1);
                mRefreshLayout.setRefreshing(false);

            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                //Toast.makeText(ChatActivity.this, s, Toast.LENGTH_SHORT).show();
                //loadMessages();
                //notif_string="";
//                messagesList.clear();
//                loadMessages();
                mAdapter.notifyDataSetChanged();
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    private void sendMessage(int version) {

        final String message = mChatMessageView.getText().toString().trim();

        if(!TextUtils.isEmpty(message)){

            String current_user_ref = "messages/" + mCurrentUserId + "/" + mChatUser;
            String chat_user_ref = "messages/" + mChatUser + "/" + mCurrentUserId;

            DatabaseReference user_message_push = mRootRef.child("messages").child(mCurrentUserId).child(mChatUser).push();

            String push_id = user_message_push.getKey();
            long timemillis = System.currentTimeMillis();
            String timemillis_string = String.valueOf(timemillis);
            String encrypted_message = encrypt(message.getBytes(),mChatUser.substring(0,16).getBytes(),timemillis_string.substring(0,8).getBytes());

            Map messageMap = new HashMap();
            messageMap.put("message", encrypted_message);
            messageMap.put("seen", false);
            messageMap.put("type", "text");
            messageMap.put("thumb", "thumb");
            messageMap.put("time", timemillis);
            messageMap.put("from", mCurrentUserId);
            messageMap.put("msg_id", push_id);
            messageMap.put("version", version);


            Map messageUserMap = new HashMap();
            messageUserMap.put(current_user_ref + "/" + push_id, messageMap);
            messageUserMap.put(chat_user_ref + "/" + push_id, messageMap);

            mChatMessageView.setText("");
            sending.setVisibility(View.VISIBLE);

            mRootRef.updateChildren(messageUserMap, new DatabaseReference.CompletionListener() {
                @Override
                public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {

                    if(databaseError != null){

                        Log.d("CHAT_LOG", databaseError.getMessage());
                        Toast.makeText(ChatActivity.this, "Failed to send message!", Toast.LENGTH_SHORT).show();
                    }

                    else {
//                        MediaPlayer ring= MediaPlayer.create(ChatActivity.this,R.raw.send);
//                        ring.start();
//                        Toast.makeText(ChatActivity.this, "sent!", Toast.LENGTH_SHORT).show();
                        sending.setVisibility(View.GONE);
                    }

                }
            });

            mRootRef.child("User").child(mChatUser).child("chatUser").addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    String chatUser = String.valueOf(dataSnapshot.getValue());
                    if(chatUser.equals(mCurrentUserId)) {
                        mRootRef.child("Chat").child(mChatUser).child(mCurrentUserId).child("seen").setValue(true);
                        mRootRef.child("Chat").child(mChatUser).child(mCurrentUserId).child("timestamp").setValue(ServerValue.TIMESTAMP);
                    }
                    else {
                        mRootRef.child("Chat").child(mChatUser).child(mCurrentUserId).child("seen").setValue(false);
                        mRootRef.child("Chat").child(mChatUser).child(mCurrentUserId).child("timestamp").setValue(ServerValue.TIMESTAMP);
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });

            mRootRef.child("Chat").child(mCurrentUserId).child(mChatUser).child("seen").setValue(true);
            mRootRef.child("Chat").child(mCurrentUserId).child(mChatUser).child("timestamp").setValue(ServerValue.TIMESTAMP);

            mRootRef.child("User").child(mCurrentUserId).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                    String username = String.valueOf(dataSnapshot.child("username").getValue());
                    String userprofile;
                    if(dataSnapshot.child("thumb_image").exists()) {
                        userprofile = String.valueOf(dataSnapshot.child("thumb_image").getValue());
                    }
                    else
                        userprofile = "default";

                    if(notify) {
                        sendNotification(mChatUser, username,userprofile, message, mCurrentUserId, "message");
                        //notif_string="";
                    }
                    notify = false;

                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });

        }
    }

    private String getContactFromStorage() {
        SharedPreferences Preferences = getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        String contact = Preferences.getString("my_phone","");
        return contact;
    }

    private void sendNotification(final String receiver, final String username, final String userprofle
            , final String message, final String mCurrentUserId, final String type) {

        DatabaseReference tokens = FirebaseDatabase.getInstance().getReference("Tokens");
        Query query = tokens.orderByKey().equalTo(receiver);
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                for(DataSnapshot dsp : dataSnapshot.getChildren()) {
                    Token token = dsp.getValue(Token.class);
                    Data data = new Data(mCurrentUserId,userprofle,message,username,receiver, type, getContactFromStorage());

                    Sender sender = new Sender(data,token.getToken());

                    apiService.sendNotification(sender)
                            .enqueue(new Callback<MyResponse>() {
                                @Override
                                public void onResponse(Call<MyResponse> call, Response<MyResponse> response) {
                                    if(response.code() == 200) {
                                        if(response.body().success != 1){
                                            //Toast.makeText(ChatActivity.this, "Failed!", Toast.LENGTH_SHORT).show();
                                            Log.i("Notification", "Failed!");
                                        }
                                    }
                                }

                                @Override
                                public void onFailure(Call<MyResponse> call, Throwable t) {

                                }
                            });

                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

//    private void seenMessage(final String userid) {
//
//        reference = FirebaseDatabase.getInstance().getReference("messages").child(mCurrentUserId).child(userid);
//        reference2 = FirebaseDatabase.getInstance().getReference("messages").child(userid).child(mCurrentUserId);
//        seenlistener = reference.addValueEventListener(new ValueEventListener() {
//            @Override
//            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//
//                for(DataSnapshot snapshot : dataSnapshot.getChildren()) {
//
//                    Messages chat = snapshot.getValue(Messages.class);
//                    if(chat.getFrom().equals(userid)) {{
//                        // messagesList.clear();
//                        HashMap<String, Object> hashMap = new HashMap<>();
//                        hashMap.put("seen",true);
//                        snapshot.getRef().updateChildren(hashMap);
//                        //  loadMessages();
//                    }}
//
//
//                }
//
//            }
//
//            @Override
//            public void onCancelled(@NonNull DatabaseError databaseError) {
//
//            }
//        });
//
//        seenlistener2 = reference2.addValueEventListener(new ValueEventListener() {
//            @Override
//            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//
//                for(DataSnapshot snapshot : dataSnapshot.getChildren()) {
//
//                    Messages chat = snapshot.getValue(Messages.class);
//                    if(chat.getFrom().equals(userid)) {{
//                        //messagesList.clear();
//                        HashMap<String, Object> hashMap = new HashMap<>();
//                        hashMap.put("seen",true);
//                        snapshot.getRef().updateChildren(hashMap);
//                        //loadMessages();
//                    }}
//
//
//                }
//
//            }
//
//            @Override
//            public void onCancelled(@NonNull DatabaseError databaseError) {
//
//            }
//        });
//
//
//
//    }

    private void currentUser(String userid) {

        SharedPreferences.Editor editor = getSharedPreferences("PREFS",MODE_PRIVATE).edit();
        editor.putString("currentuser",userid);
        editor.apply();
    }

    public Bitmap StringToBitMap(String encodedString){

        if(encodedString.equals("default") || encodedString.equals(""))
            return BitmapFactory.decodeResource(this.getResources(),R.drawable.ic_dp_icon_male_04);

        else {
            try {
                URL url = new URL(encodedString);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setDoInput(true);
                connection.connect();
                InputStream input = connection.getInputStream();
                Bitmap myBitmap = BitmapFactory.decodeStream(input);
                myBitmap = getCroppedBitmap(myBitmap);
                return myBitmap;
            } catch (Exception e) {
                //Toast.makeText(ChatActivity.this, e.toString(), Toast.LENGTH_SHORT).show();
                e.getMessage();
                return null;
            }
        }
    }

    public static String encrypt(byte[] data, byte[] key, byte[] ivs) {
        try {
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            SecretKeySpec secretKeySpec = new SecretKeySpec(key, "AES");
            byte[] finalIvs = new byte[16];
            int len = ivs.length > 16 ? 16 : ivs.length;
            System.arraycopy(ivs, 0, finalIvs, 0, len);
            IvParameterSpec ivps = new IvParameterSpec(finalIvs);
            cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec, ivps);
            byte[] encryptedBytes = cipher.doFinal(data);
            return new String(Base64.encode(encryptedBytes, Base64.DEFAULT));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public Bitmap getCroppedBitmap(Bitmap bitmap) {
        Bitmap output = Bitmap.createBitmap(bitmap.getWidth(),
                bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);

        final int color = 0xff424242;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        // canvas.drawRoundRect(rectF, roundPx, roundPx, paint);
        canvas.drawCircle(bitmap.getWidth() / 2, bitmap.getHeight() / 2,
                bitmap.getWidth() / 2, paint);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);
        //Bitmap _bmp = Bitmap.createScaledBitmap(output, 60, 60, false);
        //return _bmp;
        return output;
    }

    public void showImage(String img_url, String thumb_string) {
        Dialog builder = new Dialog(this);
        builder.requestWindowFeature(Window.FEATURE_NO_TITLE);
        builder.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));
        builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                //nothing;
            }
        });

        ImageView imageView = new ImageView(this);

        //imageView.setImageResource(R.drawable.ic_dp_icon_male_04);
        //Picasso.with(ChatActivity.this).load(img_url).placeholder(thumb_string).into(imageView);
        Picasso.with(ChatActivity.this)
                .load(thumb_string) // thumb is here
                .placeholder(R.drawable.ic_dp_icon_male_04)
                .into(imageView, new com.squareup.picasso.Callback() {
                            @Override
                            public void onSuccess() {
                                Picasso.with(ChatActivity.this).load(img_url)
                                        .placeholder(imageView.getDrawable()).into(imageView);
                                builder.addContentView(imageView, new RelativeLayout.LayoutParams(
                                        (int) pxFromDp(300),
                                        (int) pxFromDp(300)));
                                builder.show();
//                                PhotoViewAttacher photoAttacher;
//                                photoAttacher= new PhotoViewAttacher(imageView);
//                                photoAttacher.canZoom();
                            }

                            @Override
                            public void onError() {

                            }
                        });
    }

    private void seenMessage(final String userid) {

        reference = FirebaseDatabase.getInstance().getReference("messages").child(mCurrentUserId).child(userid);
        reference2 = FirebaseDatabase.getInstance().getReference("messages").child(userid).child(mCurrentUserId);
        seenlistener = reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                for(DataSnapshot snapshot : dataSnapshot.getChildren()) {

                    Messages chat = snapshot.getValue(Messages.class);
                    if(chat.getFrom().equals(userid)) {{

                        HashMap<String, Object> hashMap = new HashMap<>();
                        hashMap.put("seen",true);
                        snapshot.getRef().updateChildren(hashMap, new DatabaseReference.CompletionListener() {
                            @Override
                            public void onComplete(@Nullable DatabaseError error, @NonNull DatabaseReference ref) {
//                                messagesList.clear();
//                                loadMessages();
//                                mAdapter.notifyDataSetChanged();
                            }
                        });

                    }}


                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        seenlistener2 = reference2.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                for(DataSnapshot snapshot : dataSnapshot.getChildren()) {

                    Messages chat = snapshot.getValue(Messages.class);
                    if(chat.getFrom().equals(userid)) {{
                        //messagesList.clear();
                        HashMap<String, Object> hashMap = new HashMap<>();
                        hashMap.put("seen",true);
                        snapshot.getRef().updateChildren(hashMap);
                        //loadMessages();
                    }}


                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });



    }

    private float pxFromDp(float dp)
    {
        return dp * getResources().getDisplayMetrics().density;
    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
}