package social.locus.Chat;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.AudioAttributes;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;

import android.provider.ContactsContract;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import io.michaelrocks.libphonenumber.android.NumberParseException;
import io.michaelrocks.libphonenumber.android.PhoneNumberUtil;
import io.michaelrocks.libphonenumber.android.Phonenumber;
import social.locus.Adapters.SavedContactsModel;
import social.locus.Maps;
import social.locus.R;
import social.locus.Settings.RequestsActivity;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.Key;
import java.security.KeyFactory;
import java.security.spec.KeySpec;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.ArrayList;
import java.util.Random;

import javax.crypto.Cipher;

public class MyMessagingService extends FirebaseMessagingService {

    @Override
    public void onMessageReceived(@NonNull RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

        String sented = remoteMessage.getData().get("sented");
        String user = remoteMessage.getData().get("user");
        String contact = remoteMessage.getData().get("sender_contact");

        SharedPreferences preferences = getSharedPreferences("PREFS",MODE_PRIVATE);
        String currentUser = preferences.getString("currentuser","none");

        FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
         if(firebaseUser != null && sented.equals(firebaseUser.getUid())) {
            if(!currentUser.equals(user)) {
                if(ContextCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED) {
                    if (checkInContactIdList(contact)) {
                        sendOreoNotification(remoteMessage);
                    } else {
                        if (!check_request_notif_exists(user)) {
                            SharedPreferences sharedPreferences = getSharedPreferences("Request_prefs", MODE_PRIVATE);
                            SharedPreferences.Editor editor = sharedPreferences.edit();
                            editor.putString(user, contact);
                            editor.commit();
                            sendRequestNotif(remoteMessage.getData().get("title"), contact);
                        }
                    }

                }
            }
         }

    }

    private boolean check_request_notif_exists(String user) {
        SharedPreferences Preferences = getSharedPreferences("Request_prefs", Context.MODE_PRIVATE);
        String check = Preferences.getString(user,"");
        //Toast.makeText(getApplicationContext(), check, Toast.LENGTH_SHORT).show();
        Log.i("REQUEST",""+check);

        if(check.equals("")) {
            return false;
        }
        else {
            return true;
        }

    }

    private void sendRequestNotif(String title, String contact) {
        String channelID = "Request";

        Uri uri= Uri.parse("android.resource://"+getPackageName()+"/raw/send");
        AudioAttributes att = new AudioAttributes.Builder()
                .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                .setContentType(AudioAttributes.CONTENT_TYPE_SPEECH)
                .build();

        Intent intent = new Intent(getApplicationContext(), RequestsActivity.class);
        intent.putExtra("from", "Notification");
        PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(),400,intent,PendingIntent.FLAG_ONE_SHOT);

        NotificationChannel channel = new NotificationChannel(channelID, channelID, NotificationManager.IMPORTANCE_HIGH);
        channel.enableVibration(true);
        channel.enableLights(true);
        channel.setSound(uri,att);
        channel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
        NotificationManager manager = getSystemService(NotificationManager.class);
        manager.createNotificationChannel(channel);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(getApplicationContext(), channelID)
                .setOnlyAlertOnce(true)
                .setContentTitle(title)
                .setSound(uri)
                .setContentText(contact+" wants to connect.")
                .setAutoCancel(true)
                .setSmallIcon(R.drawable.ic_stat_name)
                .setPriority(Notification.PRIORITY_MAX);

        if (pendingIntent!=null){
            builder.setContentIntent(pendingIntent);
        }

        NotificationManagerCompat newmanager = NotificationManagerCompat.from(getApplicationContext());
        newmanager.notify(400, builder.build());
    }

    private void sendOreoNotification(final RemoteMessage remoteMessage) {

        final String user = remoteMessage.getData().get("user");
        final String icon = remoteMessage.getData().get("icon");
        final String title = remoteMessage.getData().get("title");
        final String body = remoteMessage.getData().get("body");
        final String type = remoteMessage.getData().get("notifytype");

                    //RemoteMessage.Notification notification = remoteMessage.getNotification();

        int notificationId = new Random().nextInt();

                    Intent intent;
                    if(type.matches("message")) {

                        intent = new Intent(getApplicationContext(), ChatActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString("user_id", user);
                        bundle.putString("username",title);
                        bundle.putString("userprofile_thumb",icon);
                        bundle.putString("userprofile",icon);
                        bundle.putString("open_from", "notification");
                        intent.putExtras(bundle);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(),user.hashCode(),intent,PendingIntent.FLAG_ONE_SHOT);

                        //Toast.makeText(getApplicationContext(), ""+user, Toast.LENGTH_SHORT).show();
                        Log.i("NNNN",icon);

                        String saved_unread_texts = getUnreadNotifs(user);
                        ArrayList<String> savedMessagesList;
                        String big_text;
                        if(saved_unread_texts.equals("")) {
                            big_text = body+"\n";
                            savedMessagesList = setSavedMessagelist(big_text);
                        }
                        else {
                            big_text = body+"\n"+saved_unread_texts;
                            savedMessagesList = setSavedMessagelist(big_text);
                        }
                        setUnreadNotifs(big_text,user);
                        getMessageNotification(title,body, user.hashCode(), icon, pendingIntent, big_text, savedMessagesList);

                    }
                    else if(type.matches("memory_view")){

                        intent = new Intent(getApplicationContext(), Maps.class);
                        Bundle bundle = new Bundle();
                        bundle.putString("user_id", user);
                        // bundle.putString("user_name","username");
                        intent.putExtras(bundle);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(),notificationId,intent,PendingIntent.FLAG_ONE_SHOT);
                        getNotification(title,body, notificationId, icon, pendingIntent);

                    }
                    else if(type.matches("memory_drop")){

                        intent = new Intent(getApplicationContext(), Maps.class);
                        Bundle bundle = new Bundle();
                        bundle.putString("user_id", user);
                        bundle.putString("username",title);
                        intent.putExtras(bundle);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(),notificationId,intent,PendingIntent.FLAG_ONE_SHOT);
                        getNotification(title,body, notificationId, icon, pendingIntent);

                    }

                    else if(type.matches("memory_reply")) {

                        intent = new Intent(getApplicationContext(), ChatActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString("user_id", user);
                        bundle.putString("user_name","username");
                        bundle.putString("userprofile_thumb",icon);
                        bundle.putString("userprofile",icon);
                        intent.putExtras(bundle);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(),notificationId,intent,PendingIntent.FLAG_ONE_SHOT);
                        getNotification(title,body, notificationId, icon, pendingIntent);

                    }
                    else {

                        intent = new Intent(getApplicationContext(), Maps.class);
                        Bundle bundle = new Bundle();
                        bundle.putString("user_id", user);
                        bundle.putString("user_name","username");
                        intent.putExtras(bundle);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(),notificationId,intent,PendingIntent.FLAG_ONE_SHOT);
                        getNotification(title,body, notificationId, icon, pendingIntent);

                    }
        }

    private void getMessageNotification(String Title, String Text, int id , String pic, PendingIntent intent, String bigText, ArrayList<String> unreadList) {

        String channelID = "Messaging";

        Uri uri= Uri.parse("android.resource://"+getPackageName()+"/raw/send");
        AudioAttributes att = new AudioAttributes.Builder()
                .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                .setContentType(AudioAttributes.CONTENT_TYPE_SPEECH)
                .build();

        NotificationChannel channel = new NotificationChannel(channelID, channelID, NotificationManager.IMPORTANCE_HIGH);
        channel.enableVibration(true);
        channel.enableLights(true);
        channel.setSound(uri,att);
        channel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
        NotificationManager manager = getSystemService(NotificationManager.class);
        manager.createNotificationChannel(channel);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(getApplicationContext(), channelID)
                .setContentTitle(Title)
                .setSound(uri)
                .setContentText(Text)
                //.setStyle(new NotificationCompat.BigTextStyle().bigText(bigText))
                .setAutoCancel(true)
                .setLargeIcon(StringToBitMap(pic))
                .setPriority(Notification.PRIORITY_MAX)
                .setSmallIcon(R.drawable.ic_stat_name);

        NotificationCompat.InboxStyle inboxStyle = new NotificationCompat.InboxStyle();
        NotificationCompat.BigTextStyle bigStyle = new NotificationCompat.BigTextStyle();

        if(Text.equals(bigText)) {
            builder.setContentText(Text);
            builder.setStyle(bigStyle).setContentText(Text);
        }
        else {
            int size = unreadList.size();
            if(size>7) size = 7;
            int i = size-1;
            while(i>=0) {
                inboxStyle.addLine(unreadList.get(i));
                i--;
            }
            builder.setStyle(inboxStyle);
        }

        if (intent!=null){
            builder.setContentIntent(intent);
        }

        NotificationManagerCompat newmanager = NotificationManagerCompat.from(getApplicationContext());
        newmanager.notify(id, builder.build());

    }

    public void getNotification(String Title, String Text, int id , String pic, PendingIntent intent) {

        String channelID = "Notification";
        String GroupID = "Default_group";
        int GROUP_ID = 100;

        Uri uri= Uri.parse("android.resource://"+getPackageName()+"/raw/send");
        AudioAttributes att = new AudioAttributes.Builder()
                .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                .setContentType(AudioAttributes.CONTENT_TYPE_SPEECH)
                .build();

        NotificationChannel channel = new NotificationChannel(channelID, channelID, NotificationManager.IMPORTANCE_HIGH);
            channel.enableVibration(true);
            channel.enableLights(true);
            channel.setSound(uri,att);
            channel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
            NotificationManager manager = getSystemService(NotificationManager.class);
            manager.createNotificationChannel(channel);

        NotificationCompat.Builder groupBuilder =
                new NotificationCompat.Builder(getApplicationContext())
                        .setContentTitle(Title)
                        .setContentText(Text)
                        .setGroupSummary(true)
                        .setGroup(GroupID)
                        .setSmallIcon(R.drawable.ic_stat_name)
                        .setStyle(new NotificationCompat.BigTextStyle().bigText(Text))
                        .setContentIntent(intent);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(getApplicationContext(), channelID)
                .setContentTitle(Title)
                .setOnlyAlertOnce(true)
                .setSound(uri)
                .setContentText(Text)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(Text))
                .setAutoCancel(true)
                .setSmallIcon(R.drawable.ic_stat_name)
                .setLargeIcon(StringToBitMap(pic))
                .setPriority(Notification.PRIORITY_MAX);

        if (intent!=null){
            builder.setContentIntent(intent);
        }

        NotificationManagerCompat newmanager = NotificationManagerCompat.from(getApplicationContext());
        newmanager.notify(GROUP_ID, groupBuilder.build());
        newmanager.notify(id, builder.build());

    }

    public Bitmap StringToBitMap(String encodedString){

        Log.i("NNNN",encodedString);

        if(encodedString.matches("default")) {
            Log.i("NNNN",encodedString);
//            Bitmap bitmap = ((BitmapDrawable) ResourcesCompat.getDrawable(getApplicationContext().getResources(),
//                    R.drawable.ic_dp_icon_male_04, null)).getBitmap();
            return getBitmap(R.drawable.ic_dp_icon_male_04);
        }

        else {
           // Log.i("NNNN",encodedString);

            try {
                URL url = new URL(encodedString);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setDoInput(true);
                connection.connect();
                InputStream input = connection.getInputStream();
                Bitmap myBitmap = BitmapFactory.decodeStream(input);
                myBitmap = getCroppedBitmap(myBitmap);
                return myBitmap;
            } catch (Exception e) {
                e.getMessage();
                return null;
            }
        }
    }

    public Bitmap getCroppedBitmap(Bitmap bitmap) {
        Bitmap output = Bitmap.createBitmap(bitmap.getWidth(),
                bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);

        final int color = 0xff424242;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        // canvas.drawRoundRect(rectF, roundPx, roundPx, paint);
        canvas.drawCircle(bitmap.getWidth() / 2, bitmap.getHeight() / 2,
                bitmap.getWidth() / 2, paint);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);
        //Bitmap _bmp = Bitmap.createScaledBitmap(output, 60, 60, false);
        //return _bmp;
        return output;
    }

    private void setUnreadNotifs(String text, String user_id) {
        SharedPreferences notifPreferences = getSharedPreferences("NotifPrefs",MODE_PRIVATE);
        SharedPreferences.Editor myEdit = notifPreferences.edit();
        myEdit.putString(user_id, text);
        myEdit.commit();
    }

    private String getUnreadNotifs(String user) {
        SharedPreferences notifPreferences = getSharedPreferences("NotifPrefs", MODE_PRIVATE);
        return notifPreferences.getString(user,"");
    }

    private ArrayList<String> setSavedMessagelist(String message_string) {
        Log.i("Save_input",message_string);
        String temp = "";
        ArrayList<String> savedMessageList = new ArrayList<>();
        for(int i = 0; i < message_string.length(); i++) {
            temp = temp+message_string.charAt(i);
            Log.i("Save_nextchar",temp);
            if(message_string.charAt(i) == '\n') {
                temp = temp.substring(0,temp.length()-1);
                savedMessageList.add(temp);
                Log.i("Save_nextline",temp);
                temp = "";
            }
        }

        return savedMessageList;
    }

    private Bitmap getBitmap(int drawableRes) {
        Drawable drawable = getResources().getDrawable(drawableRes);
        Canvas canvas = new Canvas();
        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        canvas.setBitmap(bitmap);
        drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
        drawable.draw(canvas);

        return bitmap;
    }

    private String getCountryCode(String number) {


        PhoneNumberUtil phoneUtil = PhoneNumberUtil.createInstance(this);;
        try {
            Phonenumber.PhoneNumber NumberProto = phoneUtil.parse(number, "IN");
            if (!phoneUtil.isValidNumber(NumberProto)) {
                //Log.i("Contact","Not Valid");
                //showError(getResources().getString(R.string.wrong_phone_number));
                return number;
            }
            String regionISO = String.valueOf(NumberProto.getCountryCode());
            if(!(regionISO.matches(""))){
                //Log.i("Contact","Valid "+regionISO);
                number = "+"+regionISO+number;
            }


        } catch (NumberParseException e) {
            //Log.i("Contact","Exception: "+e.toString());
            //showError(e.toString());
            return number;
        }


        return number;
    }

    private boolean checkInContactIdList(String contact) {
            Cursor phones = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI
                    , null, null, null, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " ASC");
            while (phones.moveToNext()) {
                String phoneNumber = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                phoneNumber = phoneNumber.replaceAll("[()\\s-]", "");
                if(phoneNumber.length() > 0) {
                    if (phoneNumber.charAt(0) != '+') {
                        phoneNumber = getCountryCode(phoneNumber);
                    }
                    if (phoneNumber.charAt(0) == '0') {
                        phoneNumber = phoneNumber.substring(1);
                    }
                    if (!phoneNumber.equals(getContactFromStorage())) {

                        if (phoneNumber.equals(contact)) {
                            phones.close();
                            return true;
                        }
                    }
                }
                //Toast.makeText(this, , Toast.LENGTH_SHORT).show();
            }
            phones.close();
        return false;
    }

    private String getContactFromStorage() {
        SharedPreferences Preferences = getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        String contact = Preferences.getString("my_phone","");
        return contact;
    }

}
