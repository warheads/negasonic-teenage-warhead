package social.locus.Chat;

import android.app.AlertDialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;

import android.util.Base64;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;
import java.security.Key;
import java.security.KeyFactory;
import java.security.spec.KeySpec;
import java.security.spec.PKCS8EncodedKeySpec;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import jp.wasabeef.picasso.transformations.BlurTransformation;
import social.locus.R;

import static android.content.ContentValues.TAG;
import static com.facebook.FacebookSdk.getApplicationContext;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class MessageAdapter extends RecyclerView.Adapter<MessageAdapter.MessageViewHolder>{

    private List<Messages> mMessageList;
    String id = FirebaseAuth.getInstance().getCurrentUser().getUid();
    Context mContext;
    String mChatuserid;
    String mCurrentid;

    public MessageAdapter(List<Messages> mMessageList, Context mContext, String Chatuserid, String Currentid) {

        this.mMessageList = mMessageList;
        this.mContext = mContext;
        this.mChatuserid = Chatuserid;
        this.mCurrentid = Currentid;

    }


    @NonNull
    @Override
    public MessageViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.message_single_layout ,parent, false);

        return new MessageViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final MessageViewHolder viewHolder, final int i) {

        final Messages c = mMessageList.get(i);
        final DatabaseReference reference = FirebaseDatabase.getInstance().getReference("messages").child(id).child(mChatuserid).child(c.getMsg_id());

        final long info = c.getTime();
        final String message = c.getMessage();
        String message_type = c.getType();

        viewHolder.messageImage.setVisibility(View.GONE);
        viewHolder.imgholder.setVisibility(View.GONE);
        viewHolder.locality.setVisibility(View.GONE);
        viewHolder.messageText.setMaxWidth((int) pxFromDp(300));
        viewHolder.messageText.setTextAlignment(View.TEXT_ALIGNMENT_TEXT_START);

        if(c.getFrom().equals(id)) {
            viewHolder.right1.setBackgroundResource(R.drawable.send_bubble);
            viewHolder.messageText.setTextColor(Color.parseColor("#FFFFFF"));
            viewHolder.parent_bubble.setGravity(Gravity.END);
        }
        else {
            viewHolder.right1.setBackgroundResource(R.drawable.receive_bubble);
            viewHolder.messageText.setTextColor(Color.parseColor("#FFFFFF"));
            viewHolder.parent_bubble.setGravity(Gravity.START);
        }

        if(message_type.equals("text")) {

            if(c.getFrom().equals(mChatuserid)) {
                String decrypt_message = decrypt(message,mCurrentid.substring(0,16).getBytes()
                        ,String.valueOf(c.getTime()).substring(0,8).getBytes(StandardCharsets.UTF_8));
                viewHolder.messageText.setText(decrypt_message);
            }
            else {
                byte[] key_bytes = mChatuserid.substring(0,16).getBytes();
                byte[] iv_bytes = String.valueOf(c.getTime()).substring(0,8).getBytes(StandardCharsets.UTF_8);
                String decrypt_message = decrypt(message, key_bytes, iv_bytes);
                viewHolder.messageText.setText(decrypt_message);
            }

            //viewHolder.messageText.setText(message);
            viewHolder.messageText.setVisibility(View.VISIBLE);
            viewHolder.messageImage.setVisibility(View.GONE);

        }

        else {
            if(c.getType().equals("view") || c.getType().equals("drop")) {
                viewHolder.right1.setBackgroundResource(R.drawable.view_bubble);
                viewHolder.messageText.setVisibility(View.VISIBLE);
                viewHolder.messageImage.setVisibility(View.GONE);
                viewHolder.imgholder.setVisibility(View.GONE);
                viewHolder.messageText.setTextColor(Color.parseColor("#000000"));
                viewHolder.messageText.setText(message);
                viewHolder.messageText.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                viewHolder.parent_bubble.setGravity(Gravity.CENTER_HORIZONTAL);
                //viewHolder.messageText.setWidth((int)pxFromDp(300));

            }
            else {
                viewHolder.messageText.setVisibility(View.VISIBLE);
                viewHolder.messageImage.setVisibility(View.VISIBLE);
                viewHolder.imgholder.setVisibility(View.VISIBLE);
                viewHolder.messageText.setMaxWidth((int) pxFromDp(150));
                viewHolder.messageText.setText("↩ ️" + message);
                viewHolder.locality.setVisibility(View.VISIBLE);
                viewHolder.locality_textiew.setText(c.getType());
                viewHolder.messageText.setTextAlignment(View.TEXT_ALIGNMENT_TEXT_START);
                Picasso.with(viewHolder.messageText.getContext()).load(c.getThumb())
                        .placeholder(R.drawable.memory_placeholder).into(viewHolder.messageImage);
            }

        }

        viewHolder.parent_bubble.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {

                if(!c.getType().equals("view") && !c.getType().equals("drop")) {

                   // Toast.makeText(mContext, c.getType(), Toast.LENGTH_SHORT).show();

                    final CharSequence[] items = {"Copy Message", "Delete Message"};

                    final AlertDialog.Builder builder = new AlertDialog.Builder(mContext);

                        builder.setTitle("Message Info : " + getDate(info, "   hh:mm a   dd/MM/yyyy"));

                    builder.setItems(items, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int item) {

                            if (item == 0) {

                                ClipboardManager clipboard = (ClipboardManager) mContext.getSystemService(Context.CLIPBOARD_SERVICE);
                                if(message_type.matches("text")) {
                                    if(c.getFrom().matches(mChatuserid)) {
                                        ClipData clip = ClipData.newPlainText("", decrypt(message,mCurrentid.substring(0,16).getBytes()
                                                ,String.valueOf(c.getTime()).substring(0,8).getBytes(StandardCharsets.UTF_8)));
                                        clipboard.setPrimaryClip(clip);
                                    } else {
                                        byte[] key_bytes = mChatuserid.substring(0,16).getBytes();
                                        byte[] iv_bytes = String.valueOf(c.getTime()).substring(0,8).getBytes(StandardCharsets.UTF_8);
                                        String decrypt_message = decrypt(message, key_bytes, iv_bytes);
                                        ClipData clip = ClipData.newPlainText("", decrypt_message);
                                        clipboard.setPrimaryClip(clip);
                                    }
                                }
                                else {
                                    ClipData clip = ClipData.newPlainText("", message);
                                    clipboard.setPrimaryClip(clip);
                                }
                                Toast.makeText(mContext, "Message Copied", Toast.LENGTH_SHORT).show();
                            } else {
                                reference.removeValue().addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void aVoid) {
                                        mMessageList.remove(i);
                                        //FirebaseDatabase.getInstance().getReference("messages").child(c.getFrom()).child(id).child(c.getMsg_id()).removeValue();
                                       // FirebaseDatabase.getInstance().getReference("messages").child(id).child(c.getFrom()).child(c.getMsg_id()).removeValue();
                                        Toast.makeText(mContext, "Message deleted.", Toast.LENGTH_SHORT).show();
                                        notifyDataSetChanged();
                                    }
                                });
                            }
                        }
                    });
                    builder.show();

                }
                return false;
            }
        });

    }

    @Override
    public int getItemCount() {

        return mMessageList.size();

    }

    public class MessageViewHolder extends RecyclerView.ViewHolder {

        public TextView messageText,read;
        // public CircularImageView profileImage;
        public LinearLayout locality;
        public TextView locality_textiew;
        public ImageView messageImage;
        public RelativeLayout right1;
        public LinearLayout parent_bubble;
        CardView imgholder;

        public MessageViewHolder(View view) {
            super(view);

            messageText = (TextView) view.findViewById(R.id.message_text_layout);
            read = (TextView) view.findViewById(R.id.read_textView);
            messageImage = view.findViewById(R.id.message_image_layout);
            right1 = view.findViewById(R.id.message_single_layout);
            parent_bubble = view.findViewById(R.id.parent_bubble);
            imgholder = view.findViewById(R.id.imagemsgHolder);
            locality = view.findViewById(R.id.locality_layout);
            locality_textiew = view.findViewById(R.id.locality_textview);
        }
    }

    private static String getDate(long milliSeconds, String dateFormat)
    {
        // Create a DateFormatter object for displaying date in specified format.
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);

        // Create a calendar object that will convert the date and time value in milliseconds to date.
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        return formatter.format(calendar.getTime());
    }

    public static String decrypt(String data, byte[] key, byte[] ivs) {
        try {
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            SecretKeySpec secretKeySpec = new SecretKeySpec(key, "AES");
            byte[] finalIvs = new byte[16];
            int len = ivs.length > 16 ? 16 : ivs.length;
            System.arraycopy(ivs, 0, finalIvs, 0, len);
            IvParameterSpec ivps = new IvParameterSpec(finalIvs);
            cipher.init(Cipher.DECRYPT_MODE, secretKeySpec, ivps);
            byte[] encryptedBytes = Base64.decode(data, Base64.DEFAULT);
            return new String(cipher.doFinal(encryptedBytes));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private float pxFromDp(float dp)
    {
        return dp * mContext.getResources().getDisplayMetrics().density;
    }

    private float pxFromsp(float sp)
    {
        return sp * mContext.getResources().getDisplayMetrics().scaledDensity;
    }

}
