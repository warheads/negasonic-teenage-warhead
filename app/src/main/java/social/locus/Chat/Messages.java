package social.locus.Chat;

public class Messages {



    private String message, type;
    private long  time;
    private boolean seen;
    private String from;
    private String thumb;
    private String msg_id;
    private int version;

    public Messages(String from) {
        this.from = from;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public Messages(String message, String type, long time, boolean seen ,String thumb, String msg_id, int version) {
        this.message = message;
        this.type = type;
        this.time = time;
        this.seen = seen;
        this.thumb = thumb;
        this.msg_id = msg_id;
        this.version = version;
    }

    public String getMessage() {
        return message;
    }

    public void setThumb(String thumb) {
        this.thumb = thumb;
    }

    public void setMsg_id(String msg_id) {
        this.msg_id = msg_id;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public boolean isSeen() {
        return seen;
    }

    public void setSeen(boolean seen) {
        this.seen = seen;
    }

    public String getThumb() {
        return thumb;
    }

    public String getMsg_id() {
        return msg_id;
    }

    public Messages(){

    }

}
