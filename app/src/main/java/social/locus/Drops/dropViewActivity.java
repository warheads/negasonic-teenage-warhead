package social.locus.Drops;
import android.animation.AnimatorInflater;
import android.animation.AnimatorSet;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.location.Address;
import android.location.Geocoder;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.text.TextUtils;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import de.hdodenhof.circleimageview.CircleImageView;
import io.michaelrocks.libphonenumber.android.NumberParseException;
import io.michaelrocks.libphonenumber.android.PhoneNumberUtil;
import io.michaelrocks.libphonenumber.android.Phonenumber;
import social.locus.Adapters.SavedContactsModel;
import social.locus.Beans.Warhead;
import social.locus.Camera.OnSwipeTouchListener;
import social.locus.APIService;
import social.locus.Chat.ChatActivity;
import social.locus.Maps;
import social.locus.Notifications.Client;
import social.locus.Notifications.Data;
import social.locus.Notifications.MyResponse;
import social.locus.Notifications.Sender;
import social.locus.Notifications.Token;
import social.locus.R;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.common.geometry.S2CellId;
import com.google.common.geometry.S2LatLng;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.security.Key;
import java.security.KeyFactory;
import java.security.spec.KeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.crypto.Cipher;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class dropViewActivity extends AppCompatActivity {

    EditText rplytext;
    ImageView front_drop_pic,back_drop_pic, receiver_pic;
    CircleImageView sender_profilepic;
    TextView drop_time,sender_name,view_count,comment_count, for_text;
    ImageView replybutton,smileup, add_reply_button, add_comment_button;
    String mCurrentUserId = FirebaseAuth.getInstance().getCurrentUser().getUid();
    private DatabaseReference mRootRef,likeref;
    //String boxId = null;
    APIService apiService;
    ProgressBar loadimage;
    private AnimatorSet mSetRightOut;
    private AnimatorSet mSetLeftIn;
    private boolean mIsBackVisible = false;
    private View mCardFrontLayout;
    private View mCardBackLayout;
    ImageView delete_memory_button;
    Warhead warhead;
    Button add_contact_button;
    String contact="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drop_view);
        overridePendingTransition(0,0);

        warhead = (Warhead) getIntent().getSerializableExtra("Warhead");
        final String username = getIntent().getStringExtra("username");
        final String userprofile = getIntent().getStringExtra("userprofile");
        //contact = getIntent().getStringExtra("contact");

        mRootRef = FirebaseDatabase.getInstance().getReference();
        likeref = FirebaseDatabase.getInstance().getReference("Upvotes");
        loadimage = new ProgressBar(this);
        apiService = Client.getClient("https://fcm.googleapis.com/").create(APIService.class);

        rplytext = findViewById(R.id.rplytext);
        add_reply_button = findViewById(R.id.add_reply_button);
        add_comment_button = findViewById(R.id.add_comment_button);
        sender_name = findViewById(R.id.sender_name);
        front_drop_pic = findViewById(R.id.front_drop_pic);
        back_drop_pic = findViewById(R.id.back_drop_pic);
        sender_profilepic = findViewById(R.id.sender_profilepic);
        replybutton = findViewById(R.id.replybutton);
        view_count = findViewById(R.id.viewcount);
        comment_count = findViewById(R.id.comment_count);
        drop_time = findViewById(R.id.sendtime);
        smileup = findViewById(R.id.smileup);
        mCardBackLayout = findViewById(R.id.backimagecard);
        mCardFrontLayout = findViewById(R.id.frontimagecard);
        delete_memory_button = findViewById(R.id.delete_memory_button);
        for_text = findViewById(R.id.for_text);
        add_contact_button = findViewById(R.id.add_contact_button);

        front_drop_pic.requestLayout();
        back_drop_pic.requestLayout();
        changeCameraDistance();

        if(warhead != null) {

            setCommentCount(warhead.getWarid());

            S2LatLng warhead_latlan = S2LatLng.fromDegrees(warhead.getDroplat(),warhead.getDroplon());
            S2CellId warhead_cell = S2CellId.fromLatLng(warhead_latlan).parent(18);


            drop_time.setText(String.valueOf(getDate(Long.parseLong(warhead.getSendtime()), " hh:mm a dd/MM/yyyy")));
            sender_name.setText(warhead.getSendername());
            Picasso.with(dropViewActivity.this).load(warhead.getSenderimage())
                    .placeholder(R.drawable.ic_dp_icon_male_04).into(sender_profilepic);
            Picasso.with(dropViewActivity.this).load(warhead.getImgUrl())
                    .placeholder(R.drawable.memory_placeholder).into(front_drop_pic);
            Picasso.with(dropViewActivity.this).load(warhead.getBackimgurl())
                    .placeholder(R.drawable.memory_placeholder).into(back_drop_pic);


            DisplayMetrics displayMetrics = new DisplayMetrics();
            getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
            int width = displayMetrics.widthPixels;
            int polWidth = (int) (width - (width * 0.10));
            int polHeight = (int) (width * 1.0635);
            //        int polWidth = width;
            //        int polHeight = width;
            front_drop_pic.getLayoutParams().width = polWidth;
            front_drop_pic.getLayoutParams().height = polHeight;
            back_drop_pic.getLayoutParams().width = polWidth;
            back_drop_pic.getLayoutParams().height = polHeight;

            rplytext.setVisibility(View.GONE);
            replybutton.setVisibility(View.GONE);
            smileup.setVisibility(View.GONE);
            view_count.setVisibility(View.GONE);
            add_reply_button.setVisibility(View.GONE);
            delete_memory_button.setVisibility(View.GONE);
            add_comment_button.setVisibility(View.GONE);
            comment_count.setVisibility(View.GONE);
            for_text.setVisibility(View.GONE);
            add_contact_button.setVisibility(View.GONE);

            delete_memory_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            switch (which) {
                                case DialogInterface.BUTTON_NEGATIVE:
                                    break;

                                case DialogInterface.BUTTON_POSITIVE:

                                    if(warhead.getType().matches("self")) {

                                        FirebaseDatabase.getInstance().getReference("self").child(mCurrentUserId).
                                                child(String.valueOf(warhead_cell.id())).child(warhead.getWarid()).removeValue()
                                                .addOnSuccessListener(new OnSuccessListener<Void>() {
                                                    @Override
                                                    public void onSuccess(Void unused) {
                                                        Toast.makeText(dropViewActivity.this, "Deleted.", Toast.LENGTH_SHORT).show();
                                                        startActivity(new Intent(dropViewActivity.this, Maps.class));
                                                        finish();
                                                    }
                                                });

                                    }

                                    else if(warhead.getType().matches("forU")) {
                                        warhead.setLikes(1);
                                        FirebaseDatabase.getInstance().getReference("foryou").child(warhead.getSender()).
                                                child("sent").child(String.valueOf(warhead_cell.id())).child(warhead.getWarid()).removeValue();
                                        FirebaseDatabase.getInstance().getReference("foryou_list").child(mCurrentUserId).
                                                child("received").child(warhead.getWarid()).removeValue();
                                        FirebaseDatabase.getInstance().getReference("foryou").child(mCurrentUserId).
                                                child("received").child(String.valueOf(warhead_cell.id())).child(warhead.getWarid()).removeValue()
                                                .addOnSuccessListener(new OnSuccessListener<Void>() {
                                                    @Override
                                                    public void onSuccess(Void unused) {
                                                        Toast.makeText(dropViewActivity.this, "Deleted.", Toast.LENGTH_SHORT).show();
                                                        startActivity(new Intent(dropViewActivity.this, Maps.class));
                                                        finish();
                                                    }
                                                });
                                    }

                                    else if(warhead.getType().matches("global")) {

                                        FirebaseDatabase.getInstance().getReference("Upvotes")
                                                .child(warhead.getWarid()).removeValue();
                                        FirebaseDatabase.getInstance().getReference("Comments")
                                                .child(warhead.getWarid()).removeValue();
                                        FirebaseDatabase.getInstance().getReference("TeenageWarheads").child(String.valueOf(warhead_cell.id()))
                                                .child(warhead.getWarid()).removeValue()
                                                .addOnSuccessListener(new OnSuccessListener<Void>() {
                                                    @Override
                                                    public void onSuccess(Void unused) {
                                                        Toast.makeText(dropViewActivity.this, "Deleted.", Toast.LENGTH_SHORT).show();
                                                        startActivity(new Intent(dropViewActivity.this, Maps.class));
                                                        finish();
                                                    }
                                                });

                                    }

                                    else if(warhead.getType().matches("sent")) {

                                        warhead.setLikes(1);
                                        FirebaseDatabase.getInstance().getReference("foryou_list").child(warhead.getCaption()).
                                                child(warhead.getWarid()).removeValue();
                                        FirebaseDatabase.getInstance().getReference("foryou").child(warhead.getCaption()).
                                                child("received").child(String.valueOf(warhead_cell.id())).child(warhead.getWarid()).removeValue();
                                        FirebaseDatabase.getInstance().getReference("foryou").child(mCurrentUserId).
                                                child("sent").child(String.valueOf(warhead_cell.id())).child(warhead.getWarid()).removeValue()
                                                .addOnSuccessListener(new OnSuccessListener<Void>() {
                                                    @Override
                                                    public void onSuccess(Void unused) {
                                                        Toast.makeText(dropViewActivity.this, "Deleted.", Toast.LENGTH_SHORT).show();
                                                        startActivity(new Intent(dropViewActivity.this, Maps.class));
                                                        finish();

                                                    }
                                                });
                                    }

                                    break;
                            }
                        }
                    };

                    AlertDialog.Builder builder = new AlertDialog.Builder(dropViewActivity.this,R.style.DatePickerTheme);
                    builder.setMessage("Do you really want to remove the memory?").setPositiveButton("Yes", dialogClickListener)
                            .setNegativeButton("No", dialogClickListener).show();
                }

            });

            if (warhead.getType().equals("global")) {

                //getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);

                smileup.setVisibility(View.VISIBLE);
                add_comment_button.setVisibility(View.VISIBLE);
                comment_count.setVisibility(View.VISIBLE);
                view_count.setVisibility(View.VISIBLE);

                add_comment_button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(dropViewActivity.this, CommentsActivity.class);
                        intent.putExtra("Warhead",warhead);
                        startActivity(intent);
                    }
                });

                //Toast.makeText(dropViewActivity.this, "herereeeee", Toast.LENGTH_SHORT).show();

                if(warhead.getSender().matches(mCurrentUserId)) {

                   // Toast.makeText(dropViewActivity.this, "herereeeee", Toast.LENGTH_SHORT).show();

                    delete_memory_button.setVisibility(View.VISIBLE);

                }

                likeref.child(warhead.getWarid()).addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        int likes = 0;
                        if (dataSnapshot.hasChildren()) {
                            likes = (int) dataSnapshot.getChildrenCount();
                        }

                        if (dataSnapshot.child(mCurrentUserId).exists()) {
                            //likeref.child(warhead.getWarid()).child(warhead.getSender()).setValue(true);
                            smileup.setImageResource(R.drawable.smiledup);
                            smileup.setTag("smiled");
                        } else {
                            // likeref.child(warhead.getWarid()).child(warhead.getSender()).removeValue();
                            smileup.setImageResource(R.drawable.smileup);
                            smileup.setTag("smile");
                        }

                        //mRootRef.child("TeenageWarheads").child(String.valueOf(warhead_cell.id())).child(warhead.getWarid()).child("likes").setValue(likes);
                        view_count.setText("" + likes);
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
                smileup.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (smileup.getTag().equals("smile")) {
                            smileup.setTag("smiled");
                            likeref.child(warhead.getWarid()).child(mCurrentUserId).setValue(true);
                        } else {
                            smileup.setTag("smile");
                            likeref.child(warhead.getWarid()).child(mCurrentUserId).removeValue();
                        }
                    }
                });
            }

            if (warhead.getType().matches("forU")) {

                get_receiver_contact(warhead.getSender());
                if (!(warhead.getIsViewed())) {

                    mRootRef.child("foryou").child(mCurrentUserId).child("received").child(String.valueOf(warhead_cell.id())).child(warhead.getWarid()).child("isViewed").setValue(true);
                    mRootRef.child("foryou").child(warhead.getSender()).child("sent").child(String.valueOf(warhead_cell.id())).child(warhead.getWarid()).child("isViewed").setValue(true);
                    mRootRef.child("foryou_list").child(mCurrentUserId).child("received").child(warhead.getWarid()).child("isViewed").setValue(true);
                    sendviewMessage(username + " viewed\uD83D\uDC40 a memory at " + getLocality(warhead.getDroplat(), warhead.getDroplon()),
                            warhead.getSender(), warhead.getThumb(), "view", 1);
                    sendNotification(warhead.getSender(), username, userprofile,
                            username+" viewed\uD83D\uDC40 a ForYou dropped by you at " + getLocality(warhead.getDroplat(), warhead.getDroplon()), mCurrentUserId, "memory_view");
                    setNotifs(warhead.getSender(), username, userprofile,
                            " viewed\uD83D\uDC40 a ForYou dropped by you at " + getLocality(warhead.getDroplat(), warhead.getDroplon()),
                            warhead.getWarid(), String.valueOf(System.currentTimeMillis()));

                }
                // smileup.setImageResource(R.drawable.add_reply_button);
                add_reply_button.setVisibility(View.VISIBLE);
                delete_memory_button.setVisibility(View.VISIBLE);

                add_reply_button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        rplytext.setVisibility(View.VISIBLE);
                        replybutton.setVisibility(View.VISIBLE);

                        replybutton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                String reply = rplytext.getText().toString().trim();
                                if (!TextUtils.isEmpty(reply)) {
                                    sendNotification(warhead.getSender(), username, userprofile,
                                            username+" replied to a ForYou dropped by you at " + getLocality(warhead.getDroplat(), warhead.getDroplon()) + ": \"" + reply + "\" .",
                                            mCurrentUserId, "memory_reply");
                                    setNotifs(warhead.getSender(), username, userprofile,
                                            " replied to a ForYou dropped by you at " + getLocality(warhead.getDroplat(), warhead.getDroplon()) + ": \"" + reply + "\" .",
                                            warhead.getWarid(), String.valueOf(System.currentTimeMillis()));
                                    rplytext.setText("");
                                    Toast.makeText(dropViewActivity.this, "Replied", Toast.LENGTH_SHORT).show();
//                                rplytext.setVisibility(View.INVISIBLE);
//                                replybutton.setVisibility(View.INVISIBLE);
                                    sendreply(reply, warhead.getSender(), warhead.getThumb()
                                            , warhead.getSendername(), warhead.getSenderimage(),warhead.getSenderimage(),
                                            getLocality(warhead.getDroplat(), warhead.getDroplon()), 1);
                                }
                            }
                        });
                    }
                });

                add_contact_button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                            addContact(warhead.getSendername(), contact);
                    }
                });

            }

            if(warhead.getType().matches("self")) {
                delete_memory_button.setVisibility(View.VISIBLE);

                if(!(warhead.getIsViewed())) {
                    mRootRef.child("self").child(mCurrentUserId).child(String.valueOf(warhead_cell.id())).child(warhead.getWarid()).child("isViewed").setValue(true);
                }

            }

            if(warhead.getType().matches("sent")) {
                add_reply_button.setVisibility(View.GONE);
                delete_memory_button.setVisibility(View.VISIBLE);
                for_text.setVisibility(View.VISIBLE);
                if((warhead.getIsViewed())) {
                    view_count.setVisibility(View.VISIBLE);
                    view_count.setText(" Viewed");
                }
            }

        }

        else
            Toast.makeText(this, "Error!Nothing to show!", Toast.LENGTH_SHORT).show();

        mCardFrontLayout.setOnTouchListener(new OnSwipeTouchListener(this){
            @Override
            public void onSwipeLeft() {
                super.onSwipeLeft();

                if(mIsBackVisible)
                    loadAnimations();
                else
                    loadleftAnimations();

                flipCard(mCardBackLayout);
            }

            @Override
            public void onSwipeRight() {
                super.onSwipeRight();
                if(!mIsBackVisible)
                    loadAnimations();
                else
                    loadleftAnimations();
                flipCard(mCardBackLayout);
            }
        });
    }

    private void setCommentCount(String war_id) {

        FirebaseDatabase.getInstance().getReference("Comments").child(war_id).addValueEventListener(new ValueEventListener() {
            int count=0;
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                count = (int) snapshot.getChildrenCount();
                comment_count.setText(""+count);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        //getWindow().clearFlags(WindowManager.LayoutParams.FLAG_SECURE);
    }

    @Override
    protected void onResume() {

        super.onResume();
        if (check_contact_availability(contact)) {
            //Toast.makeText(dropViewActivity.this, contact, Toast.LENGTH_SHORT).show();
            add_contact_button.setVisibility(View.GONE);
            add_reply_button.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onBackPressed() {
        //startActivity(new Intent(dropViewActivity.this, Maps.class));
        finish();
        overridePendingTransition(0,0);
    }

    private static String getDate(long milliSeconds, String dateFormat) {
        // Create a DateFormatter object for displaying date in specified format.
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);

        // Create a calendar object that will convert the date and time value in milliseconds to date.
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        return formatter.format(calendar.getTime());
    }

    private void setNotifs(String userid, String username, String profile, String did, String war_id, String time) {

        DatabaseReference reference = FirebaseDatabase.getInstance().getReference("Notifs").child(userid);

        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("username",username);
        hashMap.put("profile",profile);
        hashMap.put("userid",mCurrentUserId);
        hashMap.put("did",did);
        hashMap.put("war_id",war_id);
        hashMap.put("notif_time",time);
        reference.push().setValue(hashMap);

    }

    private String getContactFromStorage() {
        SharedPreferences Preferences = getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        String contact = Preferences.getString("my_phone","");
        return contact;
    }

    private void sendNotification(final String receiver, final String username, final String userprofle, final String message, final String mCurrentUserId, String type) {

        DatabaseReference tokens = FirebaseDatabase.getInstance().getReference("Tokens");
        Query query = tokens.orderByKey().equalTo(receiver);
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                for(DataSnapshot dsp : dataSnapshot.getChildren()) {
                    Token token = dsp.getValue(Token.class);
                    Data data = new Data(mCurrentUserId,userprofle,message,username,receiver,type, getContactFromStorage());

                    Sender sender = new Sender(data,token.getToken());

                    apiService.sendNotification(sender)
                            .enqueue(new Callback<MyResponse>() {
                                @Override
                                public void onResponse(Call<MyResponse> call, Response<MyResponse> response) {
                                    if(response.code() == 200) {
                                        if(response.body().success != 1){
                                            Log.i("msg","Failed");
                                        }
                                    }
                                }

                                @Override
                                public void onFailure(Call<MyResponse> call, Throwable t) {

                                }
                            });

                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    private String getLocality(Double lat, Double lon) {
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        String locality="null";
        try {
            List<Address> addresses = geocoder.getFromLocation(lat, lon, 1);
            if(addresses.size() > 0) {
                Address obj = addresses.get(0);
                locality = obj.getSubLocality();
            }
            // Toast.makeText(getActivity(), add, Toast.LENGTH_SHORT).show();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return locality;
    }

    private void changeCameraDistance() {
        int distance = 8000;
        float scale = getResources().getDisplayMetrics().density * distance;
        mCardFrontLayout.setCameraDistance(scale);
        mCardBackLayout.setCameraDistance(scale);
    }

    private void loadAnimations() {
        mSetRightOut = (AnimatorSet) AnimatorInflater.loadAnimator(this, R.animator.out_animation);
        mSetLeftIn = (AnimatorSet) AnimatorInflater.loadAnimator(this, R.animator.in_animation);
    }

    private void loadleftAnimations() {
        mSetRightOut = (AnimatorSet) AnimatorInflater.loadAnimator(this, R.animator.out_animation_left);
        mSetLeftIn = (AnimatorSet) AnimatorInflater.loadAnimator(this, R.animator.in_animation_left);
    }

    public void flipCard(View view) {
        if (!mIsBackVisible) {
            mSetRightOut.setTarget(mCardFrontLayout);
            mSetLeftIn.setTarget(mCardBackLayout);
            mSetRightOut.start();
            mSetLeftIn.start();
            mIsBackVisible = true;
        } else {
            mSetRightOut.setTarget(mCardBackLayout);
            mSetLeftIn.setTarget(mCardFrontLayout);
            mSetRightOut.start();
            mSetLeftIn.start();
            mIsBackVisible = false;
        }
    }

    private void get_receiver_contact(String receiver) {

        FirebaseDatabase.getInstance().getReference("User").child(receiver).child("contact").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                contact = snapshot.getValue(String.class);

                    if (!check_contact_availability(contact)) {
                        //Toast.makeText(dropViewActivity.this, contact, Toast.LENGTH_SHORT).show();
                        add_contact_button.setVisibility(View.VISIBLE);
                        add_reply_button.setVisibility(View.GONE);
                    }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

    }

    private void sendreply(String replytext, final String mChatUser, String image, final String username, String thumb_image, String full_image, String locality, int version) {

        if(!TextUtils.isEmpty(replytext)){

            String current_user_ref = "messages/" + mCurrentUserId + "/" + mChatUser;
            String chat_user_ref = "messages/" + mChatUser + "/" + mCurrentUserId;

            DatabaseReference user_message_push = mRootRef.child("messages").child(mCurrentUserId).child(mChatUser).push();

            String push_id = user_message_push.getKey();

            Map messageMap = new HashMap();
            messageMap.put("message", replytext);
            messageMap.put("seen", false);
            messageMap.put("type", locality);
            messageMap.put("time", ServerValue.TIMESTAMP);
            messageMap.put("from", mCurrentUserId);
            messageMap.put("thumb", image);
            messageMap.put("msg_id", push_id);
            messageMap.put("version", version);

            Map messageUserMap = new HashMap();
            messageUserMap.put(current_user_ref + "/" + push_id, messageMap);
            messageUserMap.put(chat_user_ref + "/" + push_id, messageMap);


            mRootRef.child("Chat").child(mCurrentUserId).child(mChatUser).child("seen").setValue(true);
            mRootRef.child("Chat").child(mCurrentUserId).child(mChatUser).child("timestamp").setValue(ServerValue.TIMESTAMP);

            mRootRef.child("Chat").child(mChatUser).child(mCurrentUserId).child("seen").setValue(false);
            mRootRef.child("Chat").child(mChatUser).child(mCurrentUserId).child("timestamp").setValue(ServerValue.TIMESTAMP);

            mRootRef.updateChildren(messageUserMap, new DatabaseReference.CompletionListener() {
                @Override
                public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {

                    if(databaseError != null){

                        Log.d("CHAT_LOG", databaseError.getMessage().toString());

                    }

                    else {

                        if(!image.equals("view")) {
                            Intent chatIntent = new Intent(dropViewActivity.this, ChatActivity.class);
                            chatIntent.putExtra("user_id", mChatUser);
                            chatIntent.putExtra("username", username);
                            chatIntent.putExtra("userprofile", full_image);
                            chatIntent.putExtra("userprofile_thumb", thumb_image);

                            //finish();
                            startActivity(chatIntent);
                        }

                    }

                }
            });

        }

    }

    private void sendviewMessage(String replytext, final String mChatUser, String image, String type, int version) {

        if(!TextUtils.isEmpty(replytext)){

            String current_user_ref = "messages/" + mCurrentUserId + "/" + mChatUser;
            String chat_user_ref = "messages/" + mChatUser + "/" + mCurrentUserId;

            DatabaseReference user_message_push = mRootRef.child("messages").child(mCurrentUserId).child(mChatUser).push();

            String push_id = user_message_push.getKey();

            Map messageMap = new HashMap();
            messageMap.put("message", replytext);
            messageMap.put("seen", false);
            messageMap.put("type", type);
            messageMap.put("time", ServerValue.TIMESTAMP);
            messageMap.put("from", mCurrentUserId);
            messageMap.put("thumb", image);
            messageMap.put("msg_id", push_id);
            messageMap.put("version", version);

            Map messageUserMap = new HashMap();
            messageUserMap.put(current_user_ref + "/" + push_id, messageMap);
            messageUserMap.put(chat_user_ref + "/" + push_id, messageMap);

            mRootRef.updateChildren(messageUserMap, new DatabaseReference.CompletionListener() {
                @Override
                public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {

                    if(databaseError != null){

                        Log.d("CHAT_LOG", databaseError.getMessage().toString());

                    }

                }
            });

        }

    }


    boolean check_contact_availability(String contact) {
        Cursor phones = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,null,null, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME+" ASC");
        while (phones.moveToNext()) {
            String name = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
            String phoneNumber = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
            phoneNumber = phoneNumber.replaceAll("[()\\s-]", "");
            if (phoneNumber.length() > 0) {
                if (phoneNumber.charAt(0) != '+') {
                    phoneNumber = getCountryCode(phoneNumber);
                }
                if (phoneNumber.charAt(0) == '0') {
                    phoneNumber = phoneNumber.substring(1);
                }
                if (phoneNumber.equals(contact)) {
                    return true;
                }
            }
        }
        phones.close();
        return false;
    }

    private String getCountryCode(String number) {


        PhoneNumberUtil phoneUtil = PhoneNumberUtil.createInstance(this);;
        try {
            Phonenumber.PhoneNumber NumberProto = phoneUtil.parse(number, "IN");
            if (!phoneUtil.isValidNumber(NumberProto)) {
                //Log.i("Contact","Not Valid");
                //showError(getResources().getString(R.string.wrong_phone_number));
                return number;
            }
            String regionISO = String.valueOf(NumberProto.getCountryCode());
            if(!(regionISO.matches(""))){
                //Log.i("Contact","Valid "+regionISO);
                number = "+"+regionISO+number;
            }


        } catch (NumberParseException e) {
            //Log.i("Contact","Exception: "+e.toString());
            //showError(e.toString());
            return number;
        }


        return number;
    }

    private  void addContact(String name, String contact) {
        Intent contactIntent = new Intent(ContactsContract.Intents.Insert.ACTION);
        contactIntent.setType(ContactsContract.RawContacts.CONTENT_TYPE);

        contactIntent
                .putExtra(ContactsContract.Intents.Insert.NAME, name)
                .putExtra(ContactsContract.Intents.Insert.PHONE, contact);

        startActivityForResult(contactIntent, 1);
    }

}
