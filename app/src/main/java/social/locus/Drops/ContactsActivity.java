package social.locus.Drops;

import android.database.Cursor;
import android.location.Location;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.util.Log;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import social.locus.Adapters.ContactsListadapter;
import social.locus.Adapters.SavedContactsModel;
import social.locus.Beans.User;
import social.locus.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class ContactsActivity extends AppCompatActivity {

    ArrayList<String> Contact_list = new ArrayList<>();
    ArrayList<String> Name_list = new ArrayList<>();
    ArrayList<SavedContactsModel> SavedContacts = new ArrayList<>();
    ArrayList<SavedContactsModel> reg_Contact_list = new ArrayList<>();
    ArrayList<String> reg_Contact_id_list = new ArrayList<>();
    ArrayList<User> Contact_users = new ArrayList<>();
    ArrayList<String> Pic_list = new ArrayList<>();
    RecyclerView contact_recyclerView;
    LinearLayoutManager linearLayoutManager;
    ContactsListadapter contactsListadapter;
    String contact,id,name;
    User current_user;
    FirebaseAuth auth;
    public static Button drop_button,select_all_button,deselect_all_button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contacts);
        getallContacts();
        getregcontacts();
      //  reg_Contact_list.add("123456");
        Log.i("Contacts", Contact_list.toString());
        Log.i("Name", Name_list.toString());
        auth = FirebaseAuth.getInstance();

        String memory_front = getIntent().getStringExtra("front");
        String memory_back = getIntent().getStringExtra("back");

        Log.i("Contact_check",memory_back+memory_front);

        contact_recyclerView = findViewById(R.id.contacts_recyclerview);
        drop_button = findViewById(R.id.drop_button);
        select_all_button = findViewById(R.id.select_all_button);
        deselect_all_button = findViewById(R.id.deselect_all_button);

        linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL,false);
        contactsListadapter = new ContactsListadapter(this,reg_Contact_list,Contact_users,new Location(""),memory_front,memory_back,"");
        contact_recyclerView.setLayoutManager(linearLayoutManager);
        contact_recyclerView.setAdapter(contactsListadapter);
    }

    void getallContacts() {
        Cursor phones = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,null,null, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME+" ASC");
        while (phones.moveToNext())
        {
            String name=phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
            String phoneNumber = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
            phoneNumber = phoneNumber.replaceAll("[()\\s-]+", "");
            Contact_list.add(phoneNumber);
            Name_list.add(name);
            SavedContacts.add(new SavedContactsModel(name,phoneNumber));
        }
        phones.close();
    }

    void getregcontacts() {

        final DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference("User");
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                //Contact_users.clear();
                for(DataSnapshot dsp : dataSnapshot.getChildren()) {
                    contact = String.valueOf(dsp.child("contact").getValue());
//                    User user = dsp.getValue(User.class);

                    String uid,name,username,bio,dob,mobile,profile_url,thumb_url,email,Location,online, acc_creation_time;
                            uid = String.valueOf(dsp.child("id").getValue());
                            name = String.valueOf(dsp.child("name").getValue());
                            username = String.valueOf(dsp.child("username").getValue());
                            bio = String.valueOf(dsp.child("bio").getValue());
                            dob = String.valueOf(dsp.child("birthday").getValue());
                            mobile = String.valueOf(dsp.child("contact").getValue());
                            profile_url = String.valueOf(dsp.child("profilePicURL").getValue());
                            thumb_url = String.valueOf(dsp.child("thumb_image").getValue());
                            email = String.valueOf(dsp.child("email").getValue());
                            Location = String.valueOf(dsp.child("Location").getValue());
                            online = String.valueOf(dsp.child("online").getValue());
                            acc_creation_time = String.valueOf(dsp.child("account_creation_time").getValue());
                            User user = new User(Location, bio, dob, mobile, uid, name, online, profile_url, thumb_url, username,acc_creation_time);

                   if(Contact_list.contains(contact)) {
                        reg_Contact_list.add(new SavedContactsModel(Name_list.get(Contact_list.indexOf(contact)),contact));
                        Contact_users.add(user);
//                        reg_Contact_name_list.add(name);
                    }
                }
                contactsListadapter.notifyDataSetChanged();
                getUserfromIds(reg_Contact_id_list);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    void getUserfromIds(ArrayList<String> id_list) {
        final DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference("User");
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for(DataSnapshot dsp : dataSnapshot.getChildren()) {
                    String uid,name,username,bio,dob,mobile,profile_url,thumb_url,email,Location,online;
                    uid = String.valueOf(dsp.child("id").getValue());
                    name = String.valueOf(dsp.child("name").getValue());
                    username = String.valueOf(dsp.child("username").getValue());
                    bio = String.valueOf(dsp.child("bio").getValue());
                    dob = String.valueOf(dsp.child("birthday").getValue());
                    mobile = String.valueOf(dsp.child("contact").getValue());
                    profile_url = String.valueOf(dsp.child("profilePicURL").getValue());
                    thumb_url = String.valueOf(dsp.child("thumb_image").getValue());
                    email = String.valueOf(dsp.child("email").getValue());
                    Location = String.valueOf(dsp.child("Location").getValue());
                    online = String.valueOf(dsp.child("online").getValue());
                    String acc_creation_time = String.valueOf(dsp.child("account_creation_time").getValue());
                    User user = new User(Location, bio, dob, mobile, uid, name, online, profile_url, thumb_url, username,acc_creation_time);

                    if(user.getId() != null) {
                        if (id_list.contains(user.getId()))
                            Contact_users.add(user);
                        if(user.getId().equals(auth.getCurrentUser().getUid())) {
                            String uid1,name1,username1,bio1,dob1,mobile1,profile_url1,thumb_url1,email1,Location1,online1;
                            uid1 = String.valueOf(dsp.child("id").getValue());
                            name1 = String.valueOf(dsp.child("name").getValue());
                            username1 = String.valueOf(dsp.child("username").getValue());
                            bio1 = String.valueOf(dsp.child("bio").getValue());
                            dob1 = String.valueOf(dsp.child("birthday").getValue());
                            mobile1 = String.valueOf(dsp.child("contact").getValue());
                            profile_url1 = String.valueOf(dsp.child("profilePicURL").getValue());
                            thumb_url1 = String.valueOf(dsp.child("thumb_image").getValue());

                            Location1 = String.valueOf(dsp.child("Location").getValue());
                            online1 = String.valueOf(dsp.child("online").getValue());
                            long acc_creation_time1 = (long) dsp.child("account_creation_time").getValue();
                            current_user = new User(Location, bio, dob, mobile, uid, name, online, profile_url, thumb_url, username,acc_creation_time);
                        }
                    }
                }
                contactsListadapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

}
