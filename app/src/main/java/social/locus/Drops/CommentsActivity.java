package social.locus.Drops;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.animation.AnimatorInflater;
import android.animation.AnimatorSet;
import android.content.Context;
import android.content.SharedPreferences;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import social.locus.APIService;
import social.locus.Adapters.CommentsAdapter;
import social.locus.Adapters.NotificationsAdapter;
import social.locus.Beans.CommentsModel;
import social.locus.Beans.Notif;
import social.locus.Beans.Warhead;
import social.locus.Camera.OnSwipeTouchListener;
import social.locus.Notifications.Client;
import social.locus.Notifications.Data;
import social.locus.Notifications.MyResponse;
import social.locus.Notifications.Sender;
import social.locus.Notifications.Token;
import social.locus.R;

public class CommentsActivity extends AppCompatActivity {

    APIService apiService;
    private AnimatorSet mSetRightOut;
    private AnimatorSet mSetLeftIn;
    private boolean mIsBackVisible = false;
    private View mCardFrontLayout;
    private View mCardBackLayout;
    String current_user_id = FirebaseAuth.getInstance().getCurrentUser().getUid();
    String current_username,current_userprofile= "default";
    ArrayList<CommentsModel> comments_list = new ArrayList<>();
    CommentsAdapter commentsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comments);

        setCurrentUser();

        ImageView front_image, back_image, send_comment;
        RecyclerView comments_view;
        LinearLayoutManager linearLayoutManager;
        EditText comment_editText;

        apiService = Client.getClient("https://fcm.googleapis.com/").create(APIService.class);

        front_image = findViewById(R.id.front_image);
        back_image = findViewById(R.id.back_image);
        comments_view = findViewById(R.id.comments_recyclerview);
        mCardBackLayout = findViewById(R.id.back_card);
        mCardFrontLayout = findViewById(R.id.front_card);
        comment_editText = findViewById(R.id.comment_editText);
        send_comment = findViewById(R.id.send_comment_button);

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int width = displayMetrics.widthPixels;
        int polWidth = (int) (width - (width * 0.10));
        int polHeight = (int) (width * 1.0635);
        front_image.requestLayout();
        back_image.requestLayout();
        front_image.getLayoutParams().width = polWidth/3;
        front_image.getLayoutParams().height = polHeight/3;
        back_image.getLayoutParams().width = polWidth/3;
        back_image.getLayoutParams().height = polHeight/3;

        Warhead warhead = (Warhead) getIntent().getSerializableExtra("Warhead");
        if(warhead != null) {

            Picasso.with(this).load(warhead.getImgUrl()).placeholder(R.drawable.memory_placeholder).into(front_image);
            Picasso.with(this).load(warhead.getBackimgurl()).placeholder(R.drawable.memory_placeholder).into(back_image);

            comments_view.setHasFixedSize(true);
            linearLayoutManager = new LinearLayoutManager(getApplicationContext());
            comments_view.setLayoutManager(linearLayoutManager);
            commentsAdapter = new CommentsAdapter(CommentsActivity.this,comments_list);
            comments_view.setAdapter(commentsAdapter);
            readComments(warhead.getWarid());

            send_comment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String comment = comment_editText.getText().toString().trim();
                    if(!TextUtils.isEmpty(comment)) {

                        String notif_text = " commented on your memory at " + getLocality(warhead.getDroplat(), warhead.getDroplon()) + " : " + comment;
                        setComment(current_user_id, current_username, current_userprofile, comment, warhead.getWarid(), String.valueOf(System.currentTimeMillis()));

                        if (!current_user_id.matches(warhead.getSender())) {
                            sendNotification(warhead.getSender(), current_username, current_userprofile,current_username+notif_text, current_user_id);
                            setNotifs(warhead.getSender(), current_username, current_userprofile, notif_text, warhead.getWarid(), String.valueOf(System.currentTimeMillis()));
                        }
                        comment_editText.setText("");
                        getWindow().setSoftInputMode(
                                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
                        );
                        Toast.makeText(CommentsActivity.this, "Done.", Toast.LENGTH_SHORT).show();
                    }
                }
            });

        }

        changeCameraDistance();
        mCardFrontLayout.setOnTouchListener(new OnSwipeTouchListener(this){
            @Override
            public void onSwipeLeft() {
                super.onSwipeLeft();

                if(mIsBackVisible)
                    loadAnimations();
                else
                    loadleftAnimations();

                flipCard(mCardBackLayout);
            }

            @Override
            public void onSwipeRight() {
                super.onSwipeRight();
                if(!mIsBackVisible)
                    loadAnimations();
                else
                    loadleftAnimations();
                flipCard(mCardBackLayout);
            }
        });

    }

    private void readComments(String war_id) {

        DatabaseReference reference = FirebaseDatabase.getInstance().getReference().child("Comments").child(war_id);
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                comments_list.clear();
                for(DataSnapshot dsp: dataSnapshot.getChildren()) {
                    CommentsModel notif = dsp.getValue(CommentsModel.class);
                    comments_list.add(notif);
                }
                Collections.reverse(comments_list);
                commentsAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private String getContactFromStorage() {
        SharedPreferences Preferences = getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        String contact = Preferences.getString("my_phone","");
        return contact;
    }

    private void sendNotification(final String receiver, final String username, final String userprofle, final String message, final String mCurrentUserId) {

        DatabaseReference tokens = FirebaseDatabase.getInstance().getReference("Tokens");
        Query query = tokens.orderByKey().equalTo(receiver);
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                for(DataSnapshot dsp : dataSnapshot.getChildren()) {
                    Token token = dsp.getValue(Token.class);
                    Data data = new Data(mCurrentUserId,userprofle,message,username,receiver,"comment",getContactFromStorage());

                    Sender sender = new Sender(data,token.getToken());

                    apiService.sendNotification(sender)
                            .enqueue(new Callback<MyResponse>() {
                                @Override
                                public void onResponse(Call<MyResponse> call, Response<MyResponse> response) {
                                    if(response.code() == 200) {
                                        if(response.body().success != 1){
                                            Log.i("msg","Failed");                                        }
                                    }
                                }

                                @Override
                                public void onFailure(Call<MyResponse> call, Throwable t) {

                                }
                            });

                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    private void setNotifs(String userid, String username, String profile, String did, String war_id, String time) {

        DatabaseReference reference = FirebaseDatabase.getInstance().getReference("Notifs").child(userid);

        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("username",username);
        hashMap.put("profile",profile);
        hashMap.put("userid",current_user_id);
        hashMap.put("did",did);
        hashMap.put("war_id",war_id);
        hashMap.put("notif_time",time);
        reference.push().setValue(hashMap);

    }

    private void setComment(String userid, String username, String profile, String did, String war_id, String time) {

        DatabaseReference reference = FirebaseDatabase.getInstance().getReference("Comments").child(war_id);
        String comment_id = reference.push().getKey();

        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("username",username);
        hashMap.put("profile",profile);
        hashMap.put("userid",userid);
        hashMap.put("did",did);
        hashMap.put("war_id",war_id);
        hashMap.put("notif_time",time);
        hashMap.put("comment_id",comment_id);
        reference.child(comment_id).setValue(hashMap);

    }

    private String getLocality(Double lat, Double lon) {
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        String locality=null;
        try {
            List<Address> addresses = geocoder.getFromLocation(lat, lon, 1);
            if(addresses.size() > 0) {
                Address obj = addresses.get(0);
                locality = obj.getSubLocality();
            }
            // Toast.makeText(getActivity(), add, Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return locality;
    }

    private void setCurrentUser() {
        FirebaseDatabase.getInstance().getReference("User").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                current_username = snapshot.child(current_user_id).child("username").getValue(String.class);
                if(snapshot.child(current_user_id).child("thumb_image").exists())
                    current_userprofile = String.valueOf(snapshot.child(current_user_id).child("thumb_image").getValue());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    private void changeCameraDistance() {
        int distance = 8000;
        float scale = getResources().getDisplayMetrics().density * distance;
        mCardFrontLayout.setCameraDistance(scale);
        mCardBackLayout.setCameraDistance(scale);
    }

    private void loadAnimations() {
        mSetRightOut = (AnimatorSet) AnimatorInflater.loadAnimator(this, R.animator.out_animation);
        mSetLeftIn = (AnimatorSet) AnimatorInflater.loadAnimator(this, R.animator.in_animation);
    }

    private void loadleftAnimations() {
        mSetRightOut = (AnimatorSet) AnimatorInflater.loadAnimator(this, R.animator.out_animation_left);
        mSetLeftIn = (AnimatorSet) AnimatorInflater.loadAnimator(this, R.animator.in_animation_left);
    }

    public void flipCard(View view) {
        if (!mIsBackVisible) {
            mSetRightOut.setTarget(mCardFrontLayout);
            mSetLeftIn.setTarget(mCardBackLayout);
            mSetRightOut.start();
            mSetLeftIn.start();
            mIsBackVisible = true;
        } else {
            mSetRightOut.setTarget(mCardBackLayout);
            mSetLeftIn.setTarget(mCardFrontLayout);
            mSetRightOut.start();
            mSetLeftIn.start();
            mIsBackVisible = false;
        }
    }
}