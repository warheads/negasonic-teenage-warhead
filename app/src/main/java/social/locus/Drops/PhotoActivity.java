package social.locus.Drops;

import android.Manifest;
import android.animation.AnimatorInflater;
import android.animation.AnimatorSet;
import android.app.Notification;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.Toast;

import io.michaelrocks.libphonenumber.android.NumberParseException;
import io.michaelrocks.libphonenumber.android.PhoneNumberUtil;
import io.michaelrocks.libphonenumber.android.Phonenumber;
import social.locus.Adapters.ContactsListadapter;
import social.locus.Adapters.SavedContactsModel;
import social.locus.Beans.User;
import social.locus.Beans.Warhead;
import social.locus.Camera.OnSwipeTouchListener;
import social.locus.APIService;
import social.locus.LocationUpdatesService;
import social.locus.Maps;
import social.locus.Notifications.Client;
import social.locus.Notifications.Data;
import social.locus.Notifications.MyResponse;
import social.locus.Notifications.NotificationOreo;
import social.locus.Notifications.Sender;
import social.locus.Notifications.Token;
import social.locus.R;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PhotoActivity extends AppCompatActivity {

    CardView drop_forU,drop_surprise,drop_self,drop_friends,global;
    StorageReference sref;
    FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
    String currid=null, current_contact="";
    String boxId = "";
    //String boxId = "sample_cell";
    int j=0;
    String war_id=null,quickusername,quickphoto;
    String current_user_username, current_user_thumb, self_count, global_count;
    Location warloc = new Location("dummyprovider");
    ProgressDialog progress,quickprogress;
    APIService apiService;
    String gid = FirebaseAuth.getInstance().getCurrentUser().getUid();
    NotificationOreo OnotificationManager;
    Notification.Builder Obuilder;
    NotificationManagerCompat notificationManager;
    NotificationCompat.Builder builder;
    Bitmap frontb,backb;
    private AnimatorSet mSetRightOut;
    private AnimatorSet mSetLeftIn;
    private boolean mIsBackVisible = false;
    private View mCardFrontLayout;
    private View mCardBackLayout;
    ArrayList<String> Contact_list = new ArrayList<>();
    ArrayList<String> Name_list = new ArrayList<>();
    ArrayList<SavedContactsModel> SavedContacts = new ArrayList<>();
    ArrayList<SavedContactsModel> reg_Contact_list = new ArrayList<>();
    ArrayList<String> reg_Contact_id_list = new ArrayList<>();
    ArrayList<User> Contact_users = new ArrayList<>();
    RecyclerView contact_recyclerView;
    LinearLayoutManager linearLayoutManager;
    ContactsListadapter contactsListadapter;
    String contact,id=FirebaseAuth.getInstance().getCurrentUser().getUid(),name;
    User current_user;
    FirebaseAuth auth;
    public static FloatingActionButton drop_button;
    public static CheckBox selectall_checkbox;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo);

        ActivityCompat.requestPermissions(PhotoActivity.this,
                new String[]{Manifest.permission.READ_CONTACTS},
                1);

        Contact_users.clear();
        reg_Contact_list.clear();

        selectall_checkbox=findViewById(R.id.select_all_checkbox);
        drop_button = findViewById(R.id.drop_button);

        Intent intent = getIntent();
        final String quickID = intent.getStringExtra("quickID");
        final String fromfeed = intent.getStringExtra("Direct");
        final String sself = intent.getStringExtra("sself");

        if(currentUser != null) currid = currentUser.getUid();
        auth = FirebaseAuth.getInstance();
        sref = FirebaseStorage.getInstance().getReference("Warheads");
        warloc = intent.getParcelableExtra("warloc");
        boxId = intent.getStringExtra("cell");
        //Toast.makeText(PhotoActivity.this, warloc.getLatitude()+"  "+boxId, Toast.LENGTH_SHORT).show();
        drop_self = findViewById(R.id.drop_self);
        global = findViewById(R.id.drop_global);
        progress = new ProgressDialog(PhotoActivity.this);
        quickprogress = new ProgressDialog(PhotoActivity.this);
        apiService = Client.getClient("https://fcm.googleapis.com/").create(APIService.class);


        contact_recyclerView = findViewById(R.id.contacts_recyclerview);


        ImageView front = findViewById(R.id.front);
        ImageView back = findViewById(R.id.back);
        mCardFrontLayout = findViewById(R.id.front_card);
        mCardBackLayout = findViewById(R.id.back_card);

        changeCameraDistance();

        String filename = getIntent().getStringExtra("front");
        String filename2 = getIntent().getStringExtra("back");

        try {
            FileInputStream is = this.openFileInput(filename);
            FileInputStream is2 = this.openFileInput(filename2);
            frontb = BitmapFactory.decodeStream(is);
            backb = BitmapFactory.decodeStream(is2);

            Bitmap smallfrontb = Bitmap.createScaledBitmap(frontb,frontb.getWidth()*2/5,frontb.getHeight()*2/5,true);
            Bitmap smallbackb = Bitmap.createScaledBitmap(backb,backb.getWidth()*2/5,backb.getHeight()*2/5,true);

            front.setImageBitmap(smallfrontb);
            back.setImageBitmap(smallbackb);

            is.close();
            is2.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

//        final Uri imgpost = getImageUri(this,frontb);
//        final Uri imgpost2 = getImageUri(this,backb);

        File dir = new File(Environment.DIRECTORY_PICTURES);
        if (dir.isDirectory())
        {
            String[] children = dir.list();
            if (children != null) {
                Arrays.stream(children).forEach(child -> new File(dir, child).delete());
            }
        }

        File file1 = new File(getExternalFilesDir(Environment.DIRECTORY_PICTURES),
                "LOCUS_DROP_IMAGE_FRONT"+ System.currentTimeMillis() +".enc");
        File file2 = new File(getExternalFilesDir(Environment.DIRECTORY_PICTURES),
                "LOCUS_DROP_IMAGE_BACK"+ System.currentTimeMillis() +".enc");

        persistImage(file1,frontb);
        persistImage(file2,backb);

        final Uri imgpost = Uri.fromFile(file1);
        final Uri imgpost2 = Uri.fromFile(file2);

        mCardFrontLayout.setOnTouchListener(new OnSwipeTouchListener(this){
            @Override
            public void onSwipeLeft() {
                super.onSwipeLeft();

                if(mIsBackVisible)
                    loadAnimations();
                else
                    loadleftAnimations();

                flipCard(mCardBackLayout);
            }

            @Override
            public void onSwipeRight() {
                super.onSwipeRight();
                if(!mIsBackVisible)
                    loadAnimations();
                else
                    loadleftAnimations();
                flipCard(mCardBackLayout);
            }
        });



        if(quickID != null && !quickID.matches("")) {
            quickdrop(imgpost,imgpost2,quickID);
        }

        if(fromfeed != null) {
            globaldrop(imgpost, imgpost2);
        }

        if(sself != null) {
            selfdrop(imgpost, imgpost2);
        }

        FirebaseDatabase.getInstance().getReference("User").child(id).child("contact").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                current_contact = dataSnapshot.getValue(String.class);

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        global.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                globaldrop(imgpost, imgpost2);

            }
        });

        drop_self.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selfdrop(imgpost, imgpost2);
            }
        });

        linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL,false);
        contactsListadapter = new ContactsListadapter(this,reg_Contact_list,Contact_users,warloc,
                imgpost.toString(),imgpost2.toString(),boxId);
        contact_recyclerView.setLayoutManager(linearLayoutManager);
        contact_recyclerView.setAdapter(contactsListadapter);
        contactsListadapter.notifyDataSetChanged();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 1: {

                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    getallContacts();
                    getregcontacts();

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Toast.makeText(PhotoActivity.this, "Permission denied to read your Contacts", Toast.LENGTH_SHORT).show();
                }
                return;
            }
        }
    }

    private void persistImage(File file, Bitmap bitmap) {

        OutputStream os;
        try {
            //Toast.makeText(PhotoActivity.this, "persistimage", Toast.LENGTH_SHORT).show();
            os = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, os);
            os.flush();
            os.close();
        } catch (Exception e) {
            Log.e(" Writing bitmap: ", "Error writing bitmap", e);
        }
    }

    //getting real path from uri
    private String getFilePath(Uri uri) {
        String[] projection = {MediaStore.Images.Media.DATA};

        Cursor cursor = getContentResolver().query(uri, projection, null, null, null);
        if (cursor != null) {
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(projection[0]);
            String picturePath = cursor.getString(columnIndex); // returns null
            cursor.close();
            return picturePath;
        }
        return null;
    }

    void getallContacts() {
        Cursor phones = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,null,null, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME+" ASC");
        while (phones.moveToNext())
        {
            String name=phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
            String phoneNumber = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
            phoneNumber = phoneNumber.replaceAll("[()\\s-]+", "");
            if(phoneNumber.length() > 0) {
                if (phoneNumber.charAt(0) != '+') {
                    phoneNumber = getCountryCode(phoneNumber);
                }
                if (phoneNumber.charAt(0) == '0') {
                    phoneNumber = phoneNumber.substring(1);
                }
                if (!phoneNumber.equals(geContactFromStorage())) {
                    Contact_list.add(phoneNumber);
                    Name_list.add(name);
                    Log.i("Contact", name + " " + phoneNumber);
                    SavedContacts.add(new SavedContactsModel(name, phoneNumber));
                }
            }
        }
        phones.close();
    }

    private String geContactFromStorage() {
        SharedPreferences Preferences = getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        String contact = Preferences.getString("my_phone","");
        return contact;
    }

    private String getCountryCode(String number) {


        PhoneNumberUtil phoneUtil = PhoneNumberUtil.createInstance(this);;
        try {
            Phonenumber.PhoneNumber NumberProto = phoneUtil.parse(number, "IN");
            if (!phoneUtil.isValidNumber(NumberProto)) {
                //Log.i("Contact","Not Valid");
                //showError(getResources().getString(R.string.wrong_phone_number));
                return number;
            }
            String regionISO = String.valueOf(NumberProto.getCountryCode());
            if(!(regionISO.matches(""))){
                //Log.i("Contact","Valid "+regionISO);
                number = "+"+regionISO+number;
            }


        } catch (NumberParseException e) {
            //Log.i("Contact","Exception: "+e.toString());
            //showError(e.toString());
            return number;
        }


        return number;
    }

    void getregcontacts() {

        final DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference("User");
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Contact_users.clear();
                reg_Contact_list.clear();
                for(DataSnapshot dsp : dataSnapshot.getChildren()) {

                    contact = String.valueOf(dsp.child("contact").getValue());
//                    User user = dsp.getValue(User.class);

                    String uid,name,username,bio,dob,mobile,profile_url,thumb_url,email,Location,online, acc_creation_time;
                    uid = String.valueOf(dsp.child("id").getValue());
                    name = String.valueOf(dsp.child("name").getValue());
                    username = String.valueOf(dsp.child("username").getValue());
                    bio = String.valueOf(dsp.child("bio").getValue());
                    dob = String.valueOf(dsp.child("birthday").getValue());
                    mobile = String.valueOf(dsp.child("contact").getValue());
                    profile_url = String.valueOf(dsp.child("profilePicURL").getValue());
                    thumb_url = String.valueOf(dsp.child("thumb_image").getValue());
                    email = String.valueOf(dsp.child("email").getValue());
                    Location = String.valueOf(dsp.child("Location").getValue());
                    online = String.valueOf(dsp.child("online").getValue());
                    acc_creation_time = String.valueOf(dsp.child("account_creation_time").getValue());
                    User user = new User(Location, bio, dob, mobile, uid, name, online, profile_url, thumb_url, username,acc_creation_time);

                    if(Contact_list.contains(contact)) {
                        reg_Contact_list.add(new SavedContactsModel(Name_list.get(Contact_list.indexOf(contact)),contact));
                        Contact_users.add(user);
//                        reg_Contact_name_list.add(name);
                    }
                }
                contactsListadapter.notifyDataSetChanged();
                getUserfromIds(reg_Contact_id_list);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    void getUserfromIds(ArrayList<String> id_list) {
        final DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference("User");
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for(DataSnapshot dsp : dataSnapshot.getChildren()) {
                    String uid,name,username,bio,dob,mobile,profile_url,thumb_url,email,Location,online;
                    uid = String.valueOf(dsp.child("id").getValue());
                    name = String.valueOf(dsp.child("name").getValue());
                    username = String.valueOf(dsp.child("username").getValue());
                    bio = String.valueOf(dsp.child("bio").getValue());
                    dob = String.valueOf(dsp.child("birthday").getValue());
                    mobile = String.valueOf(dsp.child("contact").getValue());
                    profile_url = String.valueOf(dsp.child("profilePicURL").getValue());
                    thumb_url = String.valueOf(dsp.child("thumb_image").getValue());
                    email = String.valueOf(dsp.child("email").getValue());
                    Location = String.valueOf(dsp.child("Location").getValue());
                    online = String.valueOf(dsp.child("online").getValue());
                    String acc_creation_time = String.valueOf(dsp.child("account_creation_time").getValue());
                    User user = new User(Location, bio, dob, mobile, uid, name, online, profile_url, thumb_url, username,acc_creation_time);

                    if(user.getId() != null) {
                        if (id_list.contains(user.getId()))
                            Contact_users.add(user);
                        if(user.getId().equals(auth.getCurrentUser().getUid())) {
                            String uid1,name1,username1,bio1,dob1,mobile1,profile_url1,thumb_url1,email1,Location1,online1;
                            uid1 = String.valueOf(dsp.child("id").getValue());
                            name1 = String.valueOf(dsp.child("name").getValue());
                            username1 = String.valueOf(dsp.child("username").getValue());
                            bio1 = String.valueOf(dsp.child("bio").getValue());
                            dob1 = String.valueOf(dsp.child("birthday").getValue());
                            mobile1 = String.valueOf(dsp.child("contact").getValue());
                            profile_url1 = String.valueOf(dsp.child("profilePicURL").getValue());
                            thumb_url1 = String.valueOf(dsp.child("thumb_image").getValue());
                            Location1 = String.valueOf(dsp.child("Location").getValue());
                            online1 = String.valueOf(dsp.child("online").getValue());
                            String acc_creation_time1 = String.valueOf(dsp.child("account_creation_time").getValue());
                            current_user = new User(Location, bio, dob, mobile, uid, name, online, profile_url, thumb_url, username,acc_creation_time);
                        }
                    }
                }
                contactsListadapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void quickdrop(Uri imgpost,final Uri imgpost2, final String quickID) {
        final StorageReference filepath = sref.child(currid).child(imgpost.getLastPathSegment());
        final StorageReference filepath2 = sref.child(currid).child(imgpost2.getLastPathSegment());
        final DatabaseReference reference = FirebaseDatabase.getInstance().getReference("User");

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            OnotificationManager = new NotificationOreo(getBaseContext());
            Obuilder = OnotificationManager.getONotification("Drop", "Setting up the memory...");
            OnotificationManager.getManager().notify(2, Obuilder.build());
        }
        else {
            notificationManager = NotificationManagerCompat.from(PhotoActivity.this);
            builder = new NotificationCompat.Builder(PhotoActivity.this, "Drop");
            builder.setContentTitle("Drop")
                    .setContentText("Setting up the memory...")
                    .setSmallIcon(R.drawable.ic_stat_name)
                    .setPriority(NotificationCompat.PRIORITY_LOW);
            notificationManager.notify(2, builder.build());
        }

        Toast.makeText(getApplicationContext(), "Check the progress in the notification bar.", Toast.LENGTH_LONG).show();

        Intent intent = new Intent(getApplicationContext(), Maps.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();

        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                current_user_username = String.valueOf(dataSnapshot.child(currid).child("username").getValue());
                current_user_thumb = String.valueOf(dataSnapshot.child(currid).child("thumb_image").getValue());
                quickusername = String.valueOf(dataSnapshot.child(quickID).child("username").getValue());
                quickphoto = String.valueOf(dataSnapshot.child(quickID).child("thumb_image").getValue());

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        filepath.putFile(imgpost).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onProgress(@NonNull UploadTask.TaskSnapshot taskSnapshot) {
                filepath.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                    @Override
                    public void onSuccess(Uri uri) {

                        String url_1 = uri.toString();

                        filepath2.putFile(imgpost2).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                            @Override
                            public void onProgress(@NonNull UploadTask.TaskSnapshot taskSnapshot) {

                                double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();

                                Obuilder.setContentText("Dropping...");
                                Obuilder.setProgress(100, (int) progress, false);
                                OnotificationManager.getManager().notify(2, Obuilder.build());

                                if ((int) progress == 100) {
                                    filepath2.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                        @Override
                                        public void onSuccess(Uri uri) {

                                            String url_2 = uri.toString();

                                                DatabaseReference newref = FirebaseDatabase.getInstance().getReference();
                                                DatabaseReference ref = newref.child("foryou").child(quickID).child("received").child(boxId);
                                                String war_id = ref.push().getKey();
                                                Warhead warrr = new Warhead(url_1, " ", quickID, currid, current_user_username,
                                                        current_user_thumb, String.valueOf(System.currentTimeMillis()), warloc.getLatitude(), warloc.getLongitude()
                                                        , "forU", war_id, false, "none", 0, url_1);
                                                ref.child(war_id).setValue(warrr);

                                                newref.child("foryou_list").child(quickID).child("received").child(war_id).setValue(warrr).addOnSuccessListener(new OnSuccessListener<Void>() {
                                                    @Override
                                                    public void onSuccess(Void unused) {
                                                        DatabaseReference ref = newref.child("foryou").child(quickID).child("received").child(boxId);
                                                        DatabaseReference self_foryou_ref = newref.child("foryou").child(currid).child("sent").child(boxId);
                                                        ref.child(war_id).child("backimgurl").setValue(url_2);
                                                        warrr.setBackimgurl(url_2);
                                                        warrr.setType("sent");
                                                        warrr.setSenderimage(quickphoto);
                                                        warrr.setSendername(quickusername);
                                                        self_foryou_ref.child(war_id).setValue(warrr);
                                                        newref.child("foryou_list").child(quickID).child("received").child(war_id).child("backimgurl")
                                                                .setValue(url_2).addOnSuccessListener(new OnSuccessListener<Void>() {
                                                            @Override
                                                            public void onSuccess(Void unused) {
                                                                Log.i("uri_back",url_2);
                                                                String locality = getLocality(warloc.getLatitude(), warloc.getLongitude());
                                                                if(locality != null) {
                                                                    sendreply(current_user_username+" dropped a memory in " + getLocality(warloc.getLatitude(), warloc.getLongitude())
                                                                            ,quickID,current_user_username, 1);
                                                                    sendNotification(quickID, current_user_username, current_user_thumb,
                                                                            current_user_username+" dropped a memory ForYou in " + getLocality(warloc.getLatitude()
                                                                                    , warloc.getLongitude()), currid);
                                                                    setNotifs(quickID, current_user_username, current_user_thumb,
                                                                            " dropped a memory ForYou in " + getLocality(warloc.getLatitude(), warloc.getLongitude()), war_id, String.valueOf(System.currentTimeMillis()));
                                                                }
                                                                else {
                                                                    sendreply(current_user_username+" dropped a memory for you."
                                                                            ,quickID,current_user_username, 1);
                                                                    sendNotification(quickID, current_user_username, current_user_thumb,
                                                                            current_user_username+" dropped a memory ForYou.", currid);
                                                                    setNotifs(quickID, current_user_username, current_user_thumb,
                                                                            " dropped a memory ForYou.", war_id, String.valueOf(System.currentTimeMillis()));

                                                                }
                                                            }
                                                        });
                                                    }
                                                });

                                            Obuilder.setContentText("ForYou Dropped ")
                                                    .setProgress(0,0,false)
                                                    .setSmallIcon(R.drawable.complete);
                                            OnotificationManager.getManager().notify(2, Obuilder.build());
                                            Toast.makeText(PhotoActivity.this, "ForYou dropped!", Toast.LENGTH_SHORT).show();
                                        }
                                    });
                                }

                            }
                        });

                    }
                });
            }
        });
    }

    private void sendreply(String replytext, final String mChatUser, final String image, final int version) {

        if(!TextUtils.isEmpty(replytext)){
            //Toast.makeText(mContext, "innnnn", Toast.LENGTH_SHORT).show();
            DatabaseReference mRootRef = FirebaseDatabase.getInstance().getReference();

            String current_user_ref = "messages/" + currid + "/" + mChatUser;
            String chat_user_ref = "messages/" + mChatUser + "/" + currid;

            DatabaseReference user_message_push = mRootRef.child("messages").child(currid).child(mChatUser).push();
            String push_id = user_message_push.getKey();

            Map messageMap = new HashMap();
            messageMap.put("message", replytext);
            messageMap.put("seen", false);
            messageMap.put("type", "drop");
            messageMap.put("time", ServerValue.TIMESTAMP);
            messageMap.put("from", currid);
            messageMap.put("thumb", image);
            messageMap.put("msg_id", push_id);
            messageMap.put("version", version);

            Map messageUserMap = new HashMap();
            messageUserMap.put(current_user_ref + "/" + push_id, messageMap);
            messageUserMap.put(chat_user_ref + "/" + push_id, messageMap);

            mRootRef.updateChildren(messageUserMap, new DatabaseReference.CompletionListener() {
                @Override
                public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {

                    if(databaseError != null){

                        Log.d("CHAT_LOG", databaseError.getMessage().toString());

                    }

                    else {

                    }

                }
            });
        }

    }


    private void forUdrop(Uri imgpost, Uri imgpost2) {
        Intent i = new Intent(PhotoActivity.this,ContactsActivity.class);
        i.putExtra("front",imgpost.toString());
        i.putExtra("back",imgpost2.toString());
        startActivity(i);
    }

    private void selfdrop(final Uri imgpost, final Uri imgpost2) {

        final StorageReference filepath = sref.child(currid).child(imgpost.getLastPathSegment());
        final StorageReference filepath2 = sref.child(currid).child(imgpost2.getLastPathSegment());
        final DatabaseReference reference = FirebaseDatabase.getInstance().getReference("User");

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            OnotificationManager = new NotificationOreo(getBaseContext());
            Obuilder = OnotificationManager.getONotification("Drop", "Setting up the memory...");
            OnotificationManager.getManager().notify(2, Obuilder.build());
        }
        else {
            notificationManager = NotificationManagerCompat.from(PhotoActivity.this);
            builder = new NotificationCompat.Builder(PhotoActivity.this, "Drop");
            builder.setContentTitle("Drop")
                    .setContentText("Setting up the memory...")
                    .setSmallIcon(R.drawable.ic_stat_name)
                    .setPriority(NotificationCompat.PRIORITY_LOW);
            notificationManager.notify(2, builder.build());
        }

        Toast.makeText(getApplicationContext(), "Check the progress in the notification bar.", Toast.LENGTH_LONG).show();

        Intent intent = new Intent(getApplicationContext(), Maps.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();


        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                current_user_username = String.valueOf(dataSnapshot.child(gid).child("username").getValue());
                current_user_thumb = String.valueOf(dataSnapshot.child(gid).child("thumb_image").getValue());
                self_count = String.valueOf(dataSnapshot.child(gid).child("self_count").getValue());
                if(!dataSnapshot.child(gid).child("self_count").exists()) self_count = "0";

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        filepath.putFile(imgpost).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {

                filepath.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                    @Override
                    public void onSuccess(Uri uri) {
                        Log.i("uri_front",uri.toString());
                        DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child("self").child(currid).child(boxId);
                        war_id = ref.push().getKey();
                        Warhead warrr = new Warhead(uri.toString()," ", "caption", gid, current_user_username, current_user_thumb
                                , String.valueOf(System.currentTimeMillis()), warloc.getLatitude(), warloc.getLongitude()
                                , "self", war_id, false, "none", 0, uri.toString());
                        ref.child(war_id).setValue(warrr).addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {

                                filepath2.putFile(imgpost2).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                                    @Override
                                    public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {

                                        double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
                                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                                            Obuilder.setContentText("Dropping...");
                                            Obuilder.setProgress(100, (int) progress, false);
                                            OnotificationManager.getManager().notify(2, Obuilder.build());
                                        }
                                        else {
                                            builder.setProgress(100, (int) progress, false);
                                            notificationManager.notify(2, builder.build());
                                        }

                                        if((int) progress == 100) {
                                            filepath2.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                                @Override
                                                public void onSuccess(Uri uri) {
                                                    String url = uri.toString();
                                                    Log.i("uri_back",uri.toString());
                                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                                                        Obuilder.setContentText("Memory dropped.")
                                                                .setProgress(0,0,false)
                                                                .setSmallIcon(R.drawable.complete);
                                                        OnotificationManager.getManager().notify(2, Obuilder.build());
                                                    }
                                                    else {
                                                        builder.setContentText("Memory dropped.")
                                                                .setProgress(0,0,false)
                                                                .setSmallIcon(R.drawable.complete);
                                                        notificationManager.notify(2, builder.build());
                                                    }

                                                    DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child("self").child(currid).child(boxId);
                                                    ref.child(war_id).child("backimgurl").setValue(url).addOnSuccessListener(new OnSuccessListener<Void>() {
                                                        @Override
                                                        public void onSuccess(Void aVoid) {
                                                            reference.child(currid).child("self_count").setValue(String.valueOf(Integer.parseInt(self_count)+1));
                                                            Toast.makeText(getApplicationContext(), "Memory dropped!!", Toast.LENGTH_SHORT).show();
                                                        }
                                                    });
                                                }
                                            });
                                        }
                                    }
                                });
                            }
                        });
                    }
                });
            }
        });
    }

    private void globaldrop(final Uri imgpost, final Uri imgpost2) {

        final DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference("User");
        final DatabaseReference dref = FirebaseDatabase.getInstance().getReference("TeenageWarheads");
        final StorageReference filepath = sref.child(currid).child(imgpost.getLastPathSegment());
        final StorageReference filepath2 = sref.child(currid).child(imgpost2.getLastPathSegment());

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            OnotificationManager = new NotificationOreo(getBaseContext());
            Obuilder = OnotificationManager.getONotification("Drop", "Setting up the memory...");
            OnotificationManager.getManager().notify(2, Obuilder.build());
        }
        else {
            notificationManager = NotificationManagerCompat.from(PhotoActivity.this);
            builder = new NotificationCompat.Builder(PhotoActivity.this, "Drop");
            builder.setContentTitle("Drop")
                    .setContentText("Setting up the memory...")
                    .setSmallIcon(R.drawable.ic_stat_name)
                    .setPriority(NotificationCompat.PRIORITY_LOW);
            notificationManager.notify(2, builder.build());
        }

        Toast.makeText(getApplicationContext(), "Check the progress in the notification bar.", Toast.LENGTH_LONG).show();

        Intent intent = new Intent(getApplicationContext(), Maps.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();

        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                current_user_username = String.valueOf(dataSnapshot.child(gid).child("username").getValue());
                current_user_thumb = String.valueOf(dataSnapshot.child(gid).child("thumb_image").getValue());
                global_count = String.valueOf(dataSnapshot.child(gid).child("global_count").getValue());
                if(!dataSnapshot.child(gid).child("global_count").exists()) global_count = "0";

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        filepath.putFile(imgpost).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {

                filepath.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                    @Override
                    public void onSuccess(Uri uri) {
                        final String url = uri.toString();
                        Log.i("uri_front",uri.toString());
                        DatabaseReference reference = FirebaseDatabase.getInstance().getReference("TeenageWarheads").child(boxId);
                        war_id = reference.push().getKey();
                        Warhead warrr = new Warhead(url," ", "caption", gid, current_user_username, current_user_thumb
                                , String.valueOf(System.currentTimeMillis()), warloc.getLatitude(), warloc.getLongitude()
                                , "global", war_id, false, "none", 0, "thumb");
                        dref.child(boxId).child(war_id).setValue(warrr).addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {

                                filepath2.putFile(imgpost2).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                                    @Override
                                    public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {

                                        double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
                                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                                            Obuilder.setProgress(100, (int) progress, false);
                                            Obuilder.setContentText("Dropping...");
                                            OnotificationManager.getManager().notify(2, Obuilder.build());
                                        }
                                        else {
                                            builder.setProgress(100, (int) progress, false);
                                            notificationManager.notify(2, builder.build());
                                        }

                                        if((int) progress == 100) {
                                            filepath2.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                                @Override
                                                public void onSuccess(Uri uri) {
                                                    final String url = uri.toString();
                                                    Log.i("uri_back",uri.toString());
                                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                                                        Obuilder.setContentText("Global dropped.")
                                                                .setProgress(0, 0, false)
                                                                .setSmallIcon(R.drawable.complete);
                                                        OnotificationManager.getManager().notify(2, Obuilder.build());
                                                    }
                                                    else {
                                                        builder.setContentText("Global dropped.")
                                                                .setProgress(0, 0, false)
                                                                .setSmallIcon(R.drawable.complete);
                                                        notificationManager.notify(2, builder.build());
                                                    }
                                                    dref.child(boxId).child(war_id).child("backimgurl").setValue(url).addOnSuccessListener(new OnSuccessListener<Void>() {
                                                        @Override
                                                        public void onSuccess(Void aVoid) {
                                                            FirebaseDatabase.getInstance().getReference("User").child(currid).child("global_count")
                                                                    .setValue(String.valueOf(Integer.parseInt(global_count)+1));
                                                            FirebaseDatabase.getInstance().getReference("global_list").child(currid)
                                                                    .child(war_id).child("war_id").setValue(war_id);
                                                            FirebaseDatabase.getInstance().getReference("global_list").child(currid)
                                                                    .child(war_id).child("cell").setValue(boxId);

                                                            Toast.makeText(getApplicationContext(), "Global Dropped...", Toast.LENGTH_SHORT).show();
                                                        }

                                                    });

                                                }
                                            });
                                        }
                                    }
                                });
                            }

                        });

                    }
                });
            }
        });

    }

    private void setNotifs(String userid, String username, String profile, String did, String war_id, String time) {

        DatabaseReference reference = FirebaseDatabase.getInstance().getReference("Notifs").child(userid);

        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("username",username);
        hashMap.put("profile",profile);
        hashMap.put("userid",currid);
        hashMap.put("did",did);
        hashMap.put("war_id",war_id);
        hashMap.put("notif_time",time);
        reference.push().setValue(hashMap);

    }

    private String getLocality(Double lat, Double lon) {
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        String locality=null;
        try {
            List<Address> addresses = geocoder.getFromLocation(lat, lon, 1);
            if(addresses.size() > 0) {
                Address obj = addresses.get(0);
                locality = obj.getSubLocality();
            }

//            add = add + "\n" + obj.getCountryName();
//            add = add + "\n" + obj.getCountryCode();
//            add = add + "\n" + obj.getAdminArea();
//            add = add + "\n" + obj.getPostalCode();
//            add = add + "\n" + obj.getSubAdminArea();
//            add = add + "\n" + obj.getLocality();
//            add = add + "\n" + obj.getSubThoroughfare();
//            add = add + "\n" + obj.getFeatureName();
//            add = add + "\n" + obj.getPhone();
//            add = add + "\n" + obj.getPremises();
//            add = add + "\n" + obj.getSubLocality();
//            add = add + "\n" + obj.getThoroughfare();
//            add = add + "\n" + obj.getLocale();
//            add = add + "\n" + obj.getUrl();
//            add = add + "\n" + obj.describeContents();
//            add = add + "\n" + obj.getExtras();

        } catch (Exception e) {
            e.printStackTrace();
        }

            return locality;

    }

    private String getContactFromStorage() {
        SharedPreferences Preferences = getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        String contact = Preferences.getString("my_phone","");
        return contact;
    }

    private void sendNotification(final String receiver, final String username, final String userprofle, final String message, final String mCurrentUserId) {

        DatabaseReference tokens = FirebaseDatabase.getInstance().getReference("Tokens");
        Query query = tokens.orderByKey().equalTo(receiver);
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                for(DataSnapshot dsp : dataSnapshot.getChildren()) {
                    Token token = dsp.getValue(Token.class);
                    Data data = new Data(mCurrentUserId,userprofle,message,username,receiver,"memory_drop", getContactFromStorage());

                    Sender sender = new Sender(data,token.getToken());

                    apiService.sendNotification(sender)
                            .enqueue(new Callback<MyResponse>() {
                                @Override
                                public void onResponse(Call<MyResponse> call, Response<MyResponse> response) {
                                    if(response.code() == 200) {
                                        if(response.body().success != 1){
                                            Log.i("msg","Failed");
                                        }
                                    }
                                }

                                @Override
                                public void onFailure(Call<MyResponse> call, Throwable t) {

                                }
                            });

                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "temp",null);
        return Uri.parse(path);
    }

    private void changeCameraDistance() {
        int distance = 8000;
        float scale = getResources().getDisplayMetrics().density * distance;
        mCardFrontLayout.setCameraDistance(scale);
        mCardBackLayout.setCameraDistance(scale);
    }

    private void loadAnimations() {
        mSetRightOut = (AnimatorSet) AnimatorInflater.loadAnimator(this, R.animator.out_animation);
        mSetLeftIn = (AnimatorSet) AnimatorInflater.loadAnimator(this, R.animator.in_animation);
    }

    private void loadleftAnimations() {
        mSetRightOut = (AnimatorSet) AnimatorInflater.loadAnimator(this, R.animator.out_animation_left);
        mSetLeftIn = (AnimatorSet) AnimatorInflater.loadAnimator(this, R.animator.in_animation_left);
    }

    public void flipCard(View view) {
        if (!mIsBackVisible) {
            mSetRightOut.setTarget(mCardFrontLayout);
            mSetLeftIn.setTarget(mCardBackLayout);
            mSetRightOut.start();
            mSetLeftIn.start();
            mIsBackVisible = true;

        } else {
            mSetRightOut.setTarget(mCardBackLayout);
            mSetLeftIn.setTarget(mCardFrontLayout);
            mSetRightOut.start();
            mSetLeftIn.start();
            mIsBackVisible = false;
        }
    }

}
