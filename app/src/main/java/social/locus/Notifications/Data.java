package social.locus.Notifications;

public class Data {

    private String user;
    private String icon;
    private String body,title,sented,notifytype, sender_contact;

    public Data() {
    }

    public Data(String user, String icon, String body, String title, String sented, String notifytype, String sender_contact) {
        this.user = user;
        this.icon = icon;
        this.body = body;
        this.title = title;
        this.sented = sented;
        this.notifytype = notifytype;
        this.sender_contact = sender_contact;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getSender_contact() {
        return sender_contact;
    }

    public void setSender_contact(String sender_contact) {
        this.sender_contact = sender_contact;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getBody() {
        return body;
    }

    public String getNotifytype() {
        return notifytype;
    }

    public void setNotifytype(String notifytype) {
        this.notifytype = notifytype;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSented() {
        return sented;
    }

    public void setSented(String sented) {
        this.sented = sented;
    }
}
