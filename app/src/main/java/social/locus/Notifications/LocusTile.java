package social.locus.Notifications;

import static social.locus.LocationUpdatesService.isRunning;

import android.content.Intent;
import android.service.quicksettings.Tile;
import android.service.quicksettings.TileService;
import android.util.Log;

import social.locus.LocationUpdatesService;

public class LocusTile extends TileService {
    private static final String PACKAGE_NAME =
            "social.locus.Fragments.locationupdatesforegroundservice";
    private static final String EXTRA_REMOVE_BACKGROUND_UPDATES = PACKAGE_NAME +
            ".remove_background_updates";
    private static final String EXTRA_START_BACKGROUND_UPDATES = PACKAGE_NAME +
            ".start_background_updates";
    @Override
    public void onTileAdded() {
        super.onTileAdded();
        Log.i("TileService", "onTileAdded");
    }

    @Override
    public void onTileRemoved() {
        super.onTileRemoved();
        Log.i("TileService", "onTileRemoved");
    }

    @Override
    public void onStartListening() {
        super.onStartListening();
        Tile tile = this.getQsTile();
        if(isRunning){
            tile.setState(Tile.STATE_ACTIVE);
        }
        else{
            tile.setState(Tile.STATE_INACTIVE);
        }
        tile.updateTile();
        Log.i("TileService", "onStartListening");
    }

    @Override
    public void onStopListening() {
        super.onStopListening();
        Log.i("TileService", "onStopListening");
    }

    @Override
    public void onClick() {
        super.onClick();
        Tile tile = this.getQsTile();
        Intent intent = new Intent(this, LocationUpdatesService.class);
        Log.i("TileService", "onClick");
        if (!isRunning) {
            intent.putExtra(EXTRA_START_BACKGROUND_UPDATES, true);
            tile.setState(Tile.STATE_ACTIVE);
        }
        else if(isRunning){
            intent.putExtra(EXTRA_REMOVE_BACKGROUND_UPDATES, true);
            tile.setState(Tile.STATE_INACTIVE);
        }
        startForegroundService(intent);
        tile.updateTile();

    }
}
