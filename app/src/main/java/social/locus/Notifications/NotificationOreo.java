package social.locus.Notifications;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.ContextWrapper;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.media.AudioAttributes;
import android.net.Uri;
import android.os.Build;
import androidx.annotation.RequiresApi;
import android.util.Base64;
import android.util.Log;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import social.locus.R;

public class NotificationOreo extends ContextWrapper {

    public static final String CHANNEL_ID = "Messaging";
    public static final String CHANNEL_NAME = "Messaging";

    private NotificationManager notificationManager;

    public NotificationOreo(Context base) {
        super(base);

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            createChannel(CHANNEL_ID, CHANNEL_NAME);
        }

    }

    @TargetApi(Build.VERSION_CODES.O)
    private void createChannel(String CHANNEL_ID, String CHANNEL_NAME) {
        Uri uri= Uri.parse("android.resource://"+getPackageName()+"/raw/send");
        AudioAttributes att = new AudioAttributes.Builder()
                .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                .setContentType(AudioAttributes.CONTENT_TYPE_SPEECH)
                .build();
        NotificationChannel channel = new NotificationChannel(CHANNEL_ID,CHANNEL_NAME, NotificationManager.IMPORTANCE_HIGH);
        channel.enableVibration(true);
        channel.enableLights(true);
        channel.setSound(uri,att);
        channel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);

            getManager().createNotificationChannel(channel);
    }

    public NotificationManager getManager() {

        if(notificationManager == null) {

            notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        }
        return notificationManager;
    }

    @TargetApi(Build.VERSION_CODES.O)
    public Notification.Builder getOreoNotification(String title, String body, PendingIntent pendingIntent, Uri sounduri, String icon) {

        Notification.Builder builder = new Notification.Builder(getApplicationContext(),CHANNEL_ID)
                .setContentTitle(title)
                .setContentText(body)
                .setAutoCancel(true)
                .setContentIntent(pendingIntent)
                .setSmallIcon(R.drawable.ic_stat_name);

//        if(!(icon.equals("default"))){
//            Picasso.with(this)
//                    .load(icon)
//                    .into(new Target() {
//                        @Override
//                        public void onBitmapLoaded (final Bitmap bitmap, Picasso.LoadedFrom from){
//                            builder.setLargeIcon(bitmap);
//                            Log.i("NOTIFY BITMAP","BITMAP MADE");
//                        }
//
//                        @Override
//                        public void onBitmapFailed(Drawable errorDrawable) {
//                            Log.e("NOTIFY BITMAP","BITMAP FAILED");
//                        }
//
//                        @Override
//                        public void onPrepareLoad(Drawable placeHolderDrawable) {
//
//                        }
//                    });
//        } else
            builder.setLargeIcon(BitmapFactory.decodeResource(this.getResources(),R.drawable.ic_dp_icon_male_04));

        return builder;

    }

    public Bitmap StringToBitMap(String encodedString){
        if(encodedString.equals("default")) return BitmapFactory.decodeResource(this.getResources(),R.drawable.ic_dp_icon_male_04);

        else {
            try {
                byte[] encodeByte = Base64.decode(encodedString, Base64.DEFAULT);
                Bitmap bitmap = BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length);
                return bitmap;
            } catch (Exception e) {
                e.getMessage();
                return null;
            }
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public Notification.Builder getONotification(String drop, String upload_in_progress) {

        String CHANNEL_ID = "Dropping";

            Uri uri= Uri.parse("android.resource://"+getPackageName()+"/raw/send");
            AudioAttributes att = new AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                    .setContentType(AudioAttributes.CONTENT_TYPE_SPEECH)
                    .build();
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, "Dropping", NotificationManager.IMPORTANCE_LOW);
            //channel.enableLights(true);
            channel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);

            NotificationManager manager = getSystemService(NotificationManager.class);
            manager.createNotificationChannel(channel);


        return new Notification.Builder(getApplicationContext(),CHANNEL_ID)
                .setContentTitle(drop)
                .setContentText(upload_in_progress)
                .setSmallIcon(R.drawable.ic_stat_name)
                .setLargeIcon(BitmapFactory.decodeResource(this.getResources(),R.drawable.locus_logo));

    }
}
