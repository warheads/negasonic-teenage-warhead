package social.locus.Notifications;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import social.locus.Adapters.NotificationsAdapter;
import social.locus.Beans.Notif;
import social.locus.R;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class NotificationsActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    TextView req;
    String id = null;
    int N = 0;
    NotificationsAdapter notificationsAdapter;
    List<Notif> notifList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notifications);
        overridePendingTransition(0,0);

        Toolbar aboutUs_toolbar = findViewById(R.id.notifications_toolbar);
        setSupportActionBar(aboutUs_toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back_arrow);
        aboutUs_toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        //req = findViewById(R.id.req);
//        req.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                startActivity(new Intent(getApplicationContext(), RequestsActivity.class));
//            }
//        });
        recyclerView = findViewById(R.id.notifyview);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(linearLayoutManager);
        notificationsAdapter = new NotificationsAdapter(getApplicationContext(),notifList);
        recyclerView.setAdapter(notificationsAdapter);
        readNotif();
        setReqcounts();
    }

    private void readNotif() {
        String id = FirebaseAuth.getInstance().getCurrentUser().getUid();
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference().child("Notifs").child(id);
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                notifList.clear();
                for(DataSnapshot dsp: dataSnapshot.getChildren()) {
                    Notif notif = dsp.getValue(Notif.class);
                    notifList.add(notif);
                }
                Collections.reverse(notifList);
                notificationsAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void setReqcounts() {
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if(user != null) {
            id = user.getUid();
        }

        DatabaseReference reference = FirebaseDatabase.getInstance().getReference("Requests");
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                N = Integer.parseInt(String.valueOf(dataSnapshot.child(id).getChildrenCount()));
                //req.setText("Connect Requests "+"("+N+")");
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
