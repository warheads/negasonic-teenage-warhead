package social.locus;

import android.content.Context;
import android.graphics.Point;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.mapbox.maps.MapView;

public class MapBoxMapView extends MapView {

    private final UpdateMapAfterUserInterection updateMapAfterUserInterection;
    private final PinchInteraction pinchInteraction;
    Point touchPoint = new Point();

    private float mPrimStartTouchEventX = -1;
    private float mPrimStartTouchEventY = -1;
    private float mSecStartTouchEventX = -1;
    private float mSecStartTouchEventY = -1;
    private float mPrimSecStartTouchDistance = 0;
    private float distanceCurrent;


    public MapBoxMapView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        try {
            updateMapAfterUserInterection = (Maps) context;
            pinchInteraction = (Maps) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement UpdateMapAfterUserInterection and pinchInteraction");
        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        switch (ev.getAction()) {
            case MotionEvent.ACTION_MOVE:
                if(ev.getPointerCount()<2) {
                    final Point newTouchPoint = new Point();  // the new position of user's finger on screen after movement is detected
                    newTouchPoint.x = (int) ev.getX();
                    newTouchPoint.y = (int) ev.getY();
                    updateMapAfterUserInterection.onUpdateMapAfterUserInterection(touchPoint,newTouchPoint);
                    touchPoint = newTouchPoint;
                }
                else  if (ev.getPointerCount() > 1 && isPinchGesture(ev)) {
//                    Log.i("pinch", "PINCH!");
                    pinchInteraction.onPinchInteraction(distanceCurrent - mPrimSecStartTouchDistance);

                }
                break;
//            case MotionEvent.ACTION_POINTER_DOWN:
            case MotionEvent.ACTION_DOWN:
                Log.i("","down");
                if (ev.getPointerCount() == 1) {
                    mPrimStartTouchEventX = ev.getX(0);
                    mPrimStartTouchEventY = ev.getY(0);
                    Log.d("TAG", String.format("POINTER ONE X = %.5f, Y = %.5f", mPrimStartTouchEventX, mPrimStartTouchEventY));
                }
                if (ev.getPointerCount() > 1) {
                    // Starting distance between fingers
                    mSecStartTouchEventX = ev.getX(1);
                    mSecStartTouchEventY = ev.getY(1);
                    Log.d("TAG", String.format("POINTER TWO X = %.5f, Y = %.5f", mSecStartTouchEventX, mSecStartTouchEventY));
                }

                break;
            case MotionEvent.ACTION_POINTER_UP:
            case MotionEvent.ACTION_UP:
                mPrimSecStartTouchDistance = 0;
                if (ev.getPointerCount() < 2) {
                    mSecStartTouchEventX = -1;
                    mSecStartTouchEventY = -1;
                }
                if (ev.getPointerCount() < 1) {
                    mPrimStartTouchEventX = -1;
                    mPrimStartTouchEventY = -1;
                }
                Log.i("","up");
                break;
        }
        return super.dispatchTouchEvent(ev);
    }

    private boolean isPinchGesture(MotionEvent event) {
        if (event.getPointerCount() == 2) {
//            Log.i("pinch", "Two fingers");
            distanceCurrent = distance(event, 0, 1);
            if (mPrimSecStartTouchDistance==0){
                mPrimSecStartTouchDistance = distanceCurrent;
            }
            final float diffPrimX = mPrimStartTouchEventX - event.getX(0);
            final float diffPrimY = mPrimStartTouchEventY - event.getY(0);
            final float diffSecX = mSecStartTouchEventX - event.getX(1);
            final float diffSecY = mSecStartTouchEventY - event.getY(1);

            if (// if the distance between the two fingers has increased past
                // our threshold
                    Math.abs(distanceCurrent - mPrimSecStartTouchDistance) > 5
                            // and the fingers are moving in opposing directions
                            && (diffPrimY * diffSecY) <= 0
                            && (diffPrimX * diffSecX) <= 0) {
//                pinchInteraction.onPinchInteraction(distanceCurrent - mPrimSecStartTouchDistance);
                Log.i("pinch", "pinch out!");
                Log.i("pinch", String.valueOf(distanceCurrent - mPrimSecStartTouchDistance));
                return true;
            }
            else if (// if the distance between the two fingers has increased past
                // our threshold
                    Math.abs(distanceCurrent - mPrimSecStartTouchDistance) > 5
                            // and the fingers are moving in opposing directions
                            && (diffPrimY * diffSecY) >= 0
                            && (diffPrimX * diffSecX) >= 0) {

                Log.i("pinch", "pinch in!");
                Log.i("pinch", String.valueOf( distanceCurrent - mPrimSecStartTouchDistance));
                return true;
            }
        }

        return false;
    }

    public float distance(MotionEvent event, int first, int second) {
        if (event.getPointerCount() >= 2) {
            final float x = event.getX(first) - event.getX(second);
            final float y = event.getY(first) - event.getY(second);

            return (float) Math.sqrt(x * x + y * y);
        } else {
            return 0;
        }
    }

    // Map Activity must implement this interface
    public interface PinchInteraction {
        public void onPinchInteraction(float distance);
    }

    // Map Activity must implement this interface
    public interface UpdateMapAfterUserInterection {
        public void onUpdateMapAfterUserInterection(Point touchpoint,Point newTouchpoint);
    }
}
