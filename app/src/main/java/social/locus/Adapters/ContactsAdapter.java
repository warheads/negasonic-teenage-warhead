package social.locus.Adapters;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import social.locus.Beans.User;
import social.locus.R;

public class ContactsAdapter extends RecyclerView.Adapter<ContactsAdapter.ViewHolder>{

    Context mContext;
    ArrayList<SavedContactsModel> mReg_contact_list;
    ArrayList<User> mcontact_users;
    private static final int STATIC_VIEW = 0;
    private static final int DYNAMIC_VIEW = 1;
    User invite_user;
    String url ="";
    private View.OnClickListener mOnItemClickListener;

    public ContactsAdapter(Context context, ArrayList<User> Contact_users) {
        invite_user = new User("","","","","","invite","","","",null,"");
        mContext = context;
        mcontact_users = Contact_users;
        mcontact_users.add(invite_user);
    }

    public void setOnItemClickListener(View.OnClickListener itemClickListener) {
        mOnItemClickListener = itemClickListener;
    }

    @NonNull
    @Override
    public ContactsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.connects, parent, false);
        return new ContactsAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        //Toast.makeText(mContext, ""+position, Toast.LENGTH_SHORT).show();

        if(mcontact_users.get(position).getUsername() == null) {
            holder.name.setText("Share");
            holder.username.setText("Invite your friends");
            holder.profilepic.setImageResource(R.drawable.sharing);
            holder.profilepic.setBorderWidth(8);

        }
        else {
            holder.name.setText(mcontact_users.get(position).getName());
            holder.username.setText(mcontact_users.get(position).getUsername());
            Picasso.with(mContext).load(mcontact_users.get(position).getThumb_image()).placeholder(R.drawable.ic_dp_icon_male_04).into(holder.profilepic);
        }

    }

    @Override
    public int getItemCount() {
        return mcontact_users.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        TextView name, username;
        CircularImageView profilepic;
        ConstraintLayout layout;

        public ViewHolder(View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.contact_name_textview);
            username = itemView.findViewById(R.id.contact_username_textview);
            profilepic = itemView.findViewById(R.id.contact_pic);
            layout = itemView.findViewById(R.id.touch_layout);

            itemView.setTag(this);
            itemView.setOnClickListener(mOnItemClickListener);

        }
    }
}
