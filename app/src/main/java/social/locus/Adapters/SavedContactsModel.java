package social.locus.Adapters;

public class SavedContactsModel {

    private String Name,Number;
    private boolean isSelected;

    public SavedContactsModel(String name, String number) {
        Name = name;
        Number = number;
        isSelected = false;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getNumber() {
        return Number;
    }

    public void setNumber(String number) {
        Number = number;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public boolean isSelected() {
        return isSelected;
    }
}
