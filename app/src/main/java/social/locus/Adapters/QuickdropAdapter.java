package social.locus.Adapters;

import android.content.Context;
import android.content.Intent;
import android.location.Location;
import androidx.annotation.NonNull;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.snackbar.Snackbar;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import de.hdodenhof.circleimageview.CircleImageView;
import social.locus.Beans.QuickdropModel;
import social.locus.Beans.Warhead;
import social.locus.Camera.CameraCaptureActivity;
import social.locus.Drops.dropViewActivity;
import social.locus.Maps;
import social.locus.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.common.geometry.S2CellId;
import com.google.common.geometry.S2LatLng;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by User on 2/12/2018.
 */

public class QuickdropAdapter extends RecyclerView.Adapter<QuickdropAdapter.ViewHolder> {

    private static final String TAG = "RecyclerViewAdapter";

    //vars
    private ArrayList<QuickdropModel> mquick_drops;
    private Context mContext;
    private static final int STATIC_VIEW = 0;
    private static final int DYNAMIC_VIEW = 1;
    private View.OnLongClickListener mOnItemClickListener;
    private View.OnClickListener mOnItemClickListener2;
    private HashMap<String, Boolean> id_seen_list;
    String userprofile;

    public QuickdropAdapter(Context context, ArrayList<QuickdropModel> quick_drops, HashMap<String, Boolean> id_seen_list, String current_userprofile) {
        //Log.i("Quick : ",quick_drops.size()+"");
        mquick_drops = quick_drops;
        mContext = context;
        this.id_seen_list = id_seen_list;
        this.userprofile = current_userprofile;
    }

    public void setOnItemClickListener(View.OnLongClickListener itemClickListener) {
        mOnItemClickListener = itemClickListener;
    }

    public void setOnItemClickListener2(View.OnClickListener itemClickListener) {
        mOnItemClickListener2 = itemClickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.quickdrop, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {

        switch (getItemViewType(position)) {

            case STATIC_VIEW :

                Log.i("Quick : ","Static");

                    Picasso.with(mContext).load(userprofile).placeholder(R.drawable.ic_dp_icon_male_04).into(holder.image);

                holder.image.setBorderWidth(6);
                holder.image.setBorderColor(mContext.getResources().getColor(R.color.black));
                holder.user.setText("Profile");
                holder.smallimage.setVisibility(View.GONE);
                holder.unread_imageview.setVisibility(View.GONE);
                break;

            case DYNAMIC_VIEW :
                Log.i("Quick : ","Dynamic");
                Picasso.with(mContext).load(mquick_drops.get(position).getUser_image()).placeholder(R.drawable.ic_dp_icon_male_04).into(holder.image);
                holder.user.setText(mquick_drops.get(position).getUser_name());
                holder.smallimage.setVisibility(View.GONE);
                holder.unread_imageview.setVisibility(View.GONE);
                if(mquick_drops.get(position).getFlag() == 1) {
                    holder.smallimage.setVisibility(View.VISIBLE);
                    holder.smallimage.setImageResource(R.drawable.ic_yellow_bubble);
                }
                if(!mquick_drops.get(position).isSeen())
                    holder.unread_imageview.setVisibility(View.VISIBLE);

                String user_id = mquick_drops.get(position).getUser_id();
               // if(id_seen_list.containsKey(user_id)) {
                   // if(!id_seen_list.get(user_id)) {
//                        holder.image.setBorderWidth(6);
//                        holder.image.setBorderColor(mContext.getResources().getColor(R.color.backgray));
                        //holder.smallimage.setVisibility(View.VISIBLE);
                       // holder.unread_imageview.setVisibility(View.VISIBLE);
                   // }
               // }
                break;

        }

    }

    @Override
    public int getItemCount() {
        return mquick_drops.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        CircleImageView image, unread_imageview;
        CardView quick_cardView;
        ImageView smallimage;
        TextView user;

        public ViewHolder(View itemView) {
            super(itemView);

            image = itemView.findViewById(R.id.quickpp);
            smallimage = itemView.findViewById(R.id.quickadd);
            unread_imageview = itemView.findViewById(R.id.quickadd_front);
            user = itemView.findViewById(R.id.quickuser);
            //quick_cardView = itemView.findViewById(R.id.quick_cardView);

            itemView.setTag(this);
            itemView.setOnLongClickListener(mOnItemClickListener);
            itemView.setOnClickListener(mOnItemClickListener2);
        }
    }

    @Override
    public int getItemViewType(int position) {
        if(position == 0 )
            return STATIC_VIEW;
        else
            return DYNAMIC_VIEW;
    }
}
