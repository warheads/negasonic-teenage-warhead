package social.locus.Adapters;

import static com.facebook.FacebookSdk.getApplicationContext;

import android.annotation.SuppressLint;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import android.content.DialogInterface;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import social.locus.Beans.CommentsModel;
import social.locus.Beans.Notif;
import social.locus.R;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

public class CommentsAdapter extends RecyclerView.Adapter<CommentsAdapter.ViewHolder>{

    private final Context mContext;
    private final List<CommentsModel> mComments;
    private String current_id = FirebaseAuth.getInstance().getCurrentUser().getUid();
    private DatabaseReference comment_reference;

    public CommentsAdapter(Context mContext, List<CommentsModel> mComments) {
        this.mContext = mContext;
        this.mComments = mComments;
        comment_reference = FirebaseDatabase.getInstance().getReference("Comments");
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.notifs,viewGroup,false);
        return new CommentsAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, @SuppressLint("RecyclerView") int i) {

        i = holder.getAdapterPosition();

        String comment = "<b>" + mComments.get(i).getUsername() + "</b>" +" "+mComments.get(holder.getAdapterPosition()).getDid();

        holder.did.setText(Html.fromHtml(comment));
        Picasso.with(mContext).load(mComments.get(i).getProfile()).placeholder(R.drawable.ic_dp_icon_male_04).into(holder.profile);
        String time = getDate(Long.parseLong(mComments.get(holder.getAdapterPosition()).getNotif_time()), " hh:mm a dd/MM/yyyy");
        holder.time.setText(time);
        holder.back.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {

                if(mComments.get(holder.getAdapterPosition()).getUserid().matches(current_id)) {
                    final CharSequence[] items = {"Delete comment","Copy text"};

                    AlertDialog.Builder builder = new AlertDialog.Builder(mContext, R.style.DatePickerTheme);
                    builder.setItems(items, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int item) {
                            switch(item) {

                                case 0 : {
                                            comment_reference.child(mComments.get(holder.getAdapterPosition()).getWar_id())
                                            .child(mComments.get(holder.getAdapterPosition()).getComment_id()).removeValue();
                                            Toast.makeText(mContext, "Comment deleted", Toast.LENGTH_SHORT).show();
                                            break;
                                         }

                                case 1 : {
                                            ClipboardManager clipboard = (ClipboardManager) mContext.getSystemService(Context.CLIPBOARD_SERVICE);
                                            ClipData clip = ClipData.newPlainText("",mComments.get(holder.getAdapterPosition()).getDid());
                                            clipboard.setPrimaryClip(clip);
                                            Toast.makeText(mContext, "Text Copied", Toast.LENGTH_SHORT).show();
                                         }

                            }
                        }
                    });
                    builder.show();
                }

                if(!mComments.get(holder.getAdapterPosition()).getUserid().matches(current_id)) {
                    final CharSequence[] items = {"Copy text"};

                    AlertDialog.Builder builder = new AlertDialog.Builder(mContext, R.style.DatePickerTheme);
                    builder.setItems(items, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int item) {

                                    ClipboardManager clipboard = (ClipboardManager) mContext.getSystemService(Context.CLIPBOARD_SERVICE);
                                    ClipData clip = ClipData.newPlainText("",mComments.get(holder.getAdapterPosition()).getDid());
                                    clipboard.setPrimaryClip(clip);
                                    Toast.makeText(mContext, "Text Copied", Toast.LENGTH_SHORT).show();
                        }
                    });
                    builder.show();
                }

                return false;
            }
        });
        //holder.symbol.setImageResource(R.drawable.notification_profile_icon);
    }


    @Override
    public int getItemCount() {
        return mComments.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        ImageView profile,symbol;
        TextView username,time,did;
        LinearLayout back;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            profile = itemView.findViewById(R.id.profile1);
            time = itemView.findViewById(R.id.time);
            did = itemView.findViewById(R.id.notif);
            // symbol = itemView.findViewById(R.id.notif_symbol);
            back = itemView.findViewById(R.id.back);
        }

    }

    private static String getDate(long milliSeconds, String dateFormat)
    {
        // Create a DateFormatter object for displaying date in specified format.
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);

        // Create a calendar object that will convert the date and time value in milliseconds to date.
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        return formatter.format(calendar.getTime());
    }

}
