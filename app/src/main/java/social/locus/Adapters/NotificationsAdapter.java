package social.locus.Adapters;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import social.locus.Beans.Notif;
import social.locus.R;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

public class NotificationsAdapter extends RecyclerView.Adapter<NotificationsAdapter.ViewHolder>{

    private Context mContext;
    private List<Notif> mNotif;
    private String id;
    private int N=0;

    public NotificationsAdapter(Context mContext, List<Notif> mNotif) {
        this.mContext = mContext;
        this.mNotif = mNotif;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.notifs,viewGroup,false);
        return new NotificationsAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int i) {

            String notif = "<b>" + mNotif.get(i).getUsername() + "</b>" +mNotif.get(i).getDid();

            holder.did.setText(Html.fromHtml(notif));
            Picasso.with(mContext).load(mNotif.get(i).getProfile()).placeholder(R.drawable.ic_dp_icon_male_04).into(holder.profile);
            String time = getDate(Long.parseLong(mNotif.get(i).getNotif_time()), " hh:mm a dd/MM/yyyy");
            holder.time.setText(time);
            //holder.symbol.setImageResource(R.drawable.notification_profile_icon);
        }


    @Override
    public int getItemCount() {
        return mNotif.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        ImageView profile,symbol;
        TextView username,time,did;
        LinearLayout back;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            profile = itemView.findViewById(R.id.profile1);
            time = itemView.findViewById(R.id.time);
            did = itemView.findViewById(R.id.notif);
           // symbol = itemView.findViewById(R.id.notif_symbol);
            back = itemView.findViewById(R.id.back);
        }

    }

    private static String getDate(long milliSeconds, String dateFormat)
    {
        // Create a DateFormatter object for displaying date in specified format.
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);

        // Create a calendar object that will convert the date and time value in milliseconds to date.
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        return formatter.format(calendar.getTime());
    }

}
