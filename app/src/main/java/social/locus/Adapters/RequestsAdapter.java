package social.locus.Adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import social.locus.Beans.User;
import social.locus.R;
import social.locus.Settings.RequestsActivity;

public class RequestsAdapter extends RecyclerView.Adapter<RequestsAdapter.ViewHolder>{

    Context mContext;
    ArrayList<User> mUnknown_users;

    public RequestsAdapter(Context context, ArrayList<User> unknown_users) {

        mContext = context;
        mUnknown_users = unknown_users;

    }

    @NonNull
    @Override
    public RequestsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.requests, parent, false);
        return new RequestsAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, @SuppressLint("RecyclerView") int i) {

        int position = holder.getAdapterPosition();

            holder.name.setText(mUnknown_users.get(position).getName());
            holder.contact.setText(mUnknown_users.get(position).getContact());
            Picasso.with(mContext).load(mUnknown_users.get(position).getThumb_image()).placeholder(R.drawable.ic_dp_icon_male_04).into(holder.profilepic);
            holder.add.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((RequestsActivity)mContext).finish();
                    addContact(mUnknown_users.get(position).getName(), mUnknown_users.get(position).getContact());
                }
            });

    }

    @Override
    public int getItemCount() {
        return mUnknown_users.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        TextView name, contact;
        CircleImageView profilepic;
        Button add;

        public ViewHolder(View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.request_name);
            contact = itemView.findViewById(R.id.request_contact);
            profilepic = itemView.findViewById(R.id.request_profilepic);
            add = itemView.findViewById(R.id.request_add_contact_button);
        }
    }

    private  void addContact(String name, String contact) {
        Intent contactIntent = new Intent(ContactsContract.Intents.Insert.ACTION);
        contactIntent.setType(ContactsContract.RawContacts.CONTENT_TYPE);

        contactIntent
                .putExtra(ContactsContract.Intents.Insert.NAME, name)
                .putExtra(ContactsContract.Intents.Insert.PHONE, contact);

        ((RequestsActivity)mContext).startActivityForResult(contactIntent, 1);
    }
}
