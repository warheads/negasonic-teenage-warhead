package social.locus.Adapters;

import static com.facebook.FacebookSdk.getApplicationContext;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Notification;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.recyclerview.widget.RecyclerView;

import social.locus.Beans.User;
import social.locus.Beans.Warhead;
import social.locus.Chat.ChatActivity;
import social.locus.Drops.PhotoActivity;
import social.locus.APIService;
import social.locus.Drops.dropViewActivity;
import social.locus.LocationUpdatesService;
import social.locus.Maps;
import social.locus.Notifications.Client;
import social.locus.Notifications.Data;
import social.locus.Notifications.MyResponse;
import social.locus.Notifications.NotificationOreo;
import social.locus.Notifications.Sender;
import social.locus.Notifications.Token;
import social.locus.R;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ContactsListadapter extends RecyclerView.Adapter<ContactsListadapter.ViewHolder> {

    private Context mContext;
    private ArrayList<SavedContactsModel> mContactsModels;
    private ArrayList<User> mUserList;
    private String memory_front, memory_back,current_user_id, current_user_username,current_user_thumb="default";
    private APIService apiService;
    private Location warloc = new Location("");
    private StorageReference sref;
    private String boxId;
    private DatabaseReference databaseReference;
    private NotificationOreo OnotificationManager;
    private Notification.Builder Obuilder;
    private NotificationManagerCompat notificationManager;
    private NotificationCompat.Builder builder;
    private ArrayList<Integer> check_list = new ArrayList<>();
    private int m = (int) ((new Date().getTime() / 1000L) % Integer.MAX_VALUE);
    private boolean isSelectedAll=false;

    public ContactsListadapter(Context context, ArrayList<SavedContactsModel> contactsModels, ArrayList<User> user_list, Location warloc,
                               String memory_front, String memory_back, String boxId) {
        this.mContactsModels = contactsModels;
        this.mUserList = user_list;
        this.mContext = context;
        this.memory_back = memory_back;
        this.memory_front = memory_front;
        apiService = Client.getClient("https://fcm.googleapis.com/").create(APIService.class);
        this.warloc = warloc;
        this.boxId = boxId;
        sref = FirebaseStorage.getInstance().getReference("Warheads");
        databaseReference = FirebaseDatabase.getInstance().getReference("User");
        getCurrentuser();

        //Toast.makeText(mContext, memory_front+memory_back, Toast.LENGTH_SHORT).show();

        OnotificationManager = new NotificationOreo(mContext);
        Obuilder = OnotificationManager.getONotification("forYou", "dropping...");
        int PROGRESS_MAX = 100;
        int PROGRESS_CURRENT = 0;
        Obuilder.setProgress(PROGRESS_MAX, PROGRESS_CURRENT, false);

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.contacts_item_layout,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ContactsListadapter.ViewHolder holder, @SuppressLint("RecyclerView") int position) {

        holder.Name.setText(mUserList.get(holder.getAdapterPosition()).getName());
        holder.username.setText(mUserList.get(holder.getAdapterPosition()).getUsername());
        Picasso.with(mContext).load(mUserList.get(holder.getAdapterPosition()).getThumb_image()).placeholder(R.drawable.ic_dp_icon_male_04).into(holder.Pic);

        if (!isSelectedAll){
            holder.checkBox.setChecked(false);
        }
        else  holder.checkBox.setChecked(true);

        holder.touch_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!mContactsModels.get(holder.getAdapterPosition()).isSelected()) {
                    mContactsModels.get(holder.getAdapterPosition()).setSelected(true);
                    holder.checkBox.setChecked(true);
                    check_list.add(holder.getAdapterPosition());
                    // Toast.makeText(mContext, check_list+"", Toast.LENGTH_SHORT).show();
                }
                else {
                    mContactsModels.get(holder.getAdapterPosition()).setSelected(false);
                    holder.checkBox.setChecked(false);
                    if(check_list.size()==1) {
                        check_list.clear();
                    }
                    else check_list.remove(Integer.parseInt(holder.getAdapterPosition()+""));
                    // Toast.makeText(mContext, check_list+"", Toast.LENGTH_SHORT).show();
                }
            }
        });

        PhotoActivity.drop_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(check_list.isEmpty())
                    Toast.makeText(mContext, "Select at least one to drop!", Toast.LENGTH_SHORT).show();
                else {
                    Toast.makeText(mContext, "Check the progress in the notification bar.", Toast.LENGTH_LONG).show();
                    ((Activity)mContext).finish();
                    Intent intent = new Intent(mContext, Maps.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    mContext.startActivity(intent);
                    // dropforyou(memory_front, memory_back, mUserList.get(check_list.get(i)), mContactsModels.get(check_list.get(i)).getName());
                    dropforyou(memory_front, memory_back, check_list);
                }
            }
        });

        PhotoActivity.selectall_checkbox.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //is chkIos checked?
                if (((CheckBox) v).isChecked()) {
                    //Case 1
                    for(int i=0; i<mContactsModels.size(); i++) {
                        mContactsModels.get(i).setSelected(true);
                        isSelectedAll=true;
                        check_list.add(i);
                        //Toast.makeText(mContext, check_list+"", Toast.LENGTH_SHORT).show();
                        notifyDataSetChanged();
                    }
                }
                else{
                    check_list.clear();
                    for(int i=0; i<mContactsModels.size(); i++) {
                        mContactsModels.get(i).setSelected(false);
                        isSelectedAll=false;
                        //Toast.makeText(mContext, check_list+"", Toast.LENGTH_SHORT).show();
                        notifyDataSetChanged();
                    }
                }
                //case 2

            }
        });

    }

    @Override
    public int getItemCount() {
        return mContactsModels.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        TextView Name,username;
        ImageView Pic;
        CheckBox checkBox;
        ConstraintLayout touch_layout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            Name = itemView.findViewById(R.id.contact_name);
            username = itemView.findViewById(R.id.contact_username);
            Pic = itemView.findViewById(R.id.contact_pic);
            touch_layout = itemView.findViewById(R.id.touch_layout);
            checkBox = itemView.findViewById(R.id.contact_checkbox);
            checkBox.setClickable(false);
        }
    }

    private void sendNotification(final String receiver, final String username, final String userprofle, final String message, final String mCurrentUserId) {

        DatabaseReference tokens = FirebaseDatabase.getInstance().getReference("Tokens");
        Query query = tokens.orderByKey().equalTo(receiver);
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                for(DataSnapshot dsp : dataSnapshot.getChildren()) {
                    Token token = dsp.getValue(Token.class);
                    Data data = new Data(mCurrentUserId,userprofle,message,username,receiver,"memory_drop", getContactFromStorage());

                    Sender sender = new Sender(data,token.getToken());

                    apiService.sendNotification(sender)
                            .enqueue(new Callback<MyResponse>() {
                                @Override
                                public void onResponse(Call<MyResponse> call, Response<MyResponse> response) {
                                    if(response.code() == 200) {
                                        if(response.body().success != 1){
                                            Log.i("msg","Failed");
                                        }
                                    }
                                }

                                @Override
                                public void onFailure(Call<MyResponse> call, Throwable t) {

                                }
                            });

                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    private String getContactFromStorage() {
        SharedPreferences Preferences = mContext.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        String contact = Preferences.getString("my_phone","");
        return contact;
    }

    private void setNotifs(String userid, String username, String profile, String did, String war_id, String time) {

        DatabaseReference reference = FirebaseDatabase.getInstance().getReference("Notifs").child(userid);

        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("username",username);
        hashMap.put("profile",profile);
        hashMap.put("userid",current_user_id);
        hashMap.put("did",did);
        hashMap.put("war_id",war_id);
        hashMap.put("notif_time",time);
        reference.push().setValue(hashMap);

    }

    private String getLocality(Double lat, Double lon) {
        Geocoder geocoder = new Geocoder(mContext, Locale.getDefault());
        String locality=null;
        try {
            List<Address> addresses = geocoder.getFromLocation(lat, lon, 1);
            if(addresses.size() > 0) {
                Address obj = addresses.get(0);
                locality = obj.getSubLocality();
            }
            //    Log.i("Address",add);
            //  Toast.makeText(this, add, Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return locality;
    }

    private void getCurrentuser() {

        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                current_user_id = FirebaseAuth.getInstance().getCurrentUser().getUid();
                current_user_username = String.valueOf(dataSnapshot.child(current_user_id).child("username").getValue());
                if(dataSnapshot.child(current_user_id).child("thumb_image").exists())
                current_user_thumb = String.valueOf(dataSnapshot.child(current_user_id).child("thumb_image").getValue());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    private void dropforyou(String memory_front, String memory_back, ArrayList<Integer> check_list) {

        //Toast.makeText(mContext, memory_front+memory_back, Toast.LENGTH_SHORT).show();

        Uri imgpost = Uri.parse(memory_front);
        Uri imgpost2 = Uri.parse(memory_back);

        //Toast.makeText(mContext, imgpost2.toString(), Toast.LENGTH_SHORT).show();

        Log.i("image_check",imgpost.toString());
        Log.i("image_check2",imgpost2.toString());

        final StorageReference filepath = sref.child(current_user_id).child(imgpost.getLastPathSegment());
        final StorageReference filepath2 = sref.child(current_user_id).child(imgpost2.getLastPathSegment());
        final DatabaseReference newref = FirebaseDatabase.getInstance().getReference();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            OnotificationManager = new NotificationOreo(mContext);
            Obuilder = OnotificationManager.getONotification("Drop", "Setting up the memory...");
            OnotificationManager.getManager().notify(2, Obuilder.build());
        }
        else {
            notificationManager = NotificationManagerCompat.from(mContext);
            builder = new NotificationCompat.Builder(mContext, "Drop");
            builder.setContentTitle("Drop")
                    .setContentText("Setting up the memory...")
                    .setSmallIcon(R.drawable.ic_stat_name)
                    .setPriority(NotificationCompat.PRIORITY_LOW);
            notificationManager.notify(2, builder.build());
        }

        filepath.putFile(imgpost).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onProgress(@NonNull UploadTask.TaskSnapshot taskSnapshot) {
                filepath.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                    @Override
                    public void onSuccess(Uri uri) {

                        String url_1 = uri.toString();

                        filepath2.putFile(imgpost2).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                            @Override
                            public void onProgress(@NonNull UploadTask.TaskSnapshot taskSnapshot) {

                                double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();

                                Obuilder.setContentText("Dropping...");
                                Obuilder.setProgress(100, (int) progress, false);
                                OnotificationManager.getManager().notify(2, Obuilder.build());

                                if ((int) progress == 100) {
                                    filepath2.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                        @Override
                                        public void onSuccess(Uri uri) {

                                            String url_2 = uri.toString();

                                            for(int i=0; i< check_list.size(); i++) {

                                                User user = mUserList.get(check_list.get(i));
                                                DatabaseReference ref = newref.child("foryou").child(user.getId()).child("received").child(boxId);
                                                String war_id = ref.push().getKey();
                                                Warhead warrr = new Warhead(url_1, " ", user.getId(), current_user_id, current_user_username,
                                                        current_user_thumb, String.valueOf(System.currentTimeMillis()), warloc.getLatitude(), warloc.getLongitude()
                                                        , "forU", war_id, false, "none", 0, url_1);
                                                ref.child(war_id).setValue(warrr);

                                                newref.child("foryou_list").child(user.getId()).child("received").child(war_id).setValue(warrr).addOnSuccessListener(new OnSuccessListener<Void>() {
                                                    @Override
                                                    public void onSuccess(Void unused) {
                                                        DatabaseReference ref = newref.child("foryou").child(user.getId()).child("received").child(boxId);
                                                        DatabaseReference self_foryou_ref = newref.child("foryou").child(current_user_id).child("sent").child(boxId);
                                                        ref.child(war_id).child("backimgurl").setValue(url_2);
                                                        warrr.setBackimgurl(url_2);
                                                        warrr.setType("sent");
                                                        warrr.setSenderimage(user.getThumb_image());
                                                        warrr.setSendername(user.getUsername());
                                                        self_foryou_ref.child(war_id).setValue(warrr);
                                                        newref.child("foryou_list").child(user.getId()).child("received").child(war_id).child("backimgurl")
                                                                .setValue(url_2).addOnSuccessListener(new OnSuccessListener<Void>() {
                                                            @Override
                                                            public void onSuccess(Void unused) {
                                                                Log.i("uri_back",url_2);
                                                                String locality = getLocality(warloc.getLatitude(), warloc.getLongitude());
                                                                if(locality != null) {
                                                                    sendreply(current_user_username+" dropped a memory in " + getLocality(warloc.getLatitude(), warloc.getLongitude())
                                                                            ,user.getId(),current_user_username, 1);
                                                                    sendNotification(user.getId(), current_user_username, current_user_thumb,
                                                                            current_user_username+" dropped a memory ForYou in " + getLocality(warloc.getLatitude(), warloc.getLongitude()), current_user_id);
                                                                    setNotifs(user.getId(), current_user_username, current_user_thumb,
                                                                            " dropped a memory ForYou in " + getLocality(warloc.getLatitude(), warloc.getLongitude()), war_id, String.valueOf(System.currentTimeMillis()));
                                                                }
                                                                else {
                                                                    sendreply(current_user_username+" dropped a memory for you."
                                                                            ,user.getId(),current_user_username, 1);
                                                                    sendNotification(user.getId(), current_user_username, current_user_thumb,
                                                                            current_user_username+" dropped a memory ForYou.", current_user_id);
                                                                    setNotifs(user.getId(), current_user_username, current_user_thumb,
                                                                            " dropped a memory ForYou.", war_id, String.valueOf(System.currentTimeMillis()));

                                                                }
                                                            }
                                                        });
                                                    }
                                                });
                                            }

                                            Obuilder.setContentText("ForYou Dropped ")
                                                    .setProgress(0,0,false)
                                                    .setSmallIcon(R.drawable.complete);
                                            OnotificationManager.getManager().notify(2, Obuilder.build());
                                            Toast.makeText(mContext, "ForYou dropped!", Toast.LENGTH_SHORT).show();
                                        }
                                    });
                                }

                            }
                        });

                    }
                });
            }
        });

    }

    //getting real path from uri
    private String getFilePath(Uri uri) {
        String[] projection = {MediaStore.Images.Media.DATA};

        Cursor cursor = mContext.getContentResolver().query(uri, projection, null, null, null);
        if (cursor != null) {
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(projection[0]);
            String picturePath = cursor.getString(columnIndex); // returns null
            cursor.close();
            return picturePath;
        }
        return null;
    }

    private void sendreply(String replytext, final String mChatUser, final String image, final int version) {

        if(!TextUtils.isEmpty(replytext)){
            //Toast.makeText(mContext, "innnnn", Toast.LENGTH_SHORT).show();
            DatabaseReference mRootRef = FirebaseDatabase.getInstance().getReference();

            String current_user_ref = "messages/" + current_user_id + "/" + mChatUser;
            String chat_user_ref = "messages/" + mChatUser + "/" + current_user_id;

            DatabaseReference user_message_push = mRootRef.child("messages").child(current_user_id).child(mChatUser).push();
            String push_id = user_message_push.getKey();

            Map messageMap = new HashMap();
            messageMap.put("message", replytext);
            messageMap.put("seen", false);
            messageMap.put("type", "drop");
            messageMap.put("time", ServerValue.TIMESTAMP);
            messageMap.put("from", current_user_id);
            messageMap.put("thumb", image);
            messageMap.put("msg_id", push_id);
            messageMap.put("version", version);

            Map messageUserMap = new HashMap();
            messageUserMap.put(current_user_ref + "/" + push_id, messageMap);
            messageUserMap.put(chat_user_ref + "/" + push_id, messageMap);

            mRootRef.updateChildren(messageUserMap, new DatabaseReference.CompletionListener() {
                @Override
                public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {

                    if(databaseError != null){

                        Log.d("CHAT_LOG", databaseError.getMessage().toString());

                    }

                    else {

                    }

                }
            });
        }

    }

}
