package social.locus.Camera;

import android.animation.AnimatorInflater;
import android.animation.AnimatorSet;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.location.Location;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.content.res.ResourcesCompat;

import android.text.Editable;
import android.text.Layout;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import de.hdodenhof.circleimageview.CircleImageView;
import social.locus.Drops.PhotoActivity;
import social.locus.LocationUpdatesService;
import social.locus.R;

import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import static android.view.View.GONE;

import com.google.android.material.bottomsheet.BottomSheetBehavior;

public class EditPhotoActivity extends AppCompatActivity {

    private AnimatorSet mSetRightOut;
    private AnimatorSet mSetLeftIn;
    private boolean mIsBackVisible = false;
    private View mCardFrontLayout;
    private View mCardBackLayout;
    EditText caption_edit,detail_edit;
    boolean swiped = false;
    boolean long_clicked = false;
    String detail=null,title = null;
    int pos=1,col=1,pol=1,aflag = 0,pol_colorname = R.color.white, MYUNDERWOOD,SANDS,NEWDAY,
            ROCKSALT,SATISFY,MARKER,HOMEMADEAPPLE,DAFOE,LOBSTER,PLAYFAIRDISPLAY,font,colorname,padding,offset;
    Typeface tfc;
    Canvas canvas,bcanvas;
    Bitmap bitmap,frontbitmap,backbitmap,backdrawbitmap;
    String loc = "28.14, 77.09";
    String dated = null;
    String cap = "Crispy vegs are love <3";
    Rect caprect = new Rect();
    Rect captrect = new Rect();
    Paint caption;
    TextPaint mTextPaint;
    Uri photo;
    ImageView imageView,imageViewf;
    int capsize, draw_colorname;
    TextView instruction;
    LinearLayout tray,brush_eraser_layout;
    ScrollView color_tray;
    RelativeLayout rl;
    ImageView doodle;
    Paint doodle_paint, pBg, mBitmapPaint, circlePaint;
    Path mPath, circlePath;
    private ArrayList<Path> paths = new ArrayList<Path>();
    private Typeface tfd;
    int brush_width = 15;
    boolean eraser_flag = false;

    //Bottomsheet attributes
    CardView bottomSheet;
    BottomSheetBehavior bottomSheetBehavior;

    @Override
    public void onBackPressed() {

        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case DialogInterface.BUTTON_POSITIVE:
                        //Yes button clicked
                        startActivity(new Intent(EditPhotoActivity.this, CameraCaptureActivity.class));
                        finish();
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        //No button clicked
                        break;
                }
            }
        };


        AlertDialog.Builder builder = new AlertDialog.Builder(EditPhotoActivity.this,R.style.DatePickerTheme);
        builder.setMessage("Are you sure? All progress will be lost.").setPositiveButton("Yes", dialogClickListener)
                .setNegativeButton("No", dialogClickListener).show();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_photo);

        ImageView  font_color, prev, next, font_type, pol_color, doodle_right, doodle_wrong, doodle_undo,brush_stroke_width,eraser;
        CircleImageView color_1, color_2, color_3, color_4, color_5, color_6, color_7, color_8, color_9, color_10;

        color_1 = findViewById(R.id.color1);
        color_2 = findViewById(R.id.color2);
        color_3 = findViewById(R.id.color3);
        color_4 = findViewById(R.id.color4);
        color_5 = findViewById(R.id.color5);
        color_6 = findViewById(R.id.color6);
        color_7 = findViewById(R.id.color7);
        color_8 = findViewById(R.id.color8);
        color_9 = findViewById(R.id.color9);
        color_10 = findViewById(R.id.color10);

        instruction = findViewById(R.id.instruction);

        pBg = new Paint();
        pBg.setColor(Color.WHITE);

        mBitmapPaint = new Paint(Paint.DITHER_FLAG);
        circlePaint = new Paint();
        circlePath = new Path();
        circlePaint.setAntiAlias(true);
        circlePaint.setColor(Color.BLUE);
        circlePaint.setStyle(Paint.Style.STROKE);
        circlePaint.setStrokeJoin(Paint.Join.MITER);
        circlePaint.setStrokeWidth(4f);

        imageView = findViewById(R.id.iv);
        imageViewf = findViewById(R.id.ivf);

        imageView.requestLayout();
        imageViewf.requestLayout();

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int width = displayMetrics.widthPixels;
        int polWidth = (int)(width - (width * 0.10));
        int polHeight = (int)(width*1.0635);

        imageView.getLayoutParams().width = polWidth;
        imageView.getLayoutParams().height = polHeight;
        imageViewf.getLayoutParams().width = polWidth;
        imageViewf.getLayoutParams().height = polHeight;

//        imageView.getLayoutParams().width = polWidth;
//        imageViewf.getLayoutParams().height = polHeight;
//        imageView.getLayoutParams().width = polWidth;
//        imageViewf.getLayoutParams().height = polHeight;

        tray = findViewById(R.id.tray);
        rl = findViewById(R.id.view);
        brush_eraser_layout = findViewById(R.id.brush_eraser_layout);
        brush_stroke_width = findViewById(R.id.brush_stroke_width);
        eraser = findViewById(R.id.eraser);

        brush_stroke_width.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                switch (brush_width) {
                    case 15:
                        brush_width = 22;
                        doodle_paint.setStrokeWidth(22);
                        brush_stroke_width.setImageResource(R.drawable.brush_stroke_15);
//                        Toast.makeText(EditPhotoActivity.this, "MYUNDERWOOD", Toast.LENGTH_SHORT).show();
                        break;
                    case 22:
                        brush_width = 29;
                        doodle_paint.setStrokeWidth(29);
                        brush_stroke_width.setImageResource(R.drawable.brush_stroke_20);
//                        Toast.makeText(EditPhotoActivity.this, "SANDS", Toast.LENGTH_SHORT).show();
                        break;
                    case 29:
                        brush_width = 7;
                        doodle_paint.setStrokeWidth(7);
                        brush_stroke_width.setImageResource(R.drawable.brush_stroke_5);
//                        Toast.makeText(EditPhotoActivity.this, "NEWDAY", Toast.LENGTH_SHORT).show();
                        break;
                    case 7:
                        brush_width = 15;
                        doodle_paint.setStrokeWidth(15);
                        brush_stroke_width.setImageResource(R.drawable.brush_stroke_10);
//                        Toast.makeText(EditPhotoActivity.this, "ROCKSALT", Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        });

        eraser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(eraser_flag) {
                    eraser_flag=false;
                    eraser.setAlpha(0.3f);

                    doodle_paint.setColor(getResources().getColor(R.color.color1));
                    color_1.setBorderWidth(5);
                    color_2.setBorderWidth(0);
                    color_3.setBorderWidth(0);
                    color_4.setBorderWidth(0);
                    color_5.setBorderWidth(0);
                    color_6.setBorderWidth(0);
                    color_7.setBorderWidth(0);
                    color_8.setBorderWidth(0);
                    color_9.setBorderWidth(0);
                    color_10.setBorderWidth(0);

                }
                else {
                    eraser_flag=true;
                    eraser.setAlpha(1f);
                    doodle_paint.setColor(getResources().getColor(R.color.white));
                    color_1.setBorderWidth(0);
                    color_2.setBorderWidth(0);
                    color_3.setBorderWidth(0);
                    color_4.setBorderWidth(0);
                    color_5.setBorderWidth(0);
                    color_6.setBorderWidth(0);
                    color_7.setBorderWidth(0);
                    color_8.setBorderWidth(0);
                    color_9.setBorderWidth(0);
                    color_10.setBorderWidth(0);

                }
            }
        });

        mCardBackLayout = findViewById(R.id.card_back);
        mCardFrontLayout = findViewById(R.id.card_front);
        caption_edit = findViewById(R.id.caption_edit);
        detail_edit = findViewById(R.id.detail_edit);
        doodle_right = findViewById(R.id.doodle_right);
        doodle_wrong = findViewById(R.id.doodle_wrong);
        doodle_undo = findViewById(R.id.doodle_undo);
        color_tray = findViewById(R.id.color_scrollbar);

        prev = findViewById(R.id.edit_close);
        next = findViewById(R.id.edit_check);
        font_type = findViewById(R.id.edit_fonts);
        font_color = findViewById(R.id.edit_color);
        pol_color = findViewById(R.id.edit_pol);
        doodle = findViewById(R.id.doodle);

        //bottomsheet attributes initialization
//        bottomSheet = findViewById(R.id.color_bottom_sheet);
//        bottomSheetBehavior = BottomSheetBehavior.from(bottomSheet);
//        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);

        MYUNDERWOOD = R.font.myunderwood;
        SANDS = R.font.sands;
        NEWDAY = R.font.newday;
        ROCKSALT = R.font.rocksalt;
        SATISFY= R.font.satisfy;
        MARKER=R.font.marker;
        HOMEMADEAPPLE = R.font.homemadeapple;
        DAFOE=R.font.dafoe;
        LOBSTER = R.font.lobster;
        PLAYFAIRDISPLAY = R.font.playfairdisplay;

        font = SANDS;
        colorname = R.color.sketch_black;
        draw_colorname = R.color.sketch_black;

        doodle_paint = new Paint();
        doodle_paint.setAntiAlias(true);
        doodle_paint.setDither(true);
        //doodle_paint.setColor(getResources().getColor(R.color.sketch_black));
        doodle_paint.setStyle(Paint.Style.STROKE);
        doodle_paint.setStrokeJoin(Paint.Join.ROUND);
        doodle_paint.setStrokeCap(Paint.Cap.ROUND);
        doodle_paint.setStrokeWidth(10);
        mPath = new Path();

        dated = getCurrentDate();

        int credtextsize = getResources().getDimensionPixelSize(R.dimen.myFontSize);
        capsize = getResources().getDimensionPixelSize(R.dimen.capsize);
        offset = getResources().getDimensionPixelOffset(R.dimen.offsets);
        int locationsize = getResources().getDimensionPixelSize(R.dimen.locsize);
        padding = getResources().getDimensionPixelSize(R.dimen.photopadding);
        int bitsize = getResources().getDimensionPixelSize(R.dimen.bitsize);


        Intent intent = getIntent();
        photo = intent.getParcelableExtra("photo");
        final String quickID = intent.getStringExtra("quickID");
        final String fromfeed = intent.getStringExtra("Direct");
        final String sself = intent.getStringExtra("sself");
        Location warloc = intent.getParcelableExtra("warloc");
        String boxId = intent.getStringExtra("cell");

        check();
        loadleftAnimations();
        changeCameraDistance();

        Typeface tf = ResourcesCompat.getFont(getApplicationContext(), MYUNDERWOOD);

        //Memory bitmap
        Bitmap original_bitmap = null;
        try {
            original_bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), photo);
            original_bitmap=rotateImageIfRequired(original_bitmap,photo);
            Matrix matrix = new Matrix();
//            matrix.setRotate(90);

            Bitmap original_copy_bitmap = Bitmap.createBitmap(original_bitmap, 0,0,original_bitmap.getWidth(), original_bitmap.getHeight(), matrix,false);
            bitmap = Bitmap.createBitmap(original_copy_bitmap, 0,(original_copy_bitmap.getHeight())/4,original_copy_bitmap.getWidth(),
                    (original_copy_bitmap.getHeight()*3)/4);
            int s_imagewidth = (int) (polWidth-2*(offset));
            bitmap = getResizedBitmap(bitmap, s_imagewidth,s_imagewidth);

            //fullbitmap=Bitmap.createBitmap(fullbitmap, 0,0,fullbitmap.getWidth(), (fullbitmap.getHeight()*3)/4);
            if(original_bitmap == null) Toast.makeText(this, "Try again!", Toast.LENGTH_SHORT).show();
        } catch (IOException e) {
            Toast.makeText(this, "Try again!", Toast.LENGTH_SHORT).show();
        }

        frontbitmap = Bitmap.createBitmap(polWidth, polHeight,Bitmap.Config.ARGB_8888);
        backbitmap = Bitmap.createBitmap(polWidth, polHeight,Bitmap.Config.ARGB_8888);

        //frontbitmap = getResizedBitmap(frontbitmap, polWidth, polHeight);
       // backbitmap = getResizedBitmap(backbitmap, polWidth, polHeight);

        canvas = new Canvas(frontbitmap);
        bcanvas = new Canvas(backbitmap);

        drawcanvas(pol_colorname);

        paintdate(colorname,locationsize,tf);

        font_type.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch (pos) {
                    case 1: pos=2;
                        font = MARKER;
//                        Toast.makeText(EditPhotoActivity.this, "MYUNDERWOOD", Toast.LENGTH_SHORT).show();
                        break;
                    case 2: pos=3;
                        font = SANDS;
//                        Toast.makeText(EditPhotoActivity.this, "SANDS", Toast.LENGTH_SHORT).show();
                        break;
                    case 3: pos=4;
                        font = NEWDAY;
//                        Toast.makeText(EditPhotoActivity.this, "NEWDAY", Toast.LENGTH_SHORT).show();
                        break;
                    case 4: pos=5;
                        font = ROCKSALT;
//                        Toast.makeText(EditPhotoActivity.this, "ROCKSALT", Toast.LENGTH_SHORT).show();
                        break;
                    case 5: pos=6;
                        font = SATISFY;
//                        Toast.makeText(EditPhotoActivity.this, "SATISFY", Toast.LENGTH_SHORT).show();
                        break;
                    case 6: pos=7;
                        font = MYUNDERWOOD;
//                        Toast.makeText(EditPhotoActivity.this, "MARKER", Toast.LENGTH_SHORT).show();
                        break;
                    case 7: pos=8;
                        font = HOMEMADEAPPLE;
//                        Toast.makeText(EditPhotoActivity.this, "HOMEMADEAPPLE", Toast.LENGTH_SHORT).show();
                        break;
                    case 8: pos=9;
                        font = LOBSTER;
//                        Toast.makeText(EditPhotoActivity.this, "HOMEMADEAPPLE", Toast.LENGTH_SHORT).show();
                        break;
                    case 9: pos=10;
                        font = PLAYFAIRDISPLAY;
//                        Toast.makeText(EditPhotoActivity.this, "HOMEMADEAPPLE", Toast.LENGTH_SHORT).show();
                        break;
                    case 10: pos=1;
                        font = DAFOE;
//                        Toast.makeText(EditPhotoActivity.this, "DAFOE", Toast.LENGTH_SHORT).show();
                        break;
                }
                Typeface tfc = ResourcesCompat.getFont(getApplicationContext(), font);
                writecaption(caption_edit.getText().toString(),tfc,colorname,pol_colorname);
                writedetail(detail_edit.getText().toString(),tfc, colorname,pol_colorname);
//                paintdate(colorname,locationsize,tf);
            }
        });

        font_color.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                switch (col) {
                    case 1: col=2;
                        colorname = R.color.black;
                        Toast.makeText(EditPhotoActivity.this, "black", Toast.LENGTH_SHORT).show();
                        break;
                    case 2: col=1;
                        colorname = R.color.sketch_black;
                        Toast.makeText(EditPhotoActivity.this, "sketch_black", Toast.LENGTH_SHORT).show();
                        break;
                }

                Typeface tfc = ResourcesCompat.getFont(getApplicationContext(), font);
                writecaption(caption_edit.getText().toString(),tfc, colorname,pol_colorname);
                writedetail(detail_edit.getText().toString(),tfc, colorname, pol_colorname);
                paintdate(colorname,locationsize,tf);

            }
        });

        pol_color.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //Toast.makeText(EditPhotoActivity.this, "check", Toast.LENGTH_SHORT).show();
                switch (pol) {
                    case 1: pol=2;
                        pol_colorname = R.color.black;
                        //Toast.makeText(EditPhotoActivity.this, "again", Toast.LENGTH_SHORT).show();
                        break;
                    case 2: pol=1;
                        pol_colorname = R.color.white;
                        //Toast.makeText(EditPhotoActivity.this, "again2", Toast.LENGTH_SHORT).show();
                        break;
                }

                drawcanvas(pol_colorname);

                Typeface tfc = ResourcesCompat.getFont(getApplicationContext(), font);
                writecaption(caption_edit.getText().toString(),tfc, colorname, pol_colorname);
                writedetail(detail_edit.getText().toString(),tfc, colorname, pol_colorname);
                paintdate(colorname,locationsize,tf);
            }
        });

        mCardFrontLayout.setOnTouchListener(new OnSwipeTouchListener(this) {

            @Override
            public void onSwipeLeft() {
                super.onSwipeLeft();
                    if (aflag == 0) {
                        if (mIsBackVisible)
                            loadAnimations();
                        else
                            loadleftAnimations();

                        flipCard(mCardBackLayout);
                    }
            }

            @Override
            public void onSwipeRight() {
                super.onSwipeRight();
                    if (aflag == 0) {
                        if (!mIsBackVisible)
                            loadAnimations();
                        else
                            loadleftAnimations();
                        flipCard(mCardBackLayout);
                    }
            }

//            @Override
//            public void onLongClicked() {
//                super.onLongClicked();
//                Toast.makeText(EditPhotoActivity.this, "Full Memory View", Toast.LENGTH_SHORT).show();
//                Intent in1 = new Intent(EditPhotoActivity.this, FullMemoryViewActivity.class);
//                in1.putExtra("fullMemory", photo);
//                startActivity(in1);
//            }
        });
        mCardBackLayout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                float x = event.getX();
                float y = event.getY();

                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        touch_start(x, y);
                        imageViewf.invalidate();
                        onDraw(bcanvas);
                        break;
                    case MotionEvent.ACTION_MOVE:
                        touch_move(x, y);
                        imageViewf.invalidate();
                        onDraw(bcanvas);
                        break;
                    case MotionEvent.ACTION_UP:
                        touch_up();
                        imageViewf.invalidate();
                        onDraw(bcanvas);
                        break;
                }
                return true;
            }
        });

        caption_edit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                //caption
                Typeface tfca = ResourcesCompat.getFont(getApplicationContext(), font);
                title = editable.toString();
                paintdate(colorname,locationsize,tf);
                writecaption(title, tfca, colorname, pol_colorname);
            }
        });

        detail_edit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

                //detail
                tfd = ResourcesCompat.getFont(getApplicationContext(), font);
                detail = editable.toString();
                writedetail(detail, tfd, colorname, pol_colorname);

            }
        });

        prev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which){
                            case DialogInterface.BUTTON_POSITIVE:
                                //Yes button clicked
                                startActivity(new Intent(EditPhotoActivity.this, CameraCaptureActivity.class));
                                finish();
                                break;

                            case DialogInterface.BUTTON_NEGATIVE:
                                //No button clicked
                                break;
                        }
                    }
                };


                AlertDialog.Builder builder = new AlertDialog.Builder(EditPhotoActivity.this,R.style.DatePickerTheme);

                builder.setMessage("Are you sure? All progress will be lost.").setPositiveButton("Yes", dialogClickListener)
                        .setNegativeButton("No", dialogClickListener).show();
            }
        });

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                try {
                    Toast.makeText(EditPhotoActivity.this, "Processing memory...", Toast.LENGTH_SHORT).show();
                    //Write file
                    String filename = "bitmap1.png";
                    String filename2 = "bitmap2.png";
                    FileOutputStream stream = EditPhotoActivity.this.openFileOutput(filename, Context.MODE_PRIVATE);
                    FileOutputStream stream2 = EditPhotoActivity.this.openFileOutput(filename2, Context.MODE_PRIVATE);
                    Bitmap compressed_frontbitmap = frontbitmap.copy(frontbitmap.getConfig(),true);
                    Bitmap compressed_backbitmap = backbitmap.copy(backbitmap.getConfig(),true);
                    compressed_frontbitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
                    compressed_backbitmap.compress(Bitmap.CompressFormat.PNG, 100, stream2);

                    //Cleanup
                    stream.close();
                    stream2.close();

                    compressed_frontbitmap.recycle();
                    compressed_backbitmap.recycle();

                    //Pop intent
                    Intent in1 = new Intent(EditPhotoActivity.this, PhotoActivity.class);
                    in1.putExtra("front", filename);
                    in1.putExtra("back",filename2);
                    in1.putExtra("quickID",quickID);
                    in1.putExtra("Direct",fromfeed);
                    in1.putExtra("sself",sself);
                    in1.putExtra("warloc",warloc);
                    in1.putExtra("cell", boxId);
                    startActivity(in1);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });

        doodle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                aflag=1;
                instruction.setVisibility(View.VISIBLE);
                instruction.setText("Drawing mode");
                tray.setVisibility(GONE);
                rl.setVisibility(GONE);
                doodle.setVisibility(GONE);
                mCardBackLayout.setElevation(20f);
                mCardFrontLayout.setElevation(8f);
                doodle_right.setVisibility(View.VISIBLE);
                doodle_wrong.setVisibility(View.VISIBLE);
                //doodle_undo.setVisibility(View.VISIBLE);
                color_tray.setVisibility(View.VISIBLE);
                brush_eraser_layout.setVisibility(View.VISIBLE);
//                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HALF_EXPANDED);
            }
        });

        doodle_right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                aflag=0;
                instruction.setVisibility(GONE);
                mCardFrontLayout.setElevation(9f);
                mCardBackLayout.setElevation(8f);
                instruction.setText("Swipe to write a message behind");
                tray.setVisibility(View.VISIBLE);
                rl.setVisibility(View.VISIBLE);
                doodle_right.setVisibility(GONE);
                doodle_wrong.setVisibility(GONE);
                doodle.setVisibility(View.VISIBLE);
                doodle_undo.setVisibility(GONE);
                color_tray.setVisibility(GONE);
                brush_eraser_layout.setVisibility(View.GONE);
            }
        });

        doodle_wrong.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                aflag=0;
                instruction.setVisibility(GONE);
                mCardFrontLayout.setElevation(9f);
                mCardBackLayout.setElevation(8f);
                instruction.setText("Swipe to write a message behind");
                tray.setVisibility(View.VISIBLE);
                rl.setVisibility(View.VISIBLE);
                doodle_right.setVisibility(GONE);
                doodle_wrong.setVisibility(GONE);
                doodle.setVisibility(View.VISIBLE);
                doodle_undo.setVisibility(GONE);
                color_tray.setVisibility(GONE);
                brush_eraser_layout.setVisibility(View.GONE);
                paintDetailRect(pol_colorname);
                writedetail(detail_edit.getText().toString(),tfc, colorname,pol_colorname);
            }
        });

        doodle_undo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                color_tray.setVisibility(View.VISIBLE);

            }
        });

        color_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //draw_colorname = color1;
                doodle_paint.setColor(getResources().getColor(R.color.color1));
                color_1.setBorderWidth(5);
                color_2.setBorderWidth(0);
                color_3.setBorderWidth(0);
                color_4.setBorderWidth(0);
                color_5.setBorderWidth(0);
                color_6.setBorderWidth(0);
                color_7.setBorderWidth(0);
                color_8.setBorderWidth(0);
                color_9.setBorderWidth(0);
                color_10.setBorderWidth(0);

            }
        });

        color_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //draw_colorname = color2;
                doodle_paint.setColor(getResources().getColor(R.color.color2));
                color_1.setBorderWidth(0);
                color_2.setBorderWidth(5);
                color_3.setBorderWidth(0);
                color_4.setBorderWidth(0);
                color_5.setBorderWidth(0);
                color_6.setBorderWidth(0);
                color_7.setBorderWidth(0);
                color_8.setBorderWidth(0);
                color_9.setBorderWidth(0);
                color_10.setBorderWidth(0);

            }
        });

        color_3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //draw_colorname = color3;
                doodle_paint.setColor(getResources().getColor(R.color.color3));
                color_1.setBorderWidth(0);
                color_2.setBorderWidth(0);
                color_3.setBorderWidth(5);
                color_4.setBorderWidth(0);
                color_5.setBorderWidth(0);
                color_6.setBorderWidth(0);
                color_7.setBorderWidth(0);
                color_8.setBorderWidth(0);
                color_9.setBorderWidth(0);
                color_10.setBorderWidth(0);

            }
        });

        color_4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //draw_colorname = color4;
                doodle_paint.setColor(getResources().getColor(R.color.color4));
                color_1.setBorderWidth(0);
                color_2.setBorderWidth(0);
                color_3.setBorderWidth(0);
                color_4.setBorderWidth(5);
                color_5.setBorderWidth(0);
                color_6.setBorderWidth(0);
                color_7.setBorderWidth(0);
                color_8.setBorderWidth(0);
                color_9.setBorderWidth(0);
                color_10.setBorderWidth(0);

            }
        });

        color_5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //draw_colorname = R.color.sketch_black;
                doodle_paint.setColor(getResources().getColor(R.color.sketch_black));
                color_1.setBorderWidth(0);
                color_2.setBorderWidth(0);
                color_3.setBorderWidth(0);
                color_4.setBorderWidth(0);
                color_5.setBorderWidth(5);
                color_6.setBorderWidth(0);
                color_7.setBorderWidth(0);
                color_8.setBorderWidth(0);
                color_9.setBorderWidth(0);
                color_10.setBorderWidth(0);

            }
        });

        color_6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //draw_colorname = R.color.sketch_black;
                doodle_paint.setColor(getResources().getColor(R.color.color6));
                color_1.setBorderWidth(0);
                color_2.setBorderWidth(0);
                color_3.setBorderWidth(0);
                color_4.setBorderWidth(0);
                color_5.setBorderWidth(0);
                color_6.setBorderWidth(5);
                color_7.setBorderWidth(0);
                color_8.setBorderWidth(0);
                color_9.setBorderWidth(0);
                color_10.setBorderWidth(0);

            }
        });

        color_7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //draw_colorname = R.color.sketch_black;
                doodle_paint.setColor(getResources().getColor(R.color.color7));
                color_1.setBorderWidth(0);
                color_2.setBorderWidth(0);
                color_3.setBorderWidth(0);
                color_4.setBorderWidth(0);
                color_5.setBorderWidth(0);
                color_6.setBorderWidth(0);
                color_7.setBorderWidth(5);
                color_8.setBorderWidth(0);
                color_9.setBorderWidth(0);
                color_10.setBorderWidth(0);

            }
        });

        color_8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //draw_colorname = R.color.sketch_black;
                doodle_paint.setColor(getResources().getColor(R.color.color8));
                color_1.setBorderWidth(0);
                color_2.setBorderWidth(0);
                color_3.setBorderWidth(0);
                color_4.setBorderWidth(0);
                color_5.setBorderWidth(0);
                color_6.setBorderWidth(0);
                color_7.setBorderWidth(0);
                color_8.setBorderWidth(5);
                color_9.setBorderWidth(0);
                color_10.setBorderWidth(0);

            }
        });

        color_9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //draw_colorname = R.color.sketch_black;
                doodle_paint.setColor(getResources().getColor(R.color.color9));
                color_1.setBorderWidth(0);
                color_2.setBorderWidth(0);
                color_3.setBorderWidth(0);
                color_4.setBorderWidth(0);
                color_5.setBorderWidth(0);
                color_6.setBorderWidth(0);
                color_7.setBorderWidth(0);
                color_8.setBorderWidth(0);
                color_9.setBorderWidth(5);
                color_10.setBorderWidth(0);

            }
        });

        color_10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //draw_colorname = R.color.sketch_black;
                doodle_paint.setColor(getResources().getColor(R.color.color10));
                color_1.setBorderWidth(0);
                color_2.setBorderWidth(0);
                color_3.setBorderWidth(0);
                color_4.setBorderWidth(0);
                color_5.setBorderWidth(0);
                color_6.setBorderWidth(0);
                color_7.setBorderWidth(0);
                color_8.setBorderWidth(0);
                color_9.setBorderWidth(0);
                color_10.setBorderWidth(5);

            }
        });


        doodle_paint.setColor(getResources().getColor(draw_colorname));

        imageView.setImageBitmap(frontbitmap);
        imageViewf.setImageBitmap(backbitmap);

    }

    private float mX, mY;
    private static final float TOUCH_TOLERANCE = 4;

    private void touch_start(float x, float y) {
        mPath.reset();
        mPath.moveTo(x, y);
        mX = x;
        mY = y;
    }

    private Bitmap rotateImageIfRequired(Bitmap img, Uri selectedImage) throws IOException
    {
        ExifInterface ei = new ExifInterface(selectedImage.getPath());
        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
//        Toast.makeText(EditPhotoActivity.this, String.valueOf(orientation), Toast.LENGTH_SHORT).show();

        switch (orientation) {
            case ExifInterface.ORIENTATION_ROTATE_90:
                return rotateImage(img, 90);
            case ExifInterface.ORIENTATION_ROTATE_180:
                return rotateImage(img, 180);
            case ExifInterface.ORIENTATION_ROTATE_270:
                Bitmap src = rotateImage(img, 270);
                Matrix matrix = new Matrix();
                matrix.preScale(-1.0f, 1.0f);
                return Bitmap.createBitmap(src, 0, 0, src.getWidth(), src.getHeight(), matrix, true);
            default:
                return img;
        }
    }

    private static Bitmap rotateImage(Bitmap img, int degree)
    {
        Matrix matrix = new Matrix();
        matrix.postRotate(degree);
        Bitmap rotatedImg = Bitmap.createBitmap(img, 0, 0, img.getWidth(), img.getHeight(), matrix, true);
        img.recycle();
        return rotatedImg;
    }

    private void touch_move(float x, float y) {
        float dx = Math.abs(x - mX);
        float dy = Math.abs(y - mY);
        if (dx >= TOUCH_TOLERANCE || dy >= TOUCH_TOLERANCE) {
            mPath.quadTo(mX, mY, (x + mX)/2, (y + mY)/2);
            mX = x;
            mY = y;

            circlePath.reset();
            circlePath.addCircle(mX, mY, 30, Path.Direction.CW);
        }
    }

    private void touch_up() {
        mPath.lineTo(mX, mY);
        circlePath.reset();
        // commit the path to our offscreen
        bcanvas.drawPath(mPath,  doodle_paint);
        // kill this so we don't double draw
        mPath.reset();
    }

    private void onDraw(Canvas canvas) {

        //canvas.drawBitmap( backbitmap, 0, 0, mBitmapPaint);
        for (Path p : paths){
            canvas.drawPath(p, doodle_paint);
        }
        canvas.drawPath( mPath,  doodle_paint);
        //canvas.drawPath( circlePath,  circlePaint);

    }

    private String getCurrentDate() {
        Date c = Calendar.getInstance().getTime();
        System.out.println("Current time => " + c);

        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
        String formattedDate = df.format(c);
        return formattedDate;
    }

    private void paintdate(int colorname, int locationsize, Typeface tf) {

        //date text
        Paint date = getTextPaint(locationsize,1,colorname);
        date.setTypeface(tf);
        date.setAntiAlias(true);
        Rect daterect = new Rect();
        date.getTextBounds(dated,0,dated.length(),daterect);
        int dx = (canvas.getWidth() / 2) - (daterect.width() / 2);
//        canvas.drawText(dated, dx, bitmap.getHeight()+5*offset+padding, date);
    }

    private void drawcanvas(int colorname) {
        canvas.drawColor(getResources().getColor(colorname));
        bcanvas.drawColor(getResources().getColor(colorname));
        canvas.drawBitmap(bitmap,offset,offset,null);
    }

    private void writecaption(String title, Typeface typeface, int fontcolor, int rectcolor) {

        if (!title.equalsIgnoreCase("")) {
            paintCaptionRect(rectcolor);
            caption = getTextPaint(capsize,1,fontcolor);
            caption.setAntiAlias(true);
            caption.setTextSize(capsize);
            caption.setTypeface(Typeface.create(typeface, Typeface.BOLD));
            caption.getTextBounds(title, 0, title.length(), caprect);
            final int x = (canvas.getWidth() / 2) - (caprect.width() / 2);
            canvas.drawText(title, x, bitmap.getHeight() + 3 * offset + caprect.height() / 2 - padding, caption);

        }
        else{
            paintCaptionRect(rectcolor);
        }

        imageView.setImageBitmap(frontbitmap);
        imageViewf.setImageBitmap(backbitmap);

    }

    private void writedetail(String detail, Typeface typeface, int fontcolor, int rectcolor) {

        if(!detail.equalsIgnoreCase("")) {
            paintDetailRect(rectcolor);
            mTextPaint=new TextPaint();
            mTextPaint.setAntiAlias(true);
            mTextPaint.setTextSize(capsize);
            mTextPaint.setColor(getResources().getColor(fontcolor));
            mTextPaint.setTypeface(Typeface.create(typeface, Typeface.BOLD));
            mTextPaint.getTextBounds(detail, 0, detail.length(), caprect);


            StaticLayout mTextLayout = new StaticLayout(detail, mTextPaint, bcanvas.getWidth()-200
                    , Layout.Alignment.ALIGN_CENTER, 1.0f, 0.0f, false);

            bcanvas.save();
            int ycenter = (bcanvas.getHeight() / 2);
            int xt = 100;
            int yt = (ycenter) -
                    ((capsize * mTextLayout.getLineCount()) / 2);

            bcanvas.translate(xt, yt);
            mTextLayout.draw(bcanvas);
            bcanvas.restore();

            //bcanvas.drawText(detail, xt, yt, capt);
        }

        else {
            paintDetailRect(rectcolor);
        }

        imageView.setImageBitmap(frontbitmap);
        imageViewf.setImageBitmap(backbitmap);

    }

    private void paintDetailRect(int colorname) {
        Paint paint = new Paint();
        paint.setColor(getResources().getColor(colorname));
        Path path = new Path();
        RectF rectF = new RectF(0,0,bcanvas.getWidth(),bcanvas.getHeight());
        path.addRect(rectF,Path.Direction.CW);
        paint.setStrokeWidth(captrect.width());
        paint.setStyle(Paint.Style.FILL);
        bcanvas.drawPath(path,paint);
    }

    private void paintCaptionRect(int colorname) {
        Paint paint = new Paint();
        paint.setColor(getResources().getColor(colorname));
        Path path = new Path();
        RectF rectF = new RectF(0, bitmap.getHeight() + offset, canvas.getWidth(), bitmap.getHeight() + 3 * offset + caprect.height() + 2*padding);
        path.addRect(rectF, Path.Direction.CW);
        paint.setStrokeWidth(caprect.width());
        paint.setStyle(Paint.Style.FILL);
        canvas.drawPath(path, paint);
    }

    private void check() {
        if (!mIsBackVisible) {
            caption_edit.setVisibility(View.VISIBLE);
            detail_edit.setVisibility(GONE);
            instruction.setVisibility(View.VISIBLE);
            doodle.setVisibility(GONE);

        } else {
            detail_edit.setVisibility(View.VISIBLE);
            caption_edit.setVisibility(GONE);
            instruction.setVisibility(GONE);
            doodle.setVisibility(View.VISIBLE);
        }
    }

    Bitmap getRoundedCornerBitmap(Bitmap bitmap, float roundPx) {
        Bitmap output = Bitmap.createBitmap(bitmap.getWidth(),
                bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);

        final int color = 0xff424242;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        final RectF rectF = new RectF(rect);

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawRoundRect(rectF, roundPx, roundPx, paint);

        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);

        return output;
    }

    Bitmap getResizedBitmap(Bitmap bm, int newWidth, int newHeight) {
        int width = bm.getWidth();
        int height = bm.getHeight();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        // CREATE A MATRIX FOR THE MANIPULATION
        Matrix matrix = new Matrix();
        // RESIZE THE BIT MAP
        matrix.postScale(scaleWidth, scaleHeight);

        // "RECREATE" THE NEW BITMAP
        Bitmap resizedBitmap = Bitmap.createBitmap(
                bm, 0, 0, width, height, matrix, false);
        return resizedBitmap;
    }

    Paint getTextPaint(float size, int alpha, int color) {
        Paint paint = new Paint();
        paint.setTextSize(size);
        paint.setAlpha(alpha);
        paint.setColor(getResources().getColor(color));
        return paint;
    }

    Paint setTextSizeForWidth(Paint paint, float desiredWidth,
                              String text) {

        // Pick a reasonably large value for the test. Larger values produce
        // more accurate results, but may cause problems with hardware
        // acceleration. But there are workarounds for that, too; refer to
        // http://stackoverflow.com/questions/6253528/font-size-too-large-to-fit-in-cache
        final float testTextSize = 48f;

        // Get the bounds of the text, using our testTextSize.
        paint.setTextSize(testTextSize);
        Rect bounds = new Rect();
        paint.getTextBounds(text, 0, text.length(), bounds);

        // Calculate the desired size as a proportion of our testTextSize.
        float desiredTextSize = testTextSize * desiredWidth / bounds.width();

        // Set the paint for that size.
        paint.setTextSize(desiredTextSize);

        return paint;
    }

    //animation functions

    private void changeCameraDistance() {
        int distance = 8000;
        float scale = getResources().getDisplayMetrics().density * distance;
        mCardFrontLayout.setCameraDistance(scale);
        mCardBackLayout.setCameraDistance(scale);
    }

    private void loadAnimations() {
        mSetRightOut = (AnimatorSet) AnimatorInflater.loadAnimator(this, R.animator.out_animation);
        mSetLeftIn = (AnimatorSet) AnimatorInflater.loadAnimator(this, R.animator.in_animation);
    }

    private void loadleftAnimations() {
        mSetRightOut = (AnimatorSet) AnimatorInflater.loadAnimator(this, R.animator.out_animation_left);
        mSetLeftIn = (AnimatorSet) AnimatorInflater.loadAnimator(this, R.animator.in_animation_left);
    }

    public void flipCard(View view) {
        if (!mIsBackVisible) {
            mSetRightOut.setTarget(mCardFrontLayout);
            mSetLeftIn.setTarget(mCardBackLayout);
            mSetRightOut.start();
            mSetLeftIn.start();
            mIsBackVisible = true;
            check();
        } else {
            mSetRightOut.setTarget(mCardBackLayout);
            mSetLeftIn.setTarget(mCardFrontLayout);
            mSetRightOut.start();
            mSetLeftIn.start();
            mIsBackVisible = false;
            check();
        }
    }

    private float pxFromDp(float dp)
    {
        return dp * getResources().getDisplayMetrics().density;
    }

}