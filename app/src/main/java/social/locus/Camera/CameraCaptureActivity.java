package social.locus.Camera;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Matrix;
import android.graphics.Point;
import android.graphics.Rect;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.Rational;
import android.view.Display;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowInsets;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.camera.core.CameraInfoUnavailableException;
import androidx.camera.core.CameraX;
import androidx.camera.core.ImageAnalysis;
import androidx.camera.core.ImageAnalysisConfig;
import androidx.camera.core.ImageCapture;
import androidx.camera.core.ImageCaptureConfig;
import androidx.camera.core.Preview;
import androidx.camera.core.PreviewConfig;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import social.locus.LocationUpdatesService;
import social.locus.R;

import java.io.File;

public class
CameraCaptureActivity extends AppCompatActivity {
    private final static int REQUEST_CODE_PERMISSION = 10;
    private final static String[] REQUIRED_PERMISSIONS = new String[]{Manifest.permission.CAMERA};

    private TextureView viewFinder;
    View camera_rect;
    private CameraX.LensFacing lensFacing;

    Uri imageUri;
    private String quickID="",fromfeed,sself;
    private float screenRatio;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cameracapture);

        quickID = getIntent().getStringExtra("quickID");
        fromfeed = getIntent().getStringExtra("Direct");
        sself = getIntent().getStringExtra("sself");


        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        WindowManager w = this.getWindowManager();
        Display d = w.getDefaultDisplay();
        Point realSize = new Point();
        try {
            Display.class.getMethod("getRealSize", Point.class).invoke(d, realSize);
        } catch (Exception e) {
            e.printStackTrace();
        }

        int width = displayMetrics.widthPixels;
        screenRatio=(float)realSize.y/realSize.x;
//        Toast.makeText(this, ""+screenRatio, Toast.LENGTH_SHORT).show();
        viewFinder = findViewById(R.id.view_finder);

        viewFinder.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
            @Override
            public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                // Every time the provided texture view changes, recompute layout
//                updateTransform();
                ViewGroup.LayoutParams params_cam = viewFinder.getLayoutParams();
                params_cam.height = 4* width/3;
                viewFinder.setLayoutParams(params_cam);
            }
        });
//        if (allPermissionsGranted()) {
//            viewFinder.post(new Runnable() {
//                @Override
//                public void run() {
//                    startCamera();
//                }
//            });
//        } else {
//            ActivityCompat.requestPermissions(
//                    this, REQUIRED_PERMISSIONS, REQUEST_CODE_PERMISSION);
//        }

//        View bottom_bar = findViewById(R.id.bottom_bar);
        View top_bar = findViewById(R.id.top_bar);

//        int height = displayMetrics.heightPixels

        int upper_cut = width/3;
//        int lower_cut = height-width-upper_cut;

        ViewGroup.LayoutParams params_top = top_bar.getLayoutParams();
        params_top.height = upper_cut;
        top_bar.setLayoutParams(params_top);

//        ViewGroup.LayoutParams params_bottom = bottom_bar.getLayoutParams();
//        params_bottom.height = lower_cut;
//        bottom_bar.setLayoutParams(params_bottom);
    }

    @Override
    protected void onResume() {
        super.onResume();

//        View decorView = getWindow().getDecorView();
//        int uiOptions = View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
//                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
//                | View.SYSTEM_UI_FLAG_FULLSCREEN;
//        decorView.setSystemUiVisibility(uiOptions);
//
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
//            getWindow().setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
//            getWindow().getAttributes().layoutInDisplayCutoutMode = WindowManager.LayoutParams.LAYOUT_IN_DISPLAY_CUTOUT_MODE_SHORT_EDGES;
//        }
        if (allPermissionsGranted()) {
            viewFinder.post(new Runnable() {
                @Override
                public void run() {
                    startCamera();
                }
            });
        } else {
            ActivityCompat.requestPermissions(
                    this, REQUIRED_PERMISSIONS, REQUEST_CODE_PERMISSION);
        }
    }

    private void updateTransform() {
        Matrix matrix = new Matrix();

        float scaleX = screenRatio/((float)16/9);
        float scaleY = 1f;

        // Compute the center of the view finder
        float centerX = viewFinder.getWidth() / 2;
        float centerY = viewFinder.getHeight() / 2;
        matrix.preScale(scaleX,scaleY,centerX,centerY);

        // Correct preview output to account for display rotation
        int rotationDegrees;
        switch (viewFinder.getDisplay().getRotation()) {
            case Surface.ROTATION_0:
                rotationDegrees = 0;
                break;
            case Surface.ROTATION_90:
                rotationDegrees = 90;
                break;
            case Surface.ROTATION_180:
                rotationDegrees = 180;
                break;
            case Surface.ROTATION_270:
                rotationDegrees = 270;
                break;
            default:
                return;
        }
//        Toast.makeText(this, String.valueOf(rotationDegrees), Toast.LENGTH_SHORT).show();
        matrix.postRotate(rotationDegrees, centerX, centerY);

        // Finally, apply transformations to our TextureView
        viewFinder.setTransform(matrix);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_CODE_PERMISSION) {
            if (allPermissionsGranted()) {
                viewFinder.post(new Runnable() {
                    @Override
                    public void run() {
                        startCamera();
                    }
                });
            }
        }
    }

    private void startCamera() {
        lensFacing = CameraX.LensFacing.BACK;
        bindCamerausecaes();

        findViewById(R.id.reverse).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (CameraX.LensFacing.FRONT == lensFacing) {
                    lensFacing = CameraX.LensFacing.BACK;
                } else {
                    lensFacing = CameraX.LensFacing.FRONT;
                }
                // Only bind use cases if we can query a camera with this orientation
                try {
                    CameraX.getCameraControl(lensFacing);
                    bindCamerausecaes();

                } catch (CameraInfoUnavailableException e) {
                    Log.e("","CameraInfoUnavailableException");
                    e.printStackTrace();
                }


            }
        });

    }

    private boolean allPermissionsGranted() {
        for (String perm : REQUIRED_PERMISSIONS) {
            if (ContextCompat.checkSelfPermission(getBaseContext(), perm) != PackageManager.PERMISSION_GRANTED) {
                return false;
            }
        }
        return true;
    }

    private void bindCamerausecaes(){
        CameraX.unbindAll();

        // Create configuration object for the viewfinder use case
        PreviewConfig previewConfig = new PreviewConfig.Builder()
                .setTargetAspectRatio(new Rational(3, 4))
                .setLensFacing(lensFacing)
                .build();

        // Build the viewfinder use case
        Preview preview = new Preview(previewConfig);

        // Everytime the viewfinder is updated, recompute the layout
        preview.setOnPreviewOutputUpdateListener(new Preview.OnPreviewOutputUpdateListener() {
            @Override
            public void onUpdated(Preview.PreviewOutput output) {
                // To update the SurfaceTexture, we have to remove it and re-add it
                ViewGroup parent = (ViewGroup) viewFinder.getParent();
                parent.removeView(viewFinder);
                parent.addView(viewFinder, 0);

                viewFinder.setSurfaceTexture(output.getSurfaceTexture());
//                updateTransform();

            }
        });


        // Create configuration object for the image capture use case
        ImageCaptureConfig imageCaptureConfig = new ImageCaptureConfig.Builder()
                .setTargetAspectRatio(new Rational(3, 4))
                .setCaptureMode(ImageCapture.CaptureMode.MIN_LATENCY)
                .setLensFacing(lensFacing)
                .build();
        final ImageCapture imageCapture = new ImageCapture(imageCaptureConfig);
        findViewById(R.id.capture_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                File existing_file = new File(getExternalFilesDir(Environment.DIRECTORY_PICTURES),
//                        "LOCUS_CAPTURE_IMAGE.enc");
//                if(existing_file.exists()) {
//                    existing_file.delete();
//                }

                File file = new File(getExternalFilesDir(Environment.DIRECTORY_PICTURES),
                        "LOCUS_CAPTURE_IMAGE"+System.currentTimeMillis()+".enc");


                imageCapture.takePicture(file, new ImageCapture.OnImageSavedListener() {
                    @Override
                    public void onImageSaved(@NonNull File file) {
//                        Toast.makeText(CameraCaptureActivity.this, "Photo saved as " + file.getAbsolutePath(), Toast.LENGTH_SHORT).show();
                        imageUri = Uri.fromFile(file);
                        CameraX.unbindAll();
                        Location warloc = new Location("");
                        warloc.setLatitude(LocationUpdatesService.mLocation.getLatitude());
                        warloc.setLongitude(LocationUpdatesService.mLocation.getLongitude());
                        String boxId = String.valueOf(LocationUpdatesService.cell2_1.id());
                        Intent i = new Intent(CameraCaptureActivity.this, EditPhotoActivity.class);
                        i.putExtra("photo",imageUri);
                        i.putExtra("quickID",quickID);
                        i.putExtra("Direct",fromfeed);
                        i.putExtra("sself",sself);
                        i.putExtra("warloc",warloc);
                        i.putExtra("cell",boxId);
                        startActivity(i);
                        finish();
                    }

                    @Override
                    public void onError(@NonNull ImageCapture.ImageCaptureError imageCaptureError, @NonNull String message, @Nullable Throwable cause) {
                        Toast.makeText(CameraCaptureActivity.this, "Couldn't save photo: " + message, Toast.LENGTH_SHORT).show();
                        if (cause != null)
                            cause.printStackTrace();
                    }
                });
            }
        });
        // Setup image analysis pipeline that computes average pixel luminance
        // TODO add analyzerThread and setCallbackHandler as in the original example in Kotlin
        ImageAnalysisConfig analysisConfig = new ImageAnalysisConfig.Builder()
                .setImageReaderMode(ImageAnalysis.ImageReaderMode.ACQUIRE_LATEST_IMAGE)
                .setLensFacing(lensFacing)
                .build();

//        MeteringPointFactory factory = new SensorOrientedMeteringPointFactory(width, height);
//        MeteringPoint point = factory.createPoint(x, y);
//        FocusMeteringAction action = FocusMeteringAction.Builder.from(point,
//                FocusMeteringAction.MeteringMode.AF_ONLY)
//                .addPoint(point2, FocusMeteringAction.MeteringMode.AE_ONLY) // could have many
//                .setAutoFocusCallback(new FocusMeteringAction.OnAutoFocusListener(){
//                    public void onFocusCompleted(boolean isSuccess) {
//                    }
//                })
//                // auto calling cancelFocusAndMetering in 5 seconds
//                .setAutoCancelDuration(5, TimeUnit.SECONDS)
//                .build();


        // Build the image analysis use case and instantiate our analyzer
//        ImageAnalysis imageAnalysis = new ImageAnalysis(analysisConfig);
//        imageAnalysis.setAnalyzer(new LuminosityAnalyzer());
        // Bind use cases to lifecycle
        CameraX.bindToLifecycle(this, preview, imageCapture); //imageAnalysis);

    }
}
