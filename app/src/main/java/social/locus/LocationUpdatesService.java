/**
 * Copyright 2017 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package social.locus;

import static android.app.PendingIntent.FLAG_UPDATE_CURRENT;

import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.media.AudioAttributes;
import android.net.Uri;
import android.os.Binder;
import android.os.Build;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.util.Log;
import android.widget.Toast;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.common.geometry.S2CellId;
import com.google.common.geometry.S2LatLng;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import social.locus.Beans.Warhead;
import social.locus.Drops.dropViewActivity;

//import androidx.annotation.NonNull;
//import androidx.core.app.NotificationCompat;
//import androidx.localbroadcastmanager.content.LocalBroadcastManager;

/**
 * A bound and started service that is promoted to a foreground service when location updates have
 * been requested and all clients unbind.
 *
 * For apps running in the background on "O" devices, location is computed only once every 10
 * minutes and delivered batched every 30 minutes. This restriction applies even to apps
 * targeting "N" or lower which are run on "O" devices.
 *
 * This sample show how to use a long-running service for location updates. When an activity is
 * bound to this service, frequent location updates are permitted. When the activity is removed
 * from the foreground, the service promotes itself to a foreground service, and location updates
 * continue. When the activity comes back to the foreground, the foreground service stops, and the
 * notification assocaited with that service is removed.
 */
public class LocationUpdatesService extends Service {

    public static boolean isRunning = false;

    private static final String PACKAGE_NAME =
            "social.locus.Fragments.locationupdatesforegroundservice";

    private static final String TAG = "resPOINT";

    String id = FirebaseAuth.getInstance().getCurrentUser().getUid();

    /**
     *
     * The name of the channel for notifications.
     */

    public static ArrayList<S2CellId> cell_list = new ArrayList<S2CellId>() ;

    private static final String CHANNEL_ID = "TILE_CHANNEL";

    public static Location current_location_from_Service=  new Location("Service");

    static final String LOCATION_BROADCAST = PACKAGE_NAME + ".broadcast";

//    static final String CELL_BROADCAST = PACKAGE_NAME +".cell"+ ".broadcast";

    static final String EXTRA_LOCATION = PACKAGE_NAME + ".location";

    static final String EXTRA_CELL = PACKAGE_NAME + ".cell";
    private static final String EXTRA_REMOVE_BACKGROUND_UPDATES = PACKAGE_NAME +
            ".remove_background_updates";
    private static final String EXTRA_START_BACKGROUND_UPDATES = PACKAGE_NAME +
            ".start_background_updates";

    private final IBinder mBinder = new LocalBinder();

    /**
     * The desired interval for location updates. Inexact. Updates may be more or less frequent.
     */
    private static final long UPDATE_INTERVAL_IN_MILLISECONDS = 1000;

    /**
     * The fastest rate for active location updates. Updates will never be more frequent
     * than this value.
     */
    private static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS =
            UPDATE_INTERVAL_IN_MILLISECONDS / 2;

    /**
     * The identifier for the notification displayed for the foreground service.
     */
    private static final int NOTIFICATION_ID = 12345678;

    /**
     * Used to check whether the bound activity has really gone away and not unbound as part of an
     * orientation change. We create a foreground service notification only if the former takes
     * place.
     */
    private boolean mChangingConfiguration = false;

    private NotificationManager mNotificationManager;

    /**
     * Contains parameters used by {@link com.google.android.gms.location.FusedLocationProviderApi}.
     */
    private LocationRequest mLocationRequest;

    /**
     * Provides access to the Fused Location Provider API.
     */
    private FusedLocationProviderClient mFusedLocationClient;

    /**
     * Callback for changes in location.
     */
    private LocationCallback mLocationCallback;

    private Handler mServiceHandler;

    Double latitude, longitude;

    /**
     * The current location.
     */
    public static Location mLocation;
    private String data_cell_1="cell1";
    private S2LatLng latl_1;
    public static S2CellId cell2_1;
    private PendingIntent mapsPendingIntent;


    /**
     * Realtime location save in firestore or firebase*/
//    GeoFire geoFire;
//    FirebaseFirestore firebaseFirestore;
//    DocumentReference documentReference;
//    FirebaseAuth firebaseAuth;

    @SuppressWarnings("deprecation")
    public LocationUpdatesService() {
    }

    @SuppressWarnings("deprecation")
    @Override
    public void onCreate() {
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);
                onNewLocation(locationResult.getLastLocation());
            }
        };

        createLocationRequest();
        getLastLocation();

        HandlerThread handlerThread = new HandlerThread(TAG);
        handlerThread.start();
        mServiceHandler = new Handler(handlerThread.getLooper());
        mNotificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        // Android O requires a Notification Channel.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = getString(R.string.app_name);
            // Create the channel for the notification
            NotificationChannel mChannel =
                    new NotificationChannel(CHANNEL_ID, name, NotificationManager.IMPORTANCE_LOW);

            // Set the Notification Channel for the Notification Manager.
            mNotificationManager.createNotificationChannel(mChannel);
        }

        mapsPendingIntent = PendingIntent.getActivity(this, 0,
                new Intent(this, Maps.class), FLAG_UPDATE_CURRENT);
    }



    @SuppressWarnings("deprecation")
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i(TAG, "Service started");
        boolean removebackgroundupdate = intent.getBooleanExtra(EXTRA_REMOVE_BACKGROUND_UPDATES,
                false);
        boolean startbackgroundupdate = intent.getBooleanExtra(EXTRA_START_BACKGROUND_UPDATES,
                false);

        // We got here because the user decided to remove location updates from the notification.
        if (removebackgroundupdate) {
            removeLocationUpdates();
//            stopSelf();
        }
        if (startbackgroundupdate) {
            try {
                Log.d(TAG, "Trying to start Location");
                mFusedLocationClient.requestLocationUpdates(mLocationRequest,
                        mLocationCallback, Looper.myLooper());
            } catch (SecurityException unlikely) {
                Utils.setRequestingLocationUpdates(this, false);
                Log.d(TAG, "Lost location permission. Could not request updates. " + unlikely);
            }
            startForeground(NOTIFICATION_ID, getNotification());

        }
        // Tells the system to not try to recreate the service after it has been killed.
        return START_NOT_STICKY;
    }


    @SuppressWarnings("deprecation")
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mChangingConfiguration = true;
    }


    @SuppressWarnings("deprecation")
    @Override
    public IBinder onBind(Intent intent) {
        // Called when a client (MainActivity in case of this sample) comes to the foreground
        // and binds with this service. The service should cease to be a foreground service
        // when that happens.
        Log.i(TAG, "in onBind()");
        stopForeground(true);

        mChangingConfiguration = false;

        return mBinder;
    }


    @SuppressWarnings("deprecation")
    @Override
    public void onRebind(Intent intent) {
        // Called when a client (MainActivity in case of this sample) returns to the foreground
        // and binds once again with this service. The service should cease to be a foreground
        // service when that happens.
        Log.i(TAG, "in onRebind()");
        stopForeground(true);
//        Intent cell_intent = new Intent(CELL_BROADCAST);
//        intent.putExtra(EXTRA_CELL, true);
//        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(cell_intent);

        mChangingConfiguration = false;

        // Register Firestore when service will restart
//        firebaseAuth = FirebaseAuth.getInstance();
//        firebaseFirestore = FirebaseFirestore.getInstance();
        super.onRebind(intent);
    }


    @SuppressWarnings("deprecation")
    @Override
    public boolean onUnbind(Intent intent) {
        Log.i(TAG, "Last client unbound from service");

        // Called when the last client (MainActivity in case of this sample) unbinds from this
        // service. If this method is called due to a configuration change in MainActivity, we
        // do nothing. Otherwise, we make this service a foreground service.
        if (!mChangingConfiguration && Utils.requestingLocationUpdates(this)) {
            Log.d(TAG, "Starting foreground service");
            /*
            // TODO(developer). If targeting O, use the following code.
            if (Build.VERSION.SDK_INT == Build.VERSION_CODES.O) {
                mNotificationManager.startServiceInForeground(new Intent(this,
                        LocationUpdatesService.class), NOTIFICATION_ID, getNotification());
            } else {
                startForeground(NOTIFICATION_ID, getNotification());
            }
             */

            startForeground(NOTIFICATION_ID, getNotification());


        }
        return true; // Ensures onRebind() is called when a client re-binds.
    }

    @SuppressWarnings("deprecation")
    @Override
    public void onDestroy() {
        isRunning = false;
        mServiceHandler.removeCallbacksAndMessages(null);
    }

    /**
     * Makes a request for location updates. Note that in this sample we merely log the
     * {@link SecurityException}.
     */
    public void requestLocationUpdates() {
        Log.i(TAG, "Requesting location updates");
        Utils.setRequestingLocationUpdates(this, true);
        startService(new Intent(getApplicationContext(), LocationUpdatesService.class));
        try {
            mFusedLocationClient.requestLocationUpdates(mLocationRequest,
                    mLocationCallback, Looper.myLooper());
        } catch (SecurityException unlikely) {
            Utils.setRequestingLocationUpdates(this, false);
            Log.d(TAG, "Lost location permission. Could not request updates. " + unlikely);
        }
    }

    /**
     * Removes location updates. Note that in this sample we merely log the
     * {@link SecurityException}.
     */
    public void removeLocationUpdates() {
        isRunning = false;
        Log.i(TAG, "Removing location updates");
        try {
            mFusedLocationClient.removeLocationUpdates(mLocationCallback);
            Utils.setRequestingLocationUpdates(this, false);
            stopSelf();
        } catch (SecurityException unlikely) {
            Utils.setRequestingLocationUpdates(this, true);
            Log.d(TAG, "Lost location permission. Could not remove updates. " + unlikely);
        }
    }

    /*
     * Returns the {@link NotificationCompat} used as part of the foreground service.
     */
    private Notification getNotification() {
        Intent intent = new Intent(this, LocationUpdatesService.class);

//        CharSequence text = Utils.getLocationText(mLocation);
        CharSequence text = "Looking for memories nearby";

        // Extra to help us figure out if we arrived in onStartCommand via the notification or not.
        intent.putExtra(EXTRA_REMOVE_BACKGROUND_UPDATES, true);

        // The PendingIntent that leads to a call to onStartCommand() in this service.
        PendingIntent servicePendingIntent = PendingIntent.getService(this, 0, intent,
                FLAG_UPDATE_CURRENT);

        // The PendingIntent to launch activity.
        PendingIntent activityPendingIntent = PendingIntent.getActivity(this, 0,
                new Intent(this, Maps.class), 0);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this)
//                .addAction(R.drawable.ic_launch, getString(R.string.launch_activity),
//                        activityPendingIntent)
//                .addAction(R.drawable.ic_cancel, getString(R.string.remove_location_updates),
//                        servicePendingIntent)
                .setContentText(text)
                .setContentIntent(mapsPendingIntent)
//                .setContentTitle(Utils.getLocationTitle(this))
                .setOngoing(true)
                .setPriority(Notification.PRIORITY_MIN)
                .setSmallIcon(R.drawable.ic_stat_name)
                .setTicker(text)
                .setWhen(System.currentTimeMillis());

        // Set the Channel ID for Android O.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            builder.setChannelId(CHANNEL_ID); // Channel ID
        }


        return builder.build();
    }

    private void getLastLocation() {
        try {
            mFusedLocationClient.getLastLocation()
                    .addOnCompleteListener(new OnCompleteListener<Location>() {
                        @Override
                        public void onComplete(@NonNull Task<Location> task) {
                            isRunning=true;
//                            if (task.isSuccessful() && task.getResult() != null) {
//                                mLocation = task.getResult();
//                                latl_1 = S2LatLng.fromDegrees(mLocation.getLatitude(), mLocation.getLongitude());
//                                cell2_1 = S2CellId.fromLatLng(latl_1).parent(18);
//                                cell_list.clear();
//                                cell_list.add(cell2_1);
//                                cell2_1.getAllNeighbors(18,cell_list);
//                                pushWarheadNotifications();
//                                Intent cell_intent = new Intent(CELL_BROADCAST);
//                                cell_intent.putExtra(EXTRA_CELL, true);
//                                LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(cell_intent);
//                                data_cell_1 =String.valueOf(cell2_1.id());
//                            } else {
//                                Log.w(TAG, "Failed to get location.");
//                            }
                        }
                    });
        } catch (SecurityException unlikely) {
            Log.d(TAG, "Lost location permission." + unlikely);
        }
    }

    private void onNewLocation(Location location) {
        isRunning = true;
        Log.d(TAG, "New location: " + location);

        mLocation = location;
        current_location_from_Service = location;

        // Notify anyone listening for broadcasts about the new location.
        Intent intent = new Intent(LOCATION_BROADCAST);
        intent.putExtra(EXTRA_LOCATION, mLocation);
        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);

        // Update notification content if running as a foreground service.
        if (serviceIsRunningInForeground(this)) {
            mNotificationManager.notify(NOTIFICATION_ID, getNotification());
        }

        latl_1 = S2LatLng.fromDegrees(mLocation.getLatitude(), mLocation.getLongitude());
        cell2_1 = S2CellId.fromLatLng(latl_1).parent(18);
//        Log.i("CELLCHANGE",cell2_1.id()+"     "+data_cell_1);
        if(!data_cell_1.matches(String.valueOf(cell2_1.id()))) {
//            Log.i("CELLCHANGE","CEKKCHANGED");
            cell_list.clear();
            cell_list.add(cell2_1);
            cell2_1.getAllNeighbors(18,cell_list);
            pushWarheadNotifications();
            data_cell_1 =String.valueOf(cell2_1.id());
        }
    }

    /**
     * Sets the location request parameters.
     */
    private void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    /**
     * Class used for the client Binder.  Since this service runs in the same process as its
     * clients, we don't need to deal with IPC.
     */
    public class LocalBinder extends Binder {
        public LocationUpdatesService getService() {
            return LocationUpdatesService.this;
        }
    }

    /**
     * Returns true if this is a foreground service.
     *
     * @param context The {@link Context}.
     */
    public boolean serviceIsRunningInForeground(Context context) {
        ActivityManager manager = (ActivityManager) context.getSystemService(
                Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(
                Integer.MAX_VALUE)) {
            if (getClass().getName().equals(service.service.getClassName())) {
                if (service.foreground) {
                    return true;
                }
            }
        }
        return false;
    }

    public void pushWarheadNotifications() {
        NotificationManagerCompat.from(this).cancel(121);
        NotificationManagerCompat.from(this).cancel(122);
        NotificationManagerCompat.from(this).cancel(123);

        // global markers
//        FirebaseDatabase.getInstance().getReference("TeenageWarheads").addValueEventListener(new ValueEventListener() {
//            @Override
//            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//                if(dataSnapshot.child(String.valueOf(cell2.id())).hasChildren()) {
//                    getWarheadNotification("\uD83C\uDF0F Globals around you!","Tap to view","Global",121,null,mapsPendingIntent);
//                }
//            }
//
//            @Override
//            public void onCancelled(@NonNull DatabaseError databaseError) {
//
//            }
//        });

        // forU notification
        FirebaseDatabase.getInstance().getReference("foryou").child(id).child("received").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                for (int i = 0; i < cell_list.size(); i++) {

                    if (dataSnapshot.child(String.valueOf(cell_list.get(i).id())).hasChildren()) {

                        for (DataSnapshot dsp : dataSnapshot.child(String.valueOf(cell_list.get(i).id())).getChildren()) {
                            if(dsp.child("imgUrl").exists()) {
                                final String senderId = String.valueOf(dsp.child("sender").getValue());
                                final String sendername = String.valueOf(dsp.child("sendername").getValue());
                                final String senderpic = String.valueOf(dsp.child("senderimage").getValue());
                                Warhead warhead = dsp.getValue(Warhead.class);
                                if (warhead != null && !(warhead.getBackimgurl().equals(" "))) {
                                    Intent intent = new Intent(LocationUpdatesService.this, dropViewActivity.class);
                                    intent.putExtra("Warhead", warhead);
                                    intent.putExtra("username", sendername);
                                    intent.putExtra("userprofile", senderpic);
//                            PendingIntent dropViewPendingIntent = PendingIntent.getActivity(LocationUpdatesService.this, 0,
//                                    i, FLAG_UPDATE_CURRENT);
                                    if (!(warhead.getIsViewed())) {
                                        getWarheadNotification(sendername, "ForYou here. Tap to view", "Memory"
                                                , 122, senderpic, mapsPendingIntent);
//                                    getWarheadNotification("ForYou", "You have a memory here by "+sendername, "Memory"
//                                            ,warhead.getSender().hashCode(), senderpic, mapsPendingIntent);
                                    }

                                }
                            }
                        }
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        //self notification
        FirebaseDatabase.getInstance().getReference("self").child(id).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (int i = 0; i < cell_list.size(); i++) {
                if(dataSnapshot.child(String.valueOf(cell_list.get(i).id())).hasChildren()) {
                    for (DataSnapshot dsp : dataSnapshot.child(String.valueOf(cell_list.get(i).id())).getChildren()) {
                        if(dsp.child("imgUrl").exists()) {
                            Warhead warhead = dsp.getValue(Warhead.class);
                            if (warhead != null && !(warhead.getBackimgurl().equals(" "))) {
                                if (!(warhead.getIsViewed())) {
                                    getWarheadNotification("You have memories nearby! \uD83D\uDCAB ", "Tap to relive", "Memory"
                                            , 123, "self", mapsPendingIntent);
                                }

                            }
                        }
                    }
                }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

    }

    public void getWarheadNotification(String Title, String Text, String channelID, int id , String pic, PendingIntent intent) {

        Uri uri= Uri.parse("android.resource://"+getPackageName()+"/raw/send");
        AudioAttributes att = new AudioAttributes.Builder()
                .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                .setContentType(AudioAttributes.CONTENT_TYPE_SPEECH)
                .build();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(channelID, channelID, NotificationManager.IMPORTANCE_HIGH);
            channel.enableVibration(true);
            channel.enableLights(true);
            channel.setSound(uri,att);
            channel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
            NotificationManager manager = getSystemService(NotificationManager.class);
            manager.createNotificationChannel(channel);
        }

        NotificationCompat.Builder builder = new NotificationCompat.Builder(getApplicationContext(), channelID)
                .setContentTitle(Title)
                .setContentText(Text)
                .setOnlyAlertOnce(true)
                .setSound(uri)
                //.setLargeIcon(StringToBitMap(pic))
                .setPriority(Notification.PRIORITY_MAX)
                .setSmallIcon(R.drawable.ic_stat_name);

        if (intent!=null){
            builder.setContentIntent(intent);
        }

        if(!pic.equals("default") && !pic.equals("")) {
            if(pic.equals("self")) {
                //no image if selfnotif
            }
            else {
            Picasso.with(this)
                    .load(pic)
                    .placeholder(R.drawable.ic_dp_icon_male_04)
                    .into(new Target() {
                        @Override
                        public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {

                            bitmap = getCroppedBitmap(bitmap);
                            //Toast.makeText(getApplicationContext(), "nnnnnnnnnnnnnnnnnnn", Toast.LENGTH_SHORT).show();
                            builder.setLargeIcon(bitmap);
                            Log.i("NOTIFY BITMAP", "BITMAP MADE");
                        }

                        @Override
                        public void onBitmapFailed(Drawable errorDrawable) {
                            Log.e("NOTIFY BITMAP", "BITMAP FAILED");
                            // Toast.makeText(getApplicationContext(), "yyyyyyyyyyyyyyyyyyyyyyy", Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        public void onPrepareLoad(Drawable placeHolderDrawable) {

                        }
                    });
        }
        }
        else {
            builder.setLargeIcon(BitmapFactory.decodeResource(this.getResources(),R.drawable.ic_dp_icon_male_04));
        }

        NotificationManagerCompat manager = NotificationManagerCompat.from(getApplicationContext());
        manager.notify(id, builder.build());

    }

    public Bitmap StringToBitMap(String encodedString){

        if(encodedString.equals("default") || encodedString.equals(""))
            return BitmapFactory.decodeResource(this.getResources(),R.drawable.ic_dp_icon_male_04);

        else {
            try {
                URL url = new URL(encodedString);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setDoInput(true);
                connection.connect();
                InputStream input = connection.getInputStream();
                Bitmap myBitmap = BitmapFactory.decodeStream(input);
                myBitmap = getCroppedBitmap(myBitmap);
                return myBitmap;
            } catch (Exception e) {
                e.getMessage();
                return null;
            }
        }
    }

    public Bitmap getCroppedBitmap(Bitmap bitmap) {
        Bitmap output = Bitmap.createBitmap(bitmap.getWidth(),
                bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);

        final int color = 0xff424242;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        // canvas.drawRoundRect(rectF, roundPx, roundPx, paint);
        canvas.drawCircle(bitmap.getWidth() / 2, bitmap.getHeight() / 2,
                bitmap.getWidth() / 2, paint);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);
        //Bitmap _bmp = Bitmap.createScaledBitmap(output, 60, 60, false);
        //return _bmp;
        return output;
    }

    private String getLocality(Double lat, Double lon) {
        Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
        String locality=null;
        try {
            List<Address> addresses = geocoder.getFromLocation(lat, lon, 1);
            if(addresses.size() > 0) {
                Address obj = addresses.get(0);
                locality = obj.getSubLocality();
            }
            //    Log.i("Address",add);
            //  Toast.makeText(this, add, Toast.LENGTH_SHORT).show();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return locality;
    }
}
