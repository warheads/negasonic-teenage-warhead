package social.locus.Settings;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import social.locus.Adapters.ContactsAdapter;
import social.locus.Adapters.RequestsAdapter;
import social.locus.Beans.QuickdropModel;
import social.locus.Beans.User;
import social.locus.Chat.ChatActivity;
import social.locus.Maps;
import social.locus.R;

public class RequestsActivity extends AppCompatActivity {

    RequestsAdapter requestsAdapter;
    LinearLayoutManager linearLayoutManager;
    RecyclerView requests_recycelerview;
    ArrayList<String> unknown_user_ids = new ArrayList<>();
    String id = FirebaseAuth.getInstance().getCurrentUser().getUid();
    ArrayList<User> unknown_user_list = new ArrayList<>();
    TextView no_request_text;
    String open_from;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_requests);

        overridePendingTransition(0,0);

        open_from = getIntent().getStringExtra("from");

        Toolbar aboutUs_toolbar = findViewById(R.id.requests_toolbar);
        setSupportActionBar(aboutUs_toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back_arrow);
        aboutUs_toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(open_from.equals("Maps")) {
                    finish();
                }
                else {
                    finish();
                    startActivity(new Intent(RequestsActivity.this, Maps.class));
                }
            }
        });

        requests_recycelerview = findViewById(R.id.requests_recycler_view);
        no_request_text = findViewById(R.id.no_request_text);

        linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL,false);
        requestsAdapter = new RequestsAdapter(RequestsActivity.this, unknown_user_list);
        requests_recycelerview.setLayoutManager(linearLayoutManager);
        requests_recycelerview.setAdapter(requestsAdapter);
        requestsAdapter.notifyDataSetChanged();
        set_unknown_user_ids();

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if(open_from.equals("Maps")) {
            finish();
        }
        else {
            finish();
            startActivity(new Intent(RequestsActivity.this, Maps.class));
        }


    }

    void set_unknown_user_ids() {
        DatabaseReference seen_reference = FirebaseDatabase.getInstance().getReference("Chat").child(id);
        seen_reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                unknown_user_ids.clear();
                unknown_user_list.clear();
                unknown_user_ids.addAll(Maps.unknown_memory_ids);
                if(snapshot.hasChildren()) {
                    requests_recycelerview.setVisibility(View.VISIBLE);
                    no_request_text.setVisibility(View.GONE);
                    for (DataSnapshot dsp : snapshot.getChildren()) {
                        boolean seen = dsp.child("seen").getValue(Boolean.class);
                            if(!Maps.Contact_users.contains(dsp.getKey()) && !seen) {
                                unknown_user_ids.add(dsp.getKey());
                            }
                    }
                }
                //set_unknown_user_to_storage(unknown_user_ids);
                set_unknown_user_list();

            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    private void set_unknown_user_list() {

        DatabaseReference user_reference = FirebaseDatabase.getInstance().getReference("User");
        user_reference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                unknown_user_list.clear();
                for(DataSnapshot dsp : snapshot.getChildren()) {

                    if(unknown_user_ids.contains(dsp.child("id").getValue(String.class))) {
                        String uid,name,username,bio,dob,mobile,profile_url,thumb_url,email,Location,online, acc_creation_time;
                        uid = String.valueOf(dsp.child("id").getValue());
                        name = String.valueOf(dsp.child("name").getValue());
                        username = String.valueOf(dsp.child("username").getValue());
                        bio = String.valueOf(dsp.child("bio").getValue());
                        dob = String.valueOf(dsp.child("birthday").getValue());
                        mobile = String.valueOf(dsp.child("contact").getValue());
                        profile_url = String.valueOf(dsp.child("profilePicURL").getValue());
                        thumb_url = String.valueOf(dsp.child("thumb_image").getValue());
                        Location = String.valueOf(dsp.child("Location").getValue());
                        online = String.valueOf(dsp.child("online").getValue());
                        acc_creation_time = String.valueOf(dsp.child("account_creation_time").getValue());
                        User user = new User(Location, bio, dob, mobile, uid, name, online, profile_url, thumb_url, username,acc_creation_time);
                        unknown_user_list.add(user);
                        //Toast.makeText(RequestsActivity.this, user.getUsername(), Toast.LENGTH_SHORT).show();
                    }

                }

                requestsAdapter = new RequestsAdapter(RequestsActivity.this, unknown_user_list);
                requests_recycelerview.setLayoutManager(linearLayoutManager);
                requests_recycelerview.setAdapter(requestsAdapter);
                requestsAdapter.notifyDataSetChanged();

            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

    }

    @Override
    protected void onStart() {
        super.onStart();
        unknown_user_ids.clear();
        unknown_user_list.clear();
        linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL,false);
        requestsAdapter = new RequestsAdapter(RequestsActivity.this, unknown_user_list);
        requests_recycelerview.setLayoutManager(linearLayoutManager);
        requests_recycelerview.setAdapter(requestsAdapter);
        requestsAdapter.notifyDataSetChanged();
        set_unknown_user_ids();
        //Toast.makeText(RequestsActivity.this, "start", Toast.LENGTH_SHORT).show();
    }
}