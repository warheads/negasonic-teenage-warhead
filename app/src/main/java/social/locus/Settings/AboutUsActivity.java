package social.locus.Settings;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import social.locus.R;
import social.locus.Start.PhoneVerificationActivity;

public class AboutUsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_us);

        overridePendingTransition(0,0);

//        Toolbar aboutUs_toolbar = findViewById(R.id.aboutUs_toolbar);
//        setSupportActionBar(aboutUs_toolbar);
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back_arrow);
//        aboutUs_toolbar.setNavigationOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                finish();
//            }
//        });
//
//        TextView privacy_text = findViewById(R.id.privacy_text);
//        if (privacy_text != null) {
//            privacy_text.setMovementMethod(LinkMovementMethod.getInstance());
//        }

        TextView about_us_text = findViewById(R.id.about_us_text);
        about_us_text.setText(

                       "Capture the moment \uD83D\uDCF7. You will get a polaroid styled photograph.\n\n" +
                       "Write your message. Draw your heart out ✏️[Both sides editable]. Now, this is your memory \uD83D\uDDBC️.\n\n" +
                       "Select the privacy level for whom you want to drop your memory. Global \uD83C\uDF0E / Friends \uD83D\uDC65 /  Self \uD83D\uDC64. Nice. Done.\uD83D\uDC4C\n\n" +
                       "Now, next time whomsoever you have dropped the memory for, is nearby it. Voila! They will get notified! ✨\n" +
                       "\n\n" +
                       "ForYou \uD83D\uDC65 - Drop memories for your friends and they will get notified when they are nearby that memory.\n" +
                       "\n\n" +
                       "Globals \uD83C\uDF0E - Drop memory globally and everyone nearby will be able to view that.\n" +
                       "\n\n" +
                       "Self\uD83D\uDC64 - Drop memories for yourself so you can cherish them when you visit that place again."

        );

    }
}