package social.locus.Settings;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.TargetApi;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.Settings;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Collections;

import de.hdodenhof.circleimageview.CircleImageView;
import io.michaelrocks.libphonenumber.android.NumberParseException;
import io.michaelrocks.libphonenumber.android.PhoneNumberUtil;
import io.michaelrocks.libphonenumber.android.Phonenumber;
import social.locus.Adapters.ContactsAdapter;
import social.locus.Adapters.ContactsListadapter;
import social.locus.Adapters.SavedContactsModel;
import social.locus.Beans.QuickdropModel;
import social.locus.Beans.User;
import social.locus.Beans.Warhead;
import social.locus.BuildConfig;
import social.locus.Camera.CameraCaptureActivity;
import social.locus.Chat.ChatActivity;
import social.locus.Drops.ContactsActivity;
import social.locus.Maps;
import social.locus.R;

public class ConnectionsActivity extends AppCompatActivity {

    private static final String TAG = "ConnectionsActivity";
    String contact,share_url = "",current_contact="";
    String id = FirebaseAuth.getInstance().getCurrentUser().getUid();
    User current_user;
    FirebaseAuth auth;

    CardView bottomSheet;
    BottomSheetBehavior bottomSheetBehavior;
    TextView b_name, b_username, b_bio, b_location, b_self_count, b_global_count;
    ImageView quickreply_icon, quick_drop_icon;
    CircleImageView b_picture;

    ContactsAdapter contactsListadapter;
    RecyclerView Contact_recyclerView;

    public static final int PERMISSIONS_REQUEST_READ_CONTACTS = 1;

    LinearLayoutManager linearLayoutManager;
    ArrayList<String> Contact_list = new ArrayList<>();
    ArrayList<String> Name_list = new ArrayList<>();
    ArrayList<SavedContactsModel> SavedContacts = new ArrayList<>();
    ArrayList<User> Contact_users = new ArrayList<>();
    DatabaseReference profile_database;
    private ArrayList<String> memory_list = new ArrayList<>();

    private View.OnClickListener onItemClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            RecyclerView.ViewHolder viewHolder = (RecyclerView.ViewHolder) view.getTag();
            int position = viewHolder.getAdapterPosition();

            String bottomsheet_id = Contact_users.get(position).getId();

            if (!(bottomsheet_id.equals(""))) {
                //Toast.makeText(ConnectionsActivity.this, bottomsheet_id, Toast.LENGTH_SHORT).show();
                profile_database.child(bottomsheet_id).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                        String pname = dataSnapshot.child("name").getValue(String.class);
                        String username = dataSnapshot.child("username").getValue(String.class);
                        String pbio = dataSnapshot.child("bio").getValue(String.class);
                        String pimage = dataSnapshot.child("thumb_image").getValue(String.class);
                        String plocation = dataSnapshot.child("Location").getValue(String.class);
                        String full_image = dataSnapshot.child("profilePicURL").getValue(String.class);

                        b_name.setText(pname);
                        b_username.setText(username);
                        b_bio.setText(pbio);
                        b_location.setText(plocation);
                        get_memory_by_you(bottomsheet_id);

                        b_self_count.setText(get_memory_for_you(bottomsheet_id)+"");
                        Picasso.with(ConnectionsActivity.this).load(pimage).placeholder(R.drawable.ic_dp_icon_male_04).into(b_picture);

                        b_picture.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                showImage(pimage, full_image);
                            }
                        });

                        //bottonsheet quick reply button
                        quickreply_icon.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {

                                Intent intent = new Intent(ConnectionsActivity.this, ChatActivity.class);
                                intent.putExtra("user_id", bottomsheet_id);
                                intent.putExtra("username", username);
                                intent.putExtra("userprofile", full_image);
                                intent.putExtra("userprofile_thumb", pimage);
                                intent.putExtra("open_from", "connections");
                                startActivity(intent);
                                overridePendingTransition(0, 0);

                            }
                        });

                        quick_drop_icon.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                                Intent intent = new Intent(ConnectionsActivity.this, CameraCaptureActivity.class);
                                intent.putExtra("quickID", bottomsheet_id);
                                startActivity(intent);
                                overridePendingTransition(0, 0);
                            }
                        });


                    }

                    @Override
                    public void onCancelled(DatabaseError error) {
                        // Failed to read value
                        Log.w(TAG, "Failed to read value.", error.toException());
                    }
                });
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
            }

            else {
                if(!(share_url.matches(""))) {
                    Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                    sharingIntent.setType("text/plain");
                    sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "LOCUS INVITATION");
                    sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, share_url);
                    startActivity(Intent.createChooser(sharingIntent, "Invite via"));
                }

            }

            //Toast.makeText(Maps.this, ""+bottomsheet_id, Toast.LENGTH_SHORT).show();
        }
    };

    private void get_memory_by_you(String bottomsheet_id) {

        DatabaseReference ref = FirebaseDatabase.getInstance().getReference("foryou_list").child(bottomsheet_id).child("received");
        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                int count = 0;
                for(DataSnapshot dsp : dataSnapshot.getChildren()) {
                    String user_id= dsp.child("sender").getValue(String.class);
                    if(id.equals(user_id)) {
                        count++;
                    }
                }
                b_global_count.setText(count+"");
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

    }

    private void setShareUrl() {
        String url;
        FirebaseDatabase.getInstance().getReference().addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                share_url = String.valueOf(snapshot.child("share_url").getValue());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_connections);
        overridePendingTransition(0,0);
        Toolbar connections_toolbar = findViewById(R.id.connections_toolbar);

        profile_database = FirebaseDatabase.getInstance().getReference("User");
        b_name = findViewById(R.id.b_name);
        b_username = findViewById(R.id.b_username);
        b_bio = findViewById(R.id.b_bio);
        b_location = findViewById(R.id.b_location);
        b_picture = findViewById(R.id.b_picture);
        b_global_count = findViewById(R.id.b_global_count);
        b_self_count = findViewById(R.id.b_self_count);
        quickreply_icon = findViewById(R.id.quickreply_icon);
        quick_drop_icon = findViewById(R.id.quickdrop_icon);
        bottomSheet = findViewById(R.id.bottom_sheet_card);
        bottomSheetBehavior = BottomSheetBehavior.from(bottomSheet);

        getCurrentUserDetails();
        set_my_memory_list();
        setSupportActionBar(connections_toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back_arrow);
        connections_toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        auth = FirebaseAuth.getInstance();
        requestContactPermission();
        setShareUrl();
        Contact_recyclerView = findViewById(R.id.contact_recyclerview);
        contactsListadapter = new ContactsAdapter(ConnectionsActivity.this,Contact_users);
        contactsListadapter.setOnItemClickListener(onItemClickListener);
        Contact_recyclerView.setLayoutManager(linearLayoutManager);
        Contact_recyclerView.setAdapter(contactsListadapter);
        contactsListadapter.notifyDataSetChanged();

    }

    @Override public boolean dispatchTouchEvent(MotionEvent event){
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            if (bottomSheetBehavior.getState()==BottomSheetBehavior.STATE_EXPANDED) {

                Rect outRect = new Rect();
                bottomSheet.getGlobalVisibleRect(outRect);

                if(!outRect.contains((int)event.getRawX(), (int)event.getRawY()))
                    bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            }
        }

        return super.dispatchTouchEvent(event);
    }

    private String getCountryCode(String number) {


        PhoneNumberUtil phoneUtil = PhoneNumberUtil.createInstance(this);;
        try {
            Phonenumber.PhoneNumber NumberProto = phoneUtil.parse(number, "IN");
            if (!phoneUtil.isValidNumber(NumberProto)) {
                // Log.i("Contact","Not Valid");
                //showError(getResources().getString(R.string.wrong_phone_number));
                return number;
            }
            String regionISO = String.valueOf(NumberProto.getCountryCode());
            if(!(regionISO.matches(""))){
                //Log.i("Contact","Valid "+regionISO);
                number = "+"+regionISO+number;
            }


        } catch (NumberParseException e) {
            //Log.i("Contact","Exception: "+e.toString());
            //showError(e.toString());
            return number;
        }


        return number;
    }

    void getallContacts() {
        Cursor phones = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                null,null,null, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME+" ASC");
        while (phones.moveToNext())
        {
            String name=phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
            String phoneNumber = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
            phoneNumber = phoneNumber.replaceAll("[()\\s-]", "");
            if(phoneNumber.length() > 0) {
                if (phoneNumber.charAt(0) != '+') {
                    phoneNumber = getCountryCode(phoneNumber);
                }
                if (phoneNumber.charAt(0) == '0') {
                    phoneNumber = phoneNumber.substring(1);
                }
                if (!phoneNumber.equals(geContactFromStorage())) {
                    Contact_list.add(phoneNumber);
                    Name_list.add(name);
                    //Log.i("Contact", name + " " + phoneNumber);
                    SavedContacts.add(new SavedContactsModel(name, phoneNumber));
                }
            }
            //Toast.makeText(this, , Toast.LENGTH_SHORT).show();
        }
        phones.close();
    }

    private String geContactFromStorage() {
        SharedPreferences Preferences = getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        String contact = Preferences.getString("my_phone","");
        return contact;
    }

    void getregcontacts() {

        final DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference("User");
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Contact_users.clear();
                for(DataSnapshot dsp : dataSnapshot.getChildren()) {
                    contact = String.valueOf(dsp.child("contact").getValue());
//                    User user = dsp.getValue(User.class);

                    String uid,name,username,bio,dob,mobile,profile_url,thumb_url,email,Location,online, acc_creation_time;
                    uid = String.valueOf(dsp.child("id").getValue());
                    name = String.valueOf(dsp.child("name").getValue());
                    username = String.valueOf(dsp.child("username").getValue());
                    bio = String.valueOf(dsp.child("bio").getValue());
                    dob = String.valueOf(dsp.child("birthday").getValue());
                    mobile = String.valueOf(dsp.child("contact").getValue());
                    profile_url = String.valueOf(dsp.child("profilePicURL").getValue());
                    thumb_url = String.valueOf(dsp.child("thumb_image").getValue());
                    email = String.valueOf(dsp.child("email").getValue());
                    Location = String.valueOf(dsp.child("Location").getValue());
                    online = String.valueOf(dsp.child("online").getValue());
                    acc_creation_time = String.valueOf(dsp.child("account_creation_time").getValue());
                    User user = new User(Location, bio, dob, mobile, uid, name, online, profile_url, thumb_url, username,acc_creation_time);

                    if(Contact_list.contains(contact)) {
                        // reg_Contact_list.add(new SavedContactsModel(Name_list.get(Contact_list.indexOf(contact)),contact));
                        Contact_users.add(user);
                    }
                }
                contactsListadapter = new ContactsAdapter(ConnectionsActivity.this,Contact_users);
                contactsListadapter.setOnItemClickListener(onItemClickListener);
                Contact_recyclerView.setLayoutManager(linearLayoutManager);
                Contact_recyclerView.setAdapter(contactsListadapter);
                contactsListadapter.notifyDataSetChanged();
                //getUserfromIds(reg_Contact_id_list);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    public void requestContactPermission() {
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    android.Manifest.permission.READ_CONTACTS)) {
                ActivityCompat.requestPermissions(this,
                        new String[]{android.Manifest.permission.READ_CONTACTS},
                        PERMISSIONS_REQUEST_READ_CONTACTS);
            }
        } else {
            getallContacts();
            getregcontacts();
            linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL,false);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Log.i("spare_fuct_call", "onRequestPermissionResult");
        if (requestCode == PERMISSIONS_REQUEST_READ_CONTACTS) {
            if (grantResults.length <= 0) {
                // If user interaction was interrupted, the permission request is cancelled and you
                // receive empty arrays.
                Log.i(TAG, "User interaction was cancelled.");
            } else if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permission was granted.
                getallContacts();
                getregcontacts();
                linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL,false);

            } else {
                // Permission denied.
                Snackbar.make(
                        findViewById(R.id.connection_layout),
                        "Contacts permission needed.",
                        Snackbar.LENGTH_INDEFINITE)
                        .setAction(R.string.settings, new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                // Build intent that displays the App settings screen.
                                Intent intent = new Intent();
                                intent.setAction(
                                        Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                Uri uri = Uri.fromParts("package",
                                        BuildConfig.APPLICATION_ID, null);
                                intent.setData(uri);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                            }
                        })
                        .show();
            }
        }
    }

    private void getCurrentUserDetails() {
        FirebaseDatabase.getInstance().getReference("User").child(id).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                current_contact = String.valueOf(dataSnapshot.child("contact").getValue());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    private int get_memory_for_you(String id) {
        int count = 0;
        for(int i = 0; i < memory_list.size(); i++) {
            if(memory_list.get(i).matches(id)){
                count++;
            }
        }
        return count;
    }

    private void set_my_memory_list() {

        DatabaseReference quick_reference = FirebaseDatabase.getInstance().getReference("foryou_list").child(id).child("received");
        quick_reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                memory_list.clear();
                if(snapshot.hasChildren()) {
                    for (DataSnapshot dsp : snapshot.getChildren()) {
                        Warhead warhead = dsp.getValue(Warhead.class);
                        memory_list.add(warhead.getSender());
                    }
                }
                //contactsListadapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

    }

    public void showImage(String thumb_string, String img_url) {
        Dialog builder = new Dialog(this);
        builder.requestWindowFeature(Window.FEATURE_NO_TITLE);
        builder.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));
        builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                //nothing;
            }
        });

        ImageView imageView = new ImageView(this);

        //imageView.setImageResource(R.drawable.ic_dp_icon_male_04);
        //Picasso.with(ChatActivity.this).load(img_url).placeholder(thumb_string).into(imageView);
        Picasso.with(ConnectionsActivity.this)
                .load(thumb_string) // small quality url goes here
                .placeholder(R.drawable.ic_dp_icon_male_04)
                .into(imageView, new com.squareup.picasso.Callback() {
                    @Override
                    public void onSuccess() {
                        Picasso.with(ConnectionsActivity.this).load(img_url)
                                .placeholder(imageView.getDrawable()).into(imageView);
//                                PhotoViewAttacher photoAttacher;
//                                photoAttacher= new PhotoViewAttacher(imageView);
//                                photoAttacher.canZoom();
                    }

                    @Override
                    public void onError() {

                    }
                });
        builder.addContentView(imageView, new RelativeLayout.LayoutParams(
                (int) pxFromDp(300),
                (int) pxFromDp(300)));
        builder.show();
    }

    private float pxFromDp(float dp) {
        return dp * getResources().getDisplayMetrics().density;
    }

}