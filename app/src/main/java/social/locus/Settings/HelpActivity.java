package social.locus.Settings;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import social.locus.R;

public class HelpActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help);
        overridePendingTransition(0,0);


        Toolbar aboutUs_toolbar = findViewById(R.id.help_toolbar);
        setSupportActionBar(aboutUs_toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back_arrow);
        aboutUs_toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        EditText help_edit_text = findViewById(R.id.help_edit_text);

        String to = "queries.locus@gmail.com";
        String subject = "FEEDBACK MAIL";

        Button help_send_button = findViewById(R.id.help_send_button);
        help_send_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String message = help_edit_text.getText().toString();

                String mailto = "mailto:"+to +
                        "?cc=" +
                        "&subject=" + Uri.encode(subject) +
                        "&body=" + Uri.encode(message);
                Intent emailIntent = new Intent(Intent.ACTION_SENDTO);
                emailIntent.setData(Uri.parse(mailto));

                try {
                    startActivity(emailIntent);
                } catch (ActivityNotFoundException e) {
                    Toast.makeText(getApplicationContext(), "Error to open email app", Toast.LENGTH_SHORT).show();
                }

            }
        });

    }
}