package social.locus.Settings;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import social.locus.Adapters.NotificationsAdapter;
import social.locus.R;

public class SettingAdapterr extends RecyclerView.Adapter<SettingAdapterr.ViewHolder> {

    ArrayList<String> dataList;
    Context mContext;
    int[] imageList;


    public SettingAdapterr(ArrayList<String> dataList, int[] imageList, Context mContext){
       this.dataList=dataList;
       this.mContext = mContext;
       this.imageList = imageList;
   }


    @NonNull
    @Override
    public SettingAdapterr.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.setting_model,viewGroup,false);
        return new SettingAdapterr.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SettingAdapterr.ViewHolder holder, int i) {

        holder.setting_icon.setImageDrawable((mContext).getResources().getDrawable(imageList[i]));
        holder.setting_text.setText(dataList.get(i));

    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        ImageView setting_icon;
        TextView setting_text;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            setting_icon =itemView.findViewById(R.id.setting_icon);
            setting_text =itemView.findViewById(R.id.setting_text);
        }
    }
}

