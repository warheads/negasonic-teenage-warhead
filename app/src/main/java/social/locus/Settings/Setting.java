package social.locus.Settings;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import social.locus.R;

public class Setting extends AppCompatActivity {
    String id = FirebaseAuth.getInstance().getCurrentUser().getUid();
    private DatabaseReference mUsersDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        overridePendingTransition(0,0);

        Toolbar a_settings_toolbar = findViewById(R.id.a_settings_toolbar);
        setSupportActionBar(a_settings_toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back_arrow);
        a_settings_toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        RecyclerView profileRecycler = findViewById(R.id.SettingRecyclerView);
        profileRecycler.setLayoutManager(new LinearLayoutManager(this));
        ArrayList<String> itemsArray=new ArrayList<>();
        int[] imageList = new int[]{R.drawable.account_info_icon,
                R.drawable.blocked_users_icon,
                R.drawable.share_people_icon,
                R.drawable.update_icon,
                R.drawable.license_icon,
                R.drawable.library_icon
        };

        mUsersDatabase = FirebaseDatabase.getInstance().getReference("User");
        mUsersDatabase.keepSynced(true);

        itemsArray.add("Account Info");
        itemsArray.add("Blocked Users \uD83D\uDEAB");
        itemsArray.add("Invite People");
        itemsArray.add("App Updates");
        itemsArray.add("Licences");
        itemsArray.add("Open Source Libraries");
        profileRecycler.setAdapter(new SettingAdapterr(itemsArray, imageList, Setting.this));



            }
}
