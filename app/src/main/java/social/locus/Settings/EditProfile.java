package social.locus.Settings;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.Settings;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;
import id.zelory.compressor.Compressor;
import social.locus.BuildConfig;
import social.locus.Maps;
import social.locus.R;
import social.locus.Start.profilePicActivity;

public class EditProfile extends AppCompatActivity {

    CircleImageView edit_profilepic;
    EditText name_editText, username_editText, bio_editText, baselocation_editText;
    Button save_changes_button;
    DatabaseReference editProfileReference;
    String user_id = FirebaseAuth.getInstance().getUid();
    Uri selectedImage;
    private StorageReference mStorageReference;
    ProgressDialog progressBar;
    String locstring = "";
    Bitmap bitmap=null;
    String username=null;
    boolean username_flag = true;
    boolean pp_flag = false;

    LinearLayout fetch_locality_layout;
    TextView locality_textView;
    Button doneButton;

    private static final String TAG = Maps.class.getSimpleName();

    private static final int REQUEST_PERMISSIONS_REQUEST_CODE = 34;

    private FusedLocationProviderClient mFusedLocationClient;

    protected Location mLastLocation;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        overridePendingTransition(0,0);

        fetch_locality_layout = findViewById(R.id.fetch_locality_layout);
        locality_textView = findViewById(R.id.fetch_locality_textView);
        doneButton = findViewById(R.id.doneButton);

        Toolbar aboutUs_toolbar = findViewById(R.id.editProfile_toolbar);
        setSupportActionBar(aboutUs_toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back_arrow);
        aboutUs_toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        edit_profilepic = findViewById(R.id.edit_profile_imageview);
        name_editText = findViewById(R.id.editprofile_name_edittext);
        username_editText = findViewById(R.id.editprofile_username_edittext);
        bio_editText = findViewById(R.id.editprofile_bio_edittext);
//        baselocation_editText = findViewById(R.id.editprofile_baselocation_edittext);
        save_changes_button = findViewById(R.id.save_changes_button);

        mStorageReference = FirebaseStorage.getInstance().getReference();
        progressBar = new ProgressDialog(this);

        edit_profilepic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final CharSequence[] items = {"Select from device", "Remove photo"};

                if(pp_flag) {

                final AlertDialog.Builder builder = new AlertDialog.Builder(EditProfile.this);
                builder.setTitle("Update profile picture");
                builder.setItems(items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int item) {

                        if (item == 0) {

                            getphoto();

                        } else {
                            DatabaseReference reference = FirebaseDatabase.getInstance().getReference("User").child(user_id);
                            reference.child("profilePicURL").removeValue();
                            reference.child("thumb_image").removeValue().addOnSuccessListener(new OnSuccessListener<Void>() {
                                @Override
                                public void onSuccess(Void aVoid) {
                                    pp_flag = false;
                                    Toast.makeText(EditProfile.this, "Picture removed.", Toast.LENGTH_SHORT).show();
                                }
                            });
                        }
                    }
                });
                builder.show();
            }
                else getphoto();
            }
        });

        editProfileReference = FirebaseDatabase.getInstance().getReference("User").child(user_id);
        editProfileReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                    String name = String.valueOf(dataSnapshot.child("name").getValue());
                    username = String.valueOf(dataSnapshot.child("username").getValue());
                    String bio = String.valueOf(dataSnapshot.child("bio").getValue());
                    String location = String.valueOf(dataSnapshot.child("Location").getValue());
                    if(dataSnapshot.child("thumb_image").exists()) {
                        pp_flag = true;
                    }
                    String profile_pic_url = String.valueOf(dataSnapshot.child("thumb_image").getValue());

                    name_editText.setText(name);
                    username_editText.setText(username);
                    bio_editText.setText(bio);
                    locality_textView.setText(location);
                    Picasso.with(EditProfile.this).load(profile_pic_url).placeholder(R.drawable.ic_dp_icon_male_04).into(edit_profilepic);


                fetch_locality_layout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(EditProfile.this);

                        if (!checkPermissions()) {
                            requestPermissions();
                        } else {
                            getLastLocation();
                        }
                    }
                });

                username_editText.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    }

                    @Override
                    public void afterTextChanged(Editable editable) {
                        Query query = FirebaseDatabase.getInstance().getReference("User").orderByChild("username").equalTo(editable.toString());
                        query.addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot snapshot) {

                                if (!(editable.toString().matches(username))) {

                                    if (snapshot.exists()) {
                                        username_editText.setTextColor(getResources().getColor(R.color.text_gray));
                                        username_editText.setTypeface(null, Typeface.ITALIC);
                                        username_flag = false;
                                        //username_avail_info.setVisibility(View.VISIBLE);
                                    } else {
                                        username_flag = true;
                                        username_editText.setTextColor(getResources().getColor(R.color.black));
                                        username_editText.setTypeface(null, Typeface.NORMAL);

                                        // next.setVisibility(View.VISIBLE);
                                        // username_avail_info.setVisibility(View.GONE);
                                    }
                                }

                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError error) {

                            }
                        });
                    }
                });


                save_changes_button.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            if( name_editText.getText().toString().matches("") ||
                                    username_editText.getText().toString().matches("") ||
                                    bio_editText.getText().toString().matches("") ||
                                    locality_textView.getText().toString().matches("")) {

                                Toast.makeText(EditProfile.this, "All fields must be filled!", Toast.LENGTH_SHORT).show();

                            }
                            else if(!username_flag) {

                                Toast.makeText(EditProfile.this, "Username not available!", Toast.LENGTH_SHORT).show();

                            }

                            else {

                                editProfileReference.child("name").setValue(name_editText.getText().toString());
                                editProfileReference.child("username").setValue(username_editText.getText().toString());
                                editProfileReference.child("bio").setValue(bio_editText.getText().toString());
                                editProfileReference.child("Location").setValue(locality_textView.getText().toString())
                                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                                            @Override
                                            public void onSuccess(Void unused) {
                                                finish();
                                                startActivity(new Intent(EditProfile.this, Maps.class));
                                            }
                                        });
                            }
                        }
                    });
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

    }

    public void getphoto() {
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, 1);
    }

    private String getLocality(Double lat, Double lon) {
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        String locality=null;
        try {
            List<Address> addresses = geocoder.getFromLocation(lat, lon, 1);
            if(addresses.size() > 0) {
                Address obj = addresses.get(0);
                locality = obj.getLocality();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return  locality;
    }

    @SuppressWarnings("MissingPermission")
    private void getLastLocation() {
        mFusedLocationClient.getLastLocation()
                .addOnCompleteListener(this, new OnCompleteListener<Location>() {
                    @Override
                    public void onComplete(@NonNull Task<Location> task) {
                        if (task.isSuccessful() && task.getResult() != null) {
                            mLastLocation = task.getResult();
                            locstring=getLocality(mLastLocation.getLatitude(),mLastLocation.getLongitude());
                            locality_textView.setText(locstring);
                            Toast.makeText(EditProfile.this, "Tap Save changes to update.", Toast.LENGTH_SHORT).show();

                        } else {
                            Log.w(TAG, "getLastLocation:exception", task.getException());
                            showSnackbar("Unable to fetch location.");
                        }
                    }
                });
    }

    private void showSnackbar(final String text) {
        View container = findViewById(R.id.profile_layout);
        if (container != null) {
            Snackbar.make(container, text, Snackbar.LENGTH_LONG).show();
        }
    }

    private void showSnackbar(final int mainTextStringId, final int actionStringId,
                              View.OnClickListener listener) {
        Snackbar.make(findViewById(android.R.id.content),
                getString(mainTextStringId),
                Snackbar.LENGTH_INDEFINITE)
                .setAction(getString(actionStringId), listener).show();
    }

    private boolean checkPermissions() {
        int permissionState = ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION);
        return permissionState == PackageManager.PERMISSION_GRANTED;
    }

    private void startLocationPermissionRequest() {
        ActivityCompat.requestPermissions(EditProfile.this,
                new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                REQUEST_PERMISSIONS_REQUEST_CODE);
    }

    private void requestPermissions() {
        boolean shouldProvideRationale =
                ActivityCompat.shouldShowRequestPermissionRationale(this,
                        Manifest.permission.ACCESS_FINE_LOCATION);

        // Provide an additional rationale to the user. This would happen if the user denied the
        // request previously, but didn't check the "Don't ask again" checkbox.
        if (shouldProvideRationale) {
            Log.i(TAG, "Displaying permission rationale to provide additional context.");

            showSnackbar(R.string.permission_rationale, android.R.string.ok,
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            // Request permission
                            startLocationPermissionRequest();
                        }
                    });

        } else {
            Log.i(TAG, "Requesting permission");
            // Request permission. It's possible this can be auto answered if device policy
            // sets the permission in a given state or the user denied the permission
            // previously and checked "Never ask again".
            startLocationPermissionRequest();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Log.i(TAG, "onRequestPermissionResult");
        if (requestCode == REQUEST_PERMISSIONS_REQUEST_CODE) {
            if (grantResults.length <= 0) {
                // If user interaction was interrupted, the permission request is cancelled and you
                // receive empty arrays.
                Log.i(TAG, "User interaction was cancelled.");
            } else if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permission granted.
                getLastLocation();
            } else {
                // Permission denied.

                // Notify the user via a SnackBar that they have rejected a core permission for the
                // app, which makes the Activity useless. In a real app, core permissions would
                // typically be best requested during a welcome-screen flow.

                // Additionally, it is important to remember that a permission might have been
                // rejected without asking the user for permission (device policy or "Never ask
                // again" prompts). Therefore, a user interface affordance is typically implemented
                // when permissions are denied. Otherwise, your app could appear unresponsive to
                // touches or interactions which have required permissions.
                showSnackbar(R.string.permission_denied_explanation, R.string.settings,
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                // Build intent that displays the App settings screen.
                                Intent intent = new Intent();
                                intent.setAction(
                                        Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                Uri uri = Uri.fromParts("package",
                                        BuildConfig.APPLICATION_ID, null);
                                intent.setData(uri);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                            }
                        });
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        if (requestCode == 1 && resultCode == RESULT_OK && data != null) {

            selectedImage = data.getData();
            CropImage.activity(selectedImage)
                    .setAspectRatio(1, 1)
                    .start(this);

        }

        Uri cropped_image= null;

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                cropped_image = result.getUri();
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }

            final File thumb_filePath = new File(cropped_image.getPath());

            Bitmap thumb_bitmap = new Compressor(this)
                    .setMaxWidth(200)
                    .setMaxHeight(200)
                    .setQuality(75)
                    .compressToBitmap(thumb_filePath);

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            thumb_bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            final byte[] thumb_byte = baos.toByteArray();



            try {
                bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(),cropped_image);
            } catch (IOException e) {
                e.printStackTrace();
            }

            progressBar.setTitle("Setting Profile Picture");
            progressBar.setMessage("Upload in progress");
            progressBar.setCanceledOnTouchOutside(false);
            progressBar.show();

            final StorageReference filepath = mStorageReference.child(user_id).child(cropped_image.getLastPathSegment());
            final StorageReference thumb_filepath = mStorageReference.child("profile_images").child("thumbs").child(user_id + ".jpg");

            final Bitmap finalBitmap = bitmap;

            filepath.putFile(cropped_image).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(final UploadTask.TaskSnapshot taskSnapshot) {

                    filepath.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                        @Override
                        public void onSuccess(final Uri uri) {
                            final String download_url = uri.toString();

                            thumb_filepath.putBytes(thumb_byte).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
                                @Override
                                public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {
                                    if(task.isSuccessful()) {
                                        thumb_filepath.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                            @Override
                                            public void onSuccess(Uri uri) {
                                                String thumb_downloadUrl = uri.toString();

                                                FirebaseDatabase.getInstance().getReference("User").child(user_id).child("profilePicURL").setValue(download_url);
                                                FirebaseDatabase.getInstance().getReference("User").child(user_id).child("thumb_image").setValue(thumb_downloadUrl);

                                                edit_profilepic.setImageBitmap(bitmap);

                                                Picasso.with(getApplication()).load(uri.toString()).placeholder(R.drawable.ic_dp_icon_male_04).into(edit_profilepic);
                                                progressBar.dismiss();
                                            }
                                        });
                                    }
                                }
                            });
                            Toast.makeText(EditProfile.this, "Upload Complete..", Toast.LENGTH_SHORT).show();

                        }
                    });

                }
            });



        }
    }
}
