package social.locus;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.OnLifecycleEvent;
import androidx.lifecycle.ProcessLifecycleOwner;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.hardware.GeomagneticField;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Handler;
import android.os.IBinder;
import android.provider.ContactsContract;
import android.provider.Settings;
import android.util.Base64;
import android.util.Log;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import static social.locus.LocationUpdatesService.cell_list;
import static social.locus.Settings.ConnectionsActivity.PERMISSIONS_REQUEST_READ_CONTACTS;

import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.snackbar.Snackbar;
import com.google.common.geometry.S2CellId;
import com.google.common.geometry.S2LatLng;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.gson.Gson;
import com.google.gson.JsonPrimitive;
import com.mapbox.maps.CameraBoundsOptions;
import com.mapbox.maps.CameraOptions;
import com.mapbox.maps.CoordinateBounds;
import com.mapbox.maps.EdgeInsets;
import com.mapbox.maps.ScreenCoordinate;
import com.mapbox.maps.Style;
import com.mapbox.maps.extension.observable.eventdata.MapLoadedEventData;
import com.mapbox.maps.plugin.animation.CameraAnimationsPlugin;
import com.mapbox.maps.plugin.animation.CameraAnimationsUtils;
import com.mapbox.maps.plugin.animation.Cancelable;
import com.mapbox.maps.plugin.animation.MapAnimationOptions;
import com.mapbox.maps.plugin.annotation.AnnotationPlugin;
import com.mapbox.maps.plugin.annotation.AnnotationPluginImplKt;
import com.mapbox.maps.plugin.annotation.generated.OnPointAnnotationClickListener;
import com.mapbox.maps.plugin.annotation.generated.PointAnnotation;
import com.mapbox.maps.plugin.annotation.generated.PointAnnotationManager;
import com.mapbox.maps.plugin.annotation.generated.PointAnnotationManagerKt;
import com.mapbox.maps.plugin.annotation.generated.PointAnnotationOptions;
import com.mapbox.maps.plugin.compass.CompassPlugin;
import com.mapbox.maps.plugin.compass.CompassViewPluginKt;
import com.mapbox.maps.plugin.delegates.MapPluginProviderDelegate;
import com.mapbox.maps.plugin.delegates.listeners.OnMapLoadedListener;
import com.mapbox.maps.plugin.gestures.GesturesPlugin;
import com.mapbox.maps.plugin.gestures.GesturesUtils;
import com.mapbox.maps.plugin.locationcomponent.LocationComponentPlugin;
import com.mapbox.maps.plugin.locationcomponent.LocationComponentUtils;
import com.mapbox.maps.plugin.locationcomponent.OnIndicatorPositionChangedListener;
import com.mapbox.maps.plugin.scalebar.ScaleBarPlugin;
import com.mapbox.maps.plugin.scalebar.ScaleBarUtils;
import com.squareup.picasso.Picasso;

import org.apache.commons.lang3.StringEscapeUtils;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.KeySpec;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import de.hdodenhof.circleimageview.CircleImageView;
import io.michaelrocks.libphonenumber.android.NumberParseException;
import io.michaelrocks.libphonenumber.android.PhoneNumberUtil;
import io.michaelrocks.libphonenumber.android.Phonenumber;
import social.locus.Adapters.ContactsAdapter;
import social.locus.Adapters.QuickdropAdapter;
import social.locus.Adapters.SavedContactsModel;
import social.locus.Beans.QuickdropModel;
import social.locus.Beans.User;
import social.locus.Beans.Warhead;
import social.locus.Camera.CameraCaptureActivity;
import social.locus.Chat.ChatActivity;
import social.locus.Drops.PhotoActivity;
import social.locus.Drops.dropViewActivity;
import social.locus.Notifications.Client;
import social.locus.Notifications.Data;
import social.locus.Notifications.NotificationsActivity;
import social.locus.Notifications.Token;
import social.locus.Settings.AboutUsActivity;
import social.locus.Settings.ConnectionsActivity;
import social.locus.Settings.EditProfile;
import social.locus.Settings.HelpActivity;
import social.locus.Settings.RequestsActivity;


public class Maps extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener,
        MapBoxMapView.UpdateMapAfterUserInterection, MapBoxMapView.PinchInteraction ,SensorEventListener{

    String id = FirebaseAuth.getInstance().getCurrentUser().getUid();
    //new location
    private static final String TAG = "resPMain";

    // Used in checking for runtime permissions.
    private static final int REQUEST_PERMISSIONS_REQUEST_CODE = 34;

    // The BroadcastReceiver used to listen from broadcasts from the service.

    // A reference to the service used to get location updates.
    private LocationUpdatesService mService = null;

    // Tracks the bound state of the service.
    private boolean mBound = false;

    // UI elements.
    private Button mRequestLocationUpdatesButton;
    private Button mRemoveLocationUpdatesButton;

    // The BroadcastReceiver used to listen from broadcasts from the service.
    private MyReceiver myReceiver;
//    private MyCellReceiver myCellReceiver;

    //****************************************************

    private MapBoxMapView mapView;
    private CameraOptions cameraPosition;
    private com.mapbox.geojson.Point loc_point;
    private com.mapbox.geojson.Point focus_point;
    private AnnotationPlugin annotationAPI;

    private float[] mRotationMatrix = new float[16];

    public static S2LatLng latl;
    public static S2CellId cell2;

    private Set<S2CellId> surround_cell_list_set = new HashSet<>();
    private ArrayList<S2CellId> surround_cell_list = new ArrayList<>();
    private ArrayList<S2CellId> all_neighbour_cell_list = new ArrayList<>();

    public static final int PERMISSIONS_REQUEST_READ_CONTACTS = 1;
    ArrayList<String> Contact_list = new ArrayList<>();
    ArrayList<String> Name_list = new ArrayList<>();
    ArrayList<SavedContactsModel> SavedContacts = new ArrayList<>();
    public static ArrayList<User> Contact_users = new ArrayList<>();
    ArrayList<String> Reg_user_ids = new ArrayList<>();

    Handler handler_zoom_back = new Handler();

    //old variables
    public static Location warlocreal = new Location("dummyprovider");
    private static final int CAM_REQUEST = 1888;
    Uri imageUri;
    QuickdropAdapter adapter;
    LinearLayoutManager layoutManager;
    String current_profile_pic, current_username="", pimage, curr_username = "", current_contact = "";
    int x = 2;
    //private ImageView Camera;
    private Button Camera;
    boolean flag = true;
    boolean flag_compass = true;
    String newId = FirebaseAuth.getInstance().getCurrentUser().getUid();
    //ArrayList<String> quick_user_id_list = new ArrayList<>();
    ArrayList<String> Usernames = new ArrayList<>();
    ArrayList<String> flags = new ArrayList<>();
    RecyclerView recyclerView;
    Switch switch_gudup;
    Gson gson = new Gson();
    Gson surround_gson = new Gson();

    Boolean map_enabled = true;

    ArrayList<String> unknown_user_ids = new ArrayList<>();

    //profile attributes
    TextView profile_name, profile_username, profile_bio, profile_location, profile_self_count, profile_global_count,bounduser;
    CircleImageView profile_picture, boundpp;
    TextView bounds_locality;
    private DatabaseReference profile_Database;
    DrawerLayout drawerLayout;
    LinearLayout bounds_parent_layout;

    //Bottomsheet attributes
    CardView bottomSheet;
    BottomSheetBehavior bottomSheetBehavior;
    TextView b_name, b_username, b_bio, b_location, b_self_count, b_global_count;
    ImageView quickreply_icon, quick_drop_icon;
    CircleImageView b_picture;

    CoordinatorLayout maps_layout;
    FrameLayout frag_container;
    NavigationView navigationView;
    RelativeLayout loading_layout;

    private String bottomsheet_id;

    private double angle;
    private GeomagneticField geoField;
    private float mDeclination;
    private ImageView ic_compass;
    private double bearing =0;
    private SensorManager mSensorManager;
    private Sensor mRotVectSensor;
    private PointAnnotationManager pointAnnotationManager;

    private ArrayList<QuickdropModel> currentCellquickList = new ArrayList<>();
    private boolean zoomedin = true;
    //quickdrop vars
    ArrayList<QuickdropModel> quick_drops = new ArrayList<>();
    QuickdropModel quick_profile = new QuickdropModel();
    HashMap<String, Boolean> id_seen_list = new HashMap<>();
    private GesturesPlugin gesturesPlugin;
    private boolean pinched = false;

    private CameraAnimationsPlugin camera;

    private ArrayList<String> memory_list = new ArrayList<>();

    public static ArrayList<String> unknown_memory_ids = new ArrayList<>();
    private String data_cell = "cell_1";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_maps);

        navigationView = findViewById(R.id.nav_view);
        maps_layout = findViewById(R.id.maps_layout);
        frag_container = findViewById(R.id.fragment_container);
        loading_layout = findViewById(R.id.loading_layout);

//        map_loading_dialog = new ProgressDialog(Maps.this);
//        map_loading_dialog.setTitle("Loading map...");
        navigationView.setVisibility(View.GONE);
        maps_layout.setVisibility(View.INVISIBLE);
        frag_container.setVisibility(View.INVISIBLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        new MyTask().execute();

        //requestContactPermission();

        //FToast.makeText(Maps.this, ""+mapView.getMapboxMap().getSize(), Toast.LENGTH_SHORT).show();

        FirebaseDatabase.getInstance().getReference("User").child(id).child("id").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if(!snapshot.exists()) {
                    FirebaseDatabase.getInstance().getReference("User").child(id).child("id").setValue(id);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

//        FirebaseDatabase.getInstance().getReference("User").child(id).child("version").addValueEventListener(new ValueEventListener() {
//            @Override
//            public void onDataChange(@NonNull DataSnapshot snapshot) {
//                if(!snapshot.exists()) {
//                    FirebaseDatabase.getInstance().getReference("User").child(id).child("version").setValue("version9");
//                }
//            }
//
//            @Override
//            public void onCancelled(@NonNull DatabaseError error) {
//
//            }
//        });

        FirebaseMessaging.getInstance().subscribeToTopic("005beta");

        ProcessLifecycleOwner.get().getLifecycle().addObserver(new AppLifecycleListener());

        Camera = findViewById(R.id.capture);
        switch_gudup = findViewById(R.id.global_switch);

        navigationView.setNavigationItemSelectedListener(this);
        View header = navigationView.getHeaderView(0);

        recyclerView = findViewById(R.id.rView);

        //boundsuser
        boundpp = findViewById(R.id.boundpp);
        bounduser = findViewById(R.id.bounduser);
        bounds_locality = findViewById(R.id.boundLocality);
        bounds_parent_layout = findViewById(R.id.bounds_parent_layout);

        //profile attributes initilaization
        profile_name = header.findViewById(R.id.profile_name);
        profile_username = header.findViewById(R.id.profile_username);
        profile_bio = header.findViewById(R.id.profile_bio);
        profile_location = header.findViewById(R.id.profile_location);
        profile_picture = header.findViewById(R.id.profile_picture);
        profile_global_count = header.findViewById(R.id.profile_total_count);
        profile_self_count = header.findViewById(R.id.profile_self_count);

        profile_Database = FirebaseDatabase.getInstance().getReference("User");
        profile_Database.keepSynced(true);
        drawerLayout = findViewById(R.id.drawer_layout);

        //bottomsheet attributes initialization
        b_name = findViewById(R.id.b_name);
        b_username = findViewById(R.id.b_username);
        b_bio = findViewById(R.id.b_bio);
        b_location = findViewById(R.id.b_location);
        b_picture = findViewById(R.id.b_picture);
        b_global_count = findViewById(R.id.b_global_count);
        b_self_count = findViewById(R.id.b_self_count);
        quickreply_icon = findViewById(R.id.quickreply_icon);
        quick_drop_icon = findViewById(R.id.quickdrop_icon);
        bottomSheet = findViewById(R.id.bottom_sheet_card);
        bottomSheetBehavior = BottomSheetBehavior.from(bottomSheet);

        //sensor
        mSensorManager = (SensorManager) this.getSystemService(Context.SENSOR_SERVICE);
        mRotVectSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR);

        recyclerView.setHasFixedSize(true);
        getCurrentUserDetails();
//        setquickdrop();
        //set_quick_seen();
        //set_unknown_user_list();
        quick_drops.add(quick_profile);
        layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new QuickdropAdapter(Maps.this, quick_drops, id_seen_list, pimage);
        recyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        adapter.setOnItemClickListener(onItemClickListener);
        adapter.setOnItemClickListener2(onItemClickListener2);

        //profile attributes data fetching
        profile_Database.child(id).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                String pname = dataSnapshot.child("name").getValue(String.class);
                curr_username = dataSnapshot.child("username").getValue(String.class);
                String pbio = dataSnapshot.child("bio").getValue(String.class);
                pimage = dataSnapshot.child("thumb_image").getValue(String.class);
                String plocation = dataSnapshot.child("Location").getValue(String.class);
                String global_count = dataSnapshot.child("global_count").getValue(String.class);
                String full_image = dataSnapshot.child("profilePicURL").getValue(String.class);
                String self_count = dataSnapshot.child("self_count").getValue(String.class);
                String oldtime = String.valueOf(dataSnapshot.child("account_creation_time").getValue());
                current_contact = String.valueOf(dataSnapshot.child("contact").getValue());
                String currtime = String.valueOf(System.currentTimeMillis());

                if (ContextCompat.checkSelfPermission(Maps.this, android.Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED
                        && quick_drops.size() > 0) {
                    quick_profile = new QuickdropModel(id, pimage, curr_username, -1, 0, true);
                    quick_drops.set(0,quick_profile);
                    adapter.notifyItemChanged(0);
                }

                profile_name.setText(pname);
                profile_username.setText(curr_username);
                profile_bio.setText(pbio);
                profile_location.setText(plocation);
                profile_global_count.setText(global_count);
                profile_self_count.setText(self_count);
                Picasso.with(Maps.this).load(pimage).placeholder(R.drawable.ic_dp_icon_male_04).into(profile_picture);

                adapter = new QuickdropAdapter(Maps.this, quick_drops, id_seen_list, pimage);
                recyclerView.setAdapter(adapter);
                adapter.notifyDataSetChanged();
                adapter.setOnItemClickListener(onItemClickListener);
                adapter.setOnItemClickListener2(onItemClickListener2);

                profile_picture.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showImage(pimage,full_image);
                    }
                });
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.w(TAG, "Failed to read value.", error.toException());
            }
        });

        updateToken(FirebaseInstanceId.getInstance().getToken());

        Camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(LocationUpdatesService.mLocation != null) {
                    cameraOpen();
                }
                else {
                    Toast.makeText(Maps.this, "Location is not yet updated!", Toast.LENGTH_SHORT).show();
                }


            }
        });

        switch_gudup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                flag = switch_gudup.isChecked();
                populateMarkers(flag);
            }

        });

        ic_compass = (ImageView) findViewById(R.id.ic_compass);
        ic_compass.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                flag_compass=!flag_compass;
            }
        });

        myReceiver = new MyReceiver();
//        myCellReceiver = new MyCellReceiver();

        //requestContactPermission();

        if (!checkPermissions()) {
            requestPermissions();
        }
        else{
            map_enabled=(true);
            enableLocationSettings();
        }

        mapView = findViewById(R.id.mapView);
        mapView.getMapboxMap().addOnMapLoadedListener(new OnMapLoadedListener() {
            @Override
            public void onMapLoaded(@NonNull MapLoadedEventData mapLoadedEventData) {

                //Toast.makeText(Maps.this, "Map loaded.", Toast.LENGTH_SHORT).show();
                //map_loading_dialog.cancel();
                navigationView.setVisibility(View.VISIBLE);
                maps_layout.setVisibility(View.VISIBLE);
                frag_container.setVisibility(View.VISIBLE);
                loading_layout.setVisibility(View.GONE);
                getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            }
        });
        mapView.getMapboxMap().loadStyleUri("mapbox://styles/davs/ckzs873gx000n14qm1lppjsvv");

        LocationComponentPlugin locationComponentPlugin = LocationComponentUtils.getLocationComponent(mapView);
        locationComponentPlugin.setEnabled(true);
        locationComponentPlugin.setPulsingEnabled(true);
        locationComponentPlugin.setPulsingColor(0xc6d7eb);
        locationComponentPlugin.setPulsingMaxRadius(150);

        CompassPlugin compass = CompassViewPluginKt.getCompass(mapView);
        compass.setEnabled(false);

        ScaleBarPlugin scaleBarPlugin = ScaleBarUtils.getScaleBar(mapView);
        scaleBarPlugin.setEnabled(false);

        float[] results = new float[]{0};

//        backgroundAlert = new AlertDialog.Builder(this)
//                .setTitle("Background Location Permission")
//                .setMessage("Locus requires to run in background to send you notifications of memories nearby.")
//
//                // Specifying a listener allows you to take an action before dismissing the dialog.
//                // The dialog is automatically dismissed when a dialog button is clicked.
//                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int which) {
//                        // Continue with delete operation
//                        ActivityCompat.requestPermissions(Maps.this,
//                                new String[]{Manifest.permission.ACCESS_BACKGROUND_LOCATION},
//                                35);
//                    }
//                })

                // A null listener allows the button to dismiss the dialog and take no further action.
//                .setNegativeButton(android.R.string.no, null)
//                .create();

//        LocationPuck3D locationPuck3D = new LocationPuck3D("asset://model.glb",Arrays.asList(0.0f, 0.0f, 0.0f),0,Arrays.asList(0.01f, 0.01f, 0.01f));
//
//        locationComponentPlugin.setLocationPuck(locationPuck3D);

        gesturesPlugin = GesturesUtils.getGestures((mapView));
        gesturesPlugin.setPitchEnabled(false);
        gesturesPlugin.setScrollEnabled(false);
        gesturesPlugin.setDoubleTapToZoomInEnabled(false);
        gesturesPlugin.setPinchToZoomEnabled(false);
        gesturesPlugin.setRotateEnabled(false);
        gesturesPlugin.setQuickZoomEnabled(false);
        gesturesPlugin.setDoubleTouchToZoomOutEnabled(false);

        int mWidth = this.getResources().getDisplayMetrics().widthPixels;
        int mHeight = this.getResources().getDisplayMetrics().heightPixels;
        Point centerOfRotation = new Point(mWidth / 2, mHeight * 2/3);
//        Log.i("centerOfRotation", String.valueOf(centerOfRotation));

        cameraPosition = new CameraOptions.Builder()
//                .center(com.mapbox.geojson.Point.fromLngLat(77.296817, 28.627414))
                .pitch(50.0)
                .zoom(18.5)
                .bearing(0.0)
                .build();

        mapView.getMapboxMap().setCamera(cameraPosition);

        annotationAPI = AnnotationPluginImplKt.getAnnotations((MapPluginProviderDelegate)mapView);

        pointAnnotationManager = PointAnnotationManagerKt.createPointAnnotationManager(annotationAPI, mapView);
        pointAnnotationManager.addClickListener(new OnPointAnnotationClickListener() {
            @Override
            public boolean onAnnotationClick(@NotNull PointAnnotation pointAnnotation) {
                if(surround_gson.fromJson(removeQuotesAndUnescape(String.valueOf(pointAnnotation.getData())),String.class).matches("surround")) {
                    Toast.makeText(Maps.this, "Too far!!", Toast.LENGTH_SHORT).show();
                }
                else {
                    Intent i = new Intent(Maps.this, dropViewActivity.class);
                    i.putExtra("Warhead", gson.fromJson(removeQuotesAndUnescape(String.valueOf(pointAnnotation.getData())), Warhead.class));
                    i.putExtra("username", curr_username);
                    i.putExtra("userprofile", current_profile_pic);
                    startActivity(i);
                    //finish();
                }

                return false;
            }
        });

        locationComponentPlugin.addOnIndicatorPositionChangedListener(new OnIndicatorPositionChangedListener() {
            @Override
            public void onIndicatorPositionChanged(@NonNull com.mapbox.geojson.Point point) {

                loc_point = point;
                double earth = 6378.137,  //radius of the earth in kilometer
                        pi = Math.PI,
                        m = (1 / ((2 * pi / 360) * earth)) / 1000;  //1 meter in degree

                double distance_from_focus_point = results[0];
                double new_latitude = point.latitude() + (Math.cos(mapView.getMapboxMap().getCameraState().getBearing()* (pi / 180))*(distance_from_focus_point * m));
                double new_longitude = point.longitude() + (Math.sin(mapView.getMapboxMap().getCameraState().getBearing()* (pi / 180))*((distance_from_focus_point * m)/ Math.cos(point.latitude() * (pi / 180))));
//                double new_longitude = point.longitude() ;
                focus_point = com.mapbox.geojson.Point.fromLngLat(new_longitude, new_latitude);
                if(zoomedin && !pinched && mapView.getMapboxMap().getCameraState().getZoom() == 18.5) {
                    mapView.getMapboxMap().setCamera(new CameraOptions.Builder()
                            .center(focus_point)
//                        .bearing(17.0)
                            .build());
                    com.mapbox.geojson.Point point2 = mapView.getMapboxMap().coordinateForPixel(new ScreenCoordinate(centerOfRotation.x, centerOfRotation.y));
                    if (results[0] == 0) {
                        Location.distanceBetween(point.latitude(), point.longitude(), point2.latitude(), point2.longitude(), results);
                        Log.i("distance", String.valueOf(results[0]));
                    }
                }
//                Log.i("loc_point", String.valueOf(mapView.getMapboxMap().pixelForCoordinate(point)));
            }
        });
    }

    private String geContactFromStorage() {
        SharedPreferences Preferences = getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        String contact = Preferences.getString("my_phone","");
        return contact;
    }

    private void updateToken(String token) {

        DatabaseReference reference = FirebaseDatabase.getInstance().getReference("Tokens");
        Token token1 = new Token(token);
        reference.child(FirebaseAuth.getInstance().getCurrentUser().getUid()).setValue(token1);

    }

    private View.OnLongClickListener onItemClickListener = new View.OnLongClickListener() {
        @Override
        public boolean onLongClick(View view) {

            RecyclerView.ViewHolder viewHolder = (RecyclerView.ViewHolder) view.getTag();
            int position = viewHolder.getAdapterPosition();
            bottomsheet_id = quick_drops.get(position).getUser_id();
            if(position == 0) {
                drawerLayout.openDrawer(GravityCompat.START);
            }
            else {
                if (quick_drops.get(position).getFlag() != -1) {
                    if (bottomsheet_id != null) {
                        profile_Database.child(bottomsheet_id).addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {

                                String pname = dataSnapshot.child("name").getValue(String.class);
                                String username = dataSnapshot.child("username").getValue(String.class);
                                String pbio = dataSnapshot.child("bio").getValue(String.class);
                                String pimage = dataSnapshot.child("thumb_image").getValue(String.class);
                                String full_image = dataSnapshot.child("profilePicURL").getValue(String.class);
                                String plocation = dataSnapshot.child("Location").getValue(String.class);
                                String global_count = dataSnapshot.child("global_count").getValue(String.class);
                                String self_count = dataSnapshot.child("self_count").getValue(String.class);

                                b_name.setText(pname);
                                b_username.setText(username);
                                b_bio.setText(pbio);
                                b_location.setText(plocation);
                                //b_global_count.setText(global_count);
                                b_self_count.setText(get_warhead_count(bottomsheet_id)+"");
                                Picasso.with(Maps.this).load(pimage).placeholder(R.drawable.ic_dp_icon_male_04).into(b_picture);

                                get_memory_by_you(bottomsheet_id);

                                b_picture.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        showImage(pimage, full_image);
                                    }
                                });

                                //bottomsheet quick reply button
                                quickreply_icon.setImageResource(R.drawable.quickreply_white);
                                if (id_seen_list.containsKey(bottomsheet_id)) {
                                    if (!id_seen_list.get(bottomsheet_id)) {
                                        quickreply_icon.setImageResource(R.drawable.ic_unread);
                                    }

                                }
                                quickreply_icon.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                                        Intent intent = new Intent(Maps.this, ChatActivity.class);
                                        intent.putExtra("user_id", bottomsheet_id);
                                        intent.putExtra("username", username);
                                        intent.putExtra("userprofile", full_image);
                                        intent.putExtra("userprofile_thumb", pimage);
                                        intent.putExtra("open_from", "maps");
                                        startActivity(intent);
                                        overridePendingTransition(0, 0);
                                    }
                                });

                                quick_drop_icon.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                                        Intent intent = new Intent(Maps.this, CameraCaptureActivity.class);
                                        intent.putExtra("quickID", bottomsheet_id);
                                        startActivity(intent);
                                        overridePendingTransition(0, 0);
                                    }
                                });


                            }

                            @Override
                            public void onCancelled(DatabaseError error) {
                                // Failed to read value
                                Log.w(TAG, "Failed to read value.", error.toException());
                            }
                        });
                        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                    }
                }
            }
            //Toast.makeText(Maps.this, ""+bottomsheet_id, Toast.LENGTH_SHORT).show();
            return true;

        }
    };

    private View.OnClickListener onItemClickListener2 = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            RecyclerView.ViewHolder viewHolder = (RecyclerView.ViewHolder) view.getTag();
            int position = viewHolder.getAdapterPosition();
            bottomsheet_id = quick_drops.get(position).getUser_id();

            if(position == 0) {
                drawerLayout.openDrawer(GravityCompat.START);
            }
            else {

                if (quick_drops.get(position).getFlag() == 1) {

                    DatabaseReference foryou_list_reference = FirebaseDatabase.getInstance().getReference("foryou_list").child(id).child("received");

                    Query query = foryou_list_reference.orderByChild("sender").equalTo(quick_drops.get(position).getUser_id());

                    query.addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                            ArrayList<Double> distances = new ArrayList<>();
                            ArrayList<Warhead> warheads = new ArrayList<>();

                            int memory_count = (int) dataSnapshot.getChildrenCount();

                            for (DataSnapshot dsp : dataSnapshot.getChildren()) {

                                Warhead warhead = dsp.getValue(Warhead.class);
                                if (!warhead.getIsViewed()) {
                                    warheads.add(warhead);
                                    Location location = new Location("");
                                    location.setLatitude(warhead.getDroplat());
                                    location.setLongitude(warhead.getDroplon());
                                    distances.add((double) warlocreal.distanceTo(location) / 1000);
                                }

                            }

                            if (distances.size() != 0) {

                                Warhead nearest_forYou = warheads.get(distances.indexOf(Collections.min(distances)));

                                if (nearest_forYou != null) {

                                    if (check_warhead_cell(nearest_forYou)) {
                                        //Toast.makeText(Maps.this, "Contains", Toast.LENGTH_SHORT).show();
                                        Intent intent = new Intent(Maps.this, dropViewActivity.class);
                                        intent.putExtra("Warhead", nearest_forYou);
                                        intent.putExtra("username", curr_username);
                                        intent.putExtra("userprofile", current_profile_pic);
                                        startActivity(intent);
                                        //finish();
                                    } else {
                                        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                                                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                                        zoomedin = false;
                                        gesturesPlugin.setPinchToZoomEnabled(false);
                                        mapView.getMapboxMap().setBounds(
                                                new CameraBoundsOptions.Builder()
                                                        .minZoom(0.0)
                                                        .build());

                                        Location nearestloc = new Location("");
                                        nearestloc.setLatitude(nearest_forYou.getDroplat());
                                        nearestloc.setLongitude(nearest_forYou.getDroplon());

                                        handler_zoom_back.removeCallbacksAndMessages(null);
                                        bounds_parent_layout.setVisibility(View.GONE);
                                        bounds_parent_layout.setVisibility(View.VISIBLE);
                                        bounds_parent_layout.setAlpha(0.0f);
                                        Picasso.with(Maps.this).load(quick_drops.get(position).getUser_image()).placeholder(R.drawable.ic_dp_icon_male_04).into(boundpp);
                                        bounduser.setText(quick_drops.get(position).getUser_name());
                                        String locality = getLocality(nearestloc.getLatitude(), nearestloc.getLongitude());
                                        bounds_locality.setVisibility(View.GONE);
                                        if(locality != null) {
                                            bounds_locality.setVisibility(View.VISIBLE);
                                            bounds_locality.setText(locality);
                                            bounds_locality.setAlpha(0.0f);
                                        }
//                                            gesturesPlugin = GesturesUtils.getGestures((mapView));

                                        CoordinateBounds coordinateBounds = new CoordinateBounds(
                                                com.mapbox.geojson.Point.fromLngLat(warlocreal.getLongitude(),
                                                        warlocreal.getLatitude()),
                                                com.mapbox.geojson.Point.fromLngLat(nearestloc.getLongitude(),
                                                        nearestloc.getLatitude())
                                        );
                                        recyclerView.animate()
                                                .translationY(-150.0f)
                                                .alpha(0.0f)
                                                .setDuration(1000);
                                        bounds_parent_layout.animate()
                                                .alpha(1.0f)
                                                .setDuration(1000);

//                                            bounds_parent_layout.setVisibility(View.VISIBLE);

                                        Camera.animate()
                                                .translationY(150.0f)
                                                .alpha(0.0f)
                                                .setDuration(1000);

                                        ic_compass.animate()
                                                .translationY(150.0f)
                                                .alpha(0.0f)
                                                .setDuration(1000);

                                        Handler handler_recycle_vis = new Handler();
                                        handler_recycle_vis.postDelayed(new Runnable() {
                                            @Override
                                            public void run() {
                                                recyclerView.setVisibility(View.GONE);
                                                Camera.setVisibility(View.GONE);
                                                ic_compass.setVisibility(View.GONE);
                                            }
                                        }, 1000);

                                        CameraOptions boundsOptions = mapView.getMapboxMap().cameraForCoordinateBounds(coordinateBounds, new EdgeInsets(200.0, 100.0, 180.0, 100.0), 0.0, 0.0);
//                                            mapView.getMapboxMap().setCamera(boundsOptions);
                                        camera = CameraAnimationsUtils.getCamera(mapView);
                                        //zoomout animation
                                        final Cancelable cancelable = camera.flyTo(
                                                boundsOptions,
                                                new MapAnimationOptions.Builder().duration(4000).build()
                                        );

                                        Handler handler_markerremove = new Handler();
                                        handler_markerremove.postDelayed(new Runnable() {
                                            @Override
                                            public void run() {

                                                annotationAPI.removeAnnotationManager(pointAnnotationManager);
                                                pointAnnotationManager = PointAnnotationManagerKt.createPointAnnotationManager(annotationAPI, mapView);
                                                pointAnnotationManager.create(new PointAnnotationOptions()
                                                        .withPoint(com.mapbox.geojson.Point.fromLngLat(nearestloc.getLongitude(), nearestloc.getLatitude()))
                                                        .withIconImage(getBitmap(R.drawable.ic_yellow_bubble_bounds)));

                                            }
                                        }, 1000);

                                        Handler handler_locality = new Handler();
                                        handler_locality.postDelayed(new Runnable() {
                                            @Override
                                            public void run() {

                                                bounds_locality.animate()
                                                        .alpha(1.0f)
                                                        .setDuration(1000);

                                            }
                                        }, 3000);

                                        Snackbar snackBar;


                                        if (Collections.min(distances) < 1) {
                                            if (Collections.min(distances) * 1000 <= 100) {
                                                snackBar = Snackbar.make(view, "Almost there! Memory ForYou  is " + (Math.round(Collections.min(distances) * 100d) / 100d) * 1000 + " meters away.", Snackbar.LENGTH_INDEFINITE);
                                            } else {
                                                snackBar = Snackbar.make(view, "Nearest memory ForYou is " + (Math.round(Collections.min(distances) * 100d) / 100d) * 1000 + " meters away.", Snackbar.LENGTH_INDEFINITE);

                                            }
                                        } else {

                                                snackBar = Snackbar.make(view, "Nearest memory ForYou is " + (Math.round(Collections.min(distances) * 100d) / 100d) + " kilometers away.", Snackbar.LENGTH_INDEFINITE);
                                            }
                                            snackBar.show();
                                            Handler handler_zoomin = new Handler();
                                            handler_zoomin.postDelayed(new Runnable() {
                                                @Override
                                                public void run() {
                                                    snackBar.dismiss();
                                                    cameraPosition = new CameraOptions.Builder()
                                                            .center(focus_point)
                                                            .pitch(50.0)
                                                            .zoom(18.5)
                                                            .build();
                                                    final Cancelable cancelable = camera.flyTo(
                                                            cameraPosition,
                                                            new MapAnimationOptions.Builder().duration(4000).build()
                                                    );
                                                    recyclerView.setVisibility(View.VISIBLE);
                                                    recyclerView.animate()
                                                            .translationY(0)
                                                            .alpha(1.0f)
                                                            .setDuration(1000);

                                                    bounds_parent_layout.animate()
                                                            .alpha(0.0f)
                                                            .setDuration(1000);

                                                    bounds_locality.animate()
                                                            .alpha(0.0f)
                                                            .setDuration(1000);

                                                    Camera.setVisibility(View.VISIBLE);
                                                    Camera.animate()
                                                            .translationY(0)
                                                            .alpha(1.0f)
                                                            .setDuration(1000);

                                                    ic_compass.setVisibility(View.VISIBLE);
                                                    ic_compass.animate()
                                                            .translationY(0)
                                                            .alpha(1.0f)
                                                            .setDuration(1000);

                                                }
                                            }, 6000);

                                            //restore to original settings
                                            Handler handler_currentloc_cam = new Handler();
                                            handler_currentloc_cam.postDelayed(new Runnable() {
                                                @Override
                                                public void run() {
                                                    bounds_parent_layout.setVisibility(View.GONE);
                                                    populateMarkers(flag);
                                                    mapView.getMapboxMap().setBounds(
                                                            new CameraBoundsOptions.Builder()
                                                                    .minZoom(18.5)
                                                                    .build());
                                                    zoomedin = true;
                                                    bounds_locality.setVisibility(View.GONE);
                                                    getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                                                }

                                            }, 10000);
                                        }
                                    }
                                }
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {

                            }
                        });

                }
                else if (quick_drops.get(position).getFlag() == 0) {
                    if (bottomsheet_id != null) {
                        profile_Database.child(bottomsheet_id).addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {

                                String pname = dataSnapshot.child("name").getValue(String.class);
                                String username = dataSnapshot.child("username").getValue(String.class);
                                String pbio = dataSnapshot.child("bio").getValue(String.class);
                                String pimage = dataSnapshot.child("thumb_image").getValue(String.class);
                                String full_image = dataSnapshot.child("profilePicURL").getValue(String.class);
                                String plocation = dataSnapshot.child("Location").getValue(String.class);
                                String global_count = dataSnapshot.child("global_count").getValue(String.class);
                                String self_count = dataSnapshot.child("self_count").getValue(String.class);
                                String oldtime = String.valueOf(dataSnapshot.child("account_creation_time").getValue());
                                String currtime = String.valueOf(System.currentTimeMillis());

                                b_name.setText(pname);
                                b_username.setText(username);
                                b_bio.setText(pbio);
                                b_location.setText(plocation);
                               // b_global_count.setText(global_count);
                                b_self_count.setText(get_warhead_count(bottomsheet_id)+"");
                                Picasso.with(Maps.this).load(pimage).placeholder(R.drawable.ic_dp_icon_male_04).into(b_picture);
                                //Toast.makeText(Maps.this, "No memory ForYou.", Toast.LENGTH_SHORT).show();

                                get_memory_by_you(bottomsheet_id);

                                b_picture.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        showImage(pimage, full_image);
                                    }
                                });

                                //bottomsheet quick reply button
                                quickreply_icon.setImageResource(R.drawable.quickreply_white);
                                if (id_seen_list.containsKey(bottomsheet_id)) {
                                    if (!id_seen_list.get(bottomsheet_id)) {
                                        quickreply_icon.setImageResource(R.drawable.ic_unread);
                                    }

                                }
                                quickreply_icon.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                                        Intent intent = new Intent(Maps.this, ChatActivity.class);
                                        intent.putExtra("user_id", bottomsheet_id);
                                        intent.putExtra("username", username);
                                        intent.putExtra("userprofile_thumb", pimage);
                                        intent.putExtra("userprofile", full_image);
                                        intent.putExtra("open_from", "maps");

                                        startActivity(intent);
                                        overridePendingTransition(0, 0);
                                    }
                                });

                                quick_drop_icon.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                                        Intent intent = new Intent(Maps.this, CameraCaptureActivity.class);
                                        intent.putExtra("quickID", bottomsheet_id);
                                        startActivity(intent);
                                        overridePendingTransition(0, 0);
                                    }
                                });


                            }

                            @Override
                            public void onCancelled(DatabaseError error) {
                                // Failed to read value
                                Log.w(TAG, "Failed to read value.", error.toException());
                            }
                        });
                        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                    }
                }
                else {
                    drawerLayout.openDrawer(GravityCompat.START);
                }
            }

        }
    };

    private boolean check_warhead_cell(Warhead warhead) {

        S2LatLng latLng = S2LatLng.fromDegrees(warhead.getDroplat(), warhead.getDroplon());
        S2CellId cell18 = S2CellId.fromLatLng(latLng).parent(18);

        return cell_list.contains(cell18);
    }

    public void showImage(String thumb_string, String img_url) {
        Dialog builder = new Dialog(this);
        builder.requestWindowFeature(Window.FEATURE_NO_TITLE);
        builder.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));
        builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                //nothing;
            }
        });

        ImageView imageView = new ImageView(this);

        //imageView.setImageResource(R.drawable.ic_dp_icon_male_04);
        //Picasso.with(ChatActivity.this).load(img_url).placeholder(thumb_string).into(imageView);
        Picasso.with(Maps.this)
                .load(thumb_string) // small quality url goes here
                .placeholder(R.drawable.ic_dp_icon_male_04)
                .into(imageView, new com.squareup.picasso.Callback() {
                    @Override
                    public void onSuccess() {
                        Picasso.with(Maps.this).load(img_url)
                                .placeholder(imageView.getDrawable()).into(imageView);
//                                PhotoViewAttacher photoAttacher;
//                                photoAttacher= new PhotoViewAttacher(imageView);
//                                photoAttacher.canZoom();
                    }

                    @Override
                    public void onError() {

                    }
                });
        builder.addContentView(imageView, new RelativeLayout.LayoutParams(
                (int) pxFromDp(300),
                (int) pxFromDp(300)));
        builder.show();
    }

    private float pxFromDp(float dp) {
        return dp * getResources().getDisplayMetrics().density;
    }

    private final ServiceConnection mServiceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            Log.i("spare_fuct_call", "onServiceConnected");
            LocationUpdatesService.LocalBinder binder = (LocationUpdatesService.LocalBinder) service;
            mService = binder.getService();
            if (!checkPermissions()) {
                requestPermissions();
            } else {
                if(mService != null) {
                    mService.requestLocationUpdates();
                    map_enabled = (true);
                }
            }
//            if (!checkBackgroundPermissions()) {
//                Snackbar.make(
//                        findViewById(R.id.maps_layout),
//                        R.string.permission_rationale,
//                        Snackbar.LENGTH_INDEFINITE)
//                        .setActionTextColor(Color.WHITE)
//                        .setAction(R.string.ok, new View.OnClickListener() {
//                            @Override
//                            public void onClick(View view) {
//                                ActivityCompat.requestPermissions(Maps.this,
//                                        new String[]{Manifest.permission.ACCESS_BACKGROUND_LOCATION},
//                                        35);
//                            }
//                        })
//                        .show();
//            }
            mBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            Log.i("spare_fuct_call", "onServiceDisConnected");
            mService = null;
            mBound = false;
        }
    };

    public void cameraOpen() {
        Log.i("spare_fuct_call", "cameraOpen");

        //TestEncryptData("Hey");

        Intent myIntent = new Intent(this,
                CameraCaptureActivity.class);
        startActivity(myIntent);

        //testAES("Vikas");

    }

    private float getDeviceHeight()
    {
        return getResources().getDisplayMetrics().heightPixels;
    }

    private float getDeviceWidth()
    {
        return getResources().getDisplayMetrics().widthPixels;
    }

    private float[] applyLowPassFilter(float[] input, float[] output) {
        if ( output == null ) return input;

        for ( int i=0; i<input.length; i++ ) {
            output[i] = output[i] + 0.5f * (input[i] - output[i]);
        }
        return output;
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
//        Log.i("SensorChange","SensorChange");

        if(zoomedin && !pinched){
            if (event.sensor.getType() == Sensor.TYPE_ROTATION_VECTOR) {
                mRotationMatrix = applyLowPassFilter(event.values, mRotationMatrix);
                SensorManager.getRotationMatrixFromVector(
                        mRotationMatrix, event.values);
                float[] orientation = new float[3];
                SensorManager.getOrientation(mRotationMatrix, orientation);
                double sensorBearing = (double) Math.toDegrees(orientation[0]) + mDeclination;
//            Log.i("sensorBearing", String.valueOf(sensorBearing));
                if (Math.abs(Math.toDegrees(orientation[0]) - angle) > 0.3) {
                    if (loc_point != null) {
                        if (flag_compass) {
                            cameraPosition = new CameraOptions.Builder()
                                    .anchor(mapView.getMapboxMap().pixelForCoordinate(loc_point))
                                    .bearing(sensorBearing)
                                    .build();
                            mapView.getMapboxMap().setCamera(cameraPosition);
                            ic_compass.setRotation(-(float) sensorBearing);
                        }
                    }
                }
                angle = Math.toDegrees(orientation[0]);
            }
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }

    private String getLocality(Double lat, Double lon) {
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        String locality="";
        try {
            List<Address> addresses = geocoder.getFromLocation(lat, lon, 1);
            if(addresses.size() > 0) {
                Address obj = addresses.get(0);
                locality = obj.getSubLocality();
            }
//            add = add + "\n" + obj.getCountryName();
//            add = add + "\n" + obj.getCountryCode();
//            add = add + "\n" + obj.getAdminArea();
//            add = add + "\n" + obj.getPostalCode();
//            add = add + "\n" + obj.getSubAdminArea();
//            add = add + "\n" + obj.getLocality();
//            add = add + "\n" + obj.getSubThoroughfare();
//            add = add + "\n" + obj.getFeatureName();
//            add = add + "\n" + obj.getPhone();
//            add = add + "\n" + obj.getPremises();
//            add = add + "\n" + obj.getSubLocality();
//            add = add + "\n" + obj.getThoroughfare();
//            add = add + "\n" + obj.getLocale();
//            add = add + "\n" + obj.getUrl();
//            add = add + "\n" + obj.describeContents();
//            add = add + "\n" + obj.getExtras();

        } catch (Exception e) {
            e.printStackTrace();
        }
            return  locality;
    }

    public void populateMarkers(Boolean flag) {
//        CoordinateBounds coordinateBounds = new CoordinateBounds(
//                com.mapbox.geojson.Point.fromLngLat(warlocreal.getLongitude()-0.001,
//                        warlocreal.getLatitude()-0.001),
//                com.mapbox.geojson.Point.fromLngLat(warlocreal.getLongitude()+0.001,
//                        warlocreal.getLatitude()+0.001)
//        );
//        mapView.getMapboxMap().setBounds(new CameraBoundsOptions.Builder().bounds(coordinateBounds).build());

        //Toast.makeText(Maps.this, "Populate marker", Toast.LENGTH_SHORT).show();

        annotationAPI.removeAnnotationManager(pointAnnotationManager);
        pointAnnotationManager = PointAnnotationManagerKt.createPointAnnotationManager(annotationAPI, mapView);

        pointAnnotationManager.addClickListener(new OnPointAnnotationClickListener() {
            @Override
            public boolean onAnnotationClick(@NotNull PointAnnotation pointAnnotation) {

                if(gson.fromJson(removeQuotesAndUnescape(String.valueOf(pointAnnotation.getData())),Warhead.class).getSendername().matches("surround")) {
                    Toast.makeText(Maps.this, "Too far!!", Toast.LENGTH_SHORT).show();
                }
                else {
                    Intent i = new Intent(Maps.this, dropViewActivity.class);
                    i.putExtra("Warhead", gson.fromJson(removeQuotesAndUnescape(String.valueOf(pointAnnotation.getData())), Warhead.class));
                    i.putExtra("username", curr_username);
                    i.putExtra("userprofile", current_profile_pic);
                    startActivity(i);
                    //finish();
                }

                return false;
            }
        });

        //normal markers
        if(!(String.valueOf(cell_list.get(0).id()).matches(""))) {
            if (flag) {

                // global markers
                for (int i = 0; i < cell_list.size(); i++) {
                    FirebaseDatabase.getInstance().getReference("TeenageWarheads").child(String.valueOf(cell_list.get(i).id()))
                            .addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            if (dataSnapshot.hasChildren()) {
                                for (DataSnapshot dsp : dataSnapshot.getChildren()) {
                                    if(dsp.child("imgUrl").exists()) {
                                        Warhead warhead = dsp.getValue(Warhead.class);
                                        //Log.i("warhead: ","value "+warhead.getWarid());

                                        if (warhead != null && !(warhead.getBackimgurl().equals(" "))) {
                                            PointAnnotation pointAnnotation = pointAnnotationManager.create(new PointAnnotationOptions()
                                                    .withPoint(com.mapbox.geojson.Point.fromLngLat(warhead.getDroplon(), warhead.getDroplat()))
                                                    .withIconImage(getBitmap(R.drawable.ic_green_bubble)
                                                    ));
                                            pointAnnotation.setData(new JsonPrimitive(gson.toJson(warhead)));
                                        }
                                    }
                                }
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });
                }
            }

            ArrayList<String> repeat_ids = new ArrayList<>();

            // forU marker
            FirebaseDatabase.getInstance().getReference("foryou").child(id).child("received").addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    currentCellquickList.clear();
                    repeat_ids.clear();
                    for (int i = 0; i < cell_list.size(); i++) {

                        if (dataSnapshot.child(String.valueOf(cell_list.get(i).id())).hasChildren()) {

                            for (DataSnapshot dsp : dataSnapshot.child(String.valueOf(cell_list.get(i).id())).getChildren()) {
                                if(dsp.child("imgUrl").exists()) {
                                    Warhead warhead = dsp.getValue(Warhead.class);

                                    if (warhead != null && !(warhead.getBackimgurl().equals(" "))) {
                                        if (!repeat_ids.contains(warhead.getSender())) {
                                            QuickdropModel quickdrop = new QuickdropModel(warhead.getSender(), warhead.getSenderimage(), warhead.getSendername()
                                                    , 2, 0, true);
                                            repeat_ids.add(warhead.getSender());
                                            currentCellquickList.add(quickdrop);
                                        }
                                        PointAnnotation pointAnnotation = pointAnnotationManager.create(new PointAnnotationOptions()
                                                .withPoint(com.mapbox.geojson.Point.fromLngLat(warhead.getDroplon(), warhead.getDroplat()))
                                                .withIconImage(getBitmap(R.drawable.ic_yellow_bubble)
                                                ));
                                        pointAnnotation.setData(new JsonPrimitive(gson.toJson(warhead)));
                                    }
                                }
                            }
                        }
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });

            //self marker
            FirebaseDatabase.getInstance().getReference("self").child(id).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    for (int i = 0; i < cell_list.size(); i++) {
                        if(dataSnapshot.child(String.valueOf(cell_list.get(i).id())).hasChildren()) {
                            for (DataSnapshot dsp : dataSnapshot.child(String.valueOf(cell_list.get(i).id())).getChildren()) {
                                if(dsp.child("imgUrl").exists()) {
                                    Warhead warhead = dsp.getValue(Warhead.class);

                                    if (warhead != null && !(warhead.getBackimgurl().equals(" "))) {
                                        if (warhead.getIsViewed()) {
                                            PointAnnotation pointAnnotation = pointAnnotationManager.create(new PointAnnotationOptions()
                                                    .withPoint(com.mapbox.geojson.Point.fromLngLat(warhead.getDroplon(), warhead.getDroplat()))
                                                    .withIconImage(getBitmap(R.drawable.ic_blue_bubble)
                                                    ));
                                            pointAnnotation.setData(new JsonPrimitive(gson.toJson(warhead)));
                                        } else {
                                            PointAnnotation pointAnnotation = pointAnnotationManager.create(new PointAnnotationOptions()
                                                    .withPoint(com.mapbox.geojson.Point.fromLngLat(warhead.getDroplon(), warhead.getDroplat()))
                                                    .withIconImage(getBitmap(R.drawable.ic_blue_bubble)
                                                    ));
                                            pointAnnotation.setData(new JsonPrimitive(gson.toJson(warhead)));
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {

                }
            });

            //sent memories
            FirebaseDatabase.getInstance().getReference("foryou").child(id).child("sent").addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    for (int i = 0; i < cell_list.size(); i++) {
                        if (dataSnapshot.child(String.valueOf(cell_list.get(i).id())).hasChildren()) {
                            for (DataSnapshot dsp : dataSnapshot.child(String.valueOf(cell_list.get(i).id())).getChildren()) {
                                if(dsp.child("imgUrl").exists()) {
                                    Warhead warhead = dsp.getValue(Warhead.class);
                                    //Log.i("warhead: ","value "+warhead.getWarid());
                                    if (warhead != null && !(warhead.getBackimgurl().equals(" "))) {
                                        PointAnnotation pointAnnotation = pointAnnotationManager.create(new PointAnnotationOptions()
                                                .withPoint(com.mapbox.geojson.Point.fromLngLat(warhead.getDroplon(), warhead.getDroplat()))
                                                .withIconImage(getBitmap(R.drawable.ic_sent_bubble)
                                                ));
                                        pointAnnotation.setData(new JsonPrimitive(gson.toJson(warhead)));
                                    }
                                }
                            }
                        }
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });

        }

        //surround markers
        all_neighbour_cell_list.clear();
        surround_cell_list_set.clear();

        for (int i=0; i<cell_list.size(); i++) {
            if(cell_list.get(i).id() != cell2.id())
                cell_list.get(i).getAllNeighbors(18,all_neighbour_cell_list);
            Log.i("CELLID",cell_list.get(i).id()+"");
        }

        for (int i=0; i<all_neighbour_cell_list.size(); i++) {
            if(!cell_list.contains(all_neighbour_cell_list.get(i))) {
                surround_cell_list_set.add(all_neighbour_cell_list.get(i));
            }
        }
        surround_cell_list = new ArrayList<>(surround_cell_list_set);
        if(surround_cell_list.size() > 0) {
            if(flag) {
                if (!(String.valueOf(surround_cell_list.get(0).id()).matches(""))) {
                    if (flag) {

                        FirebaseDatabase.getInstance().getReference("TeenageWarheads").addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                for (int i = 0; i < surround_cell_list.size(); i++) {
                                    if (dataSnapshot.child(String.valueOf(surround_cell_list.get(i).id())).hasChildren()) {
                                        for (DataSnapshot dsp : dataSnapshot.child(String.valueOf(surround_cell_list.get(i).id())).getChildren()) {
                                            if(dsp.child("imgurl").exists()) {
                                                Warhead warhead = dsp.getValue(Warhead.class);
                                                //Log.i("warhead: ","value "+warhead.getWarid());

                                                if (warhead != null && !(warhead.getBackimgurl().equals(" "))) {
                                                    PointAnnotation pointAnnotation = pointAnnotationManager.create(new PointAnnotationOptions()
                                                            .withPoint(com.mapbox.geojson.Point.fromLngLat(warhead.getDroplon(), warhead.getDroplat()))
                                                            .withIconImage(getBitmap(R.drawable.ic_black_bubble)
                                                            ));
                                                    pointAnnotation.setData(new JsonPrimitive(gson.toJson(new Warhead())));
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        });
                    }

//                FirebaseDatabase.getInstance().getReference("User").addValueEventListener(new ValueEventListener() {
//                    @Override
//                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//
//                        final String username = String.valueOf(dataSnapshot.child(id).child("name").getValue());
//                        final String userprofile = String.valueOf(dataSnapshot.child(id).child("thumb_image").getValue());
//
//                        for (int i = 0; i < surround_cell_list.size(); i++) {
//
//                            if (dataSnapshot.child(id).child("vWar").child(String.valueOf(surround_cell_list.get(i).id())).hasChildren()) {
//
//                                for (DataSnapshot dsp : dataSnapshot.child(id).child("vWar").child(String.valueOf(surround_cell_list.get(i).id())).getChildren()) {
//                                    Warhead warhead = dsp.getValue(Warhead.class);
//
//                                    if (warhead != null && !(warhead.getBackimgurl().equals(" "))) {
//                                        PointAnnotation pointAnnotation = pointAnnotationManager.create(new PointAnnotationOptions()
//                                                .withPoint(com.mapbox.geojson.Point.fromLngLat(warhead.getDroplon(), warhead.getDroplat()))
//                                                .withIconImage(getBitmap(R.drawable.ic_black_bubble)
//                                                ));
//                                        pointAnnotation.setData(new JsonPrimitive(gson.toJson(new Warhead())));
//                                    }
//                                }
//                            }
//                            if (dataSnapshot.child(id).child("memories").child(String.valueOf(surround_cell_list.get(i).id())).hasChildren()) {
//
//                                for (DataSnapshot dsp : dataSnapshot.child(id).child("memories").child(String.valueOf(surround_cell_list.get(i).id())).getChildren()) {
//
//                                    Warhead warhead = dsp.getValue(Warhead.class);
//
//                                    if (warhead != null && !(warhead.getBackimgurl().equals(" "))) {
//
//                                        if (warhead.getIsViewed()) {
//                                            PointAnnotation pointAnnotation = pointAnnotationManager.create(new PointAnnotationOptions()
//                                                    .withPoint(com.mapbox.geojson.Point.fromLngLat(warhead.getDroplon(), warhead.getDroplat()))
//                                                    .withIconImage(getBitmap(R.drawable.ic_black_bubble)
//                                                    ));
//                                            pointAnnotation.setData(new JsonPrimitive(gson.toJson(new Warhead())));
//                                        } else {
//                                            PointAnnotation pointAnnotation = pointAnnotationManager.create(new PointAnnotationOptions()
//                                                    .withPoint(com.mapbox.geojson.Point.fromLngLat(warhead.getDroplon(), warhead.getDroplat()))
//                                                    .withIconImage(getBitmap(R.drawable.ic_black_bubble)
//                                                    ));
//                                            pointAnnotation.setData(new JsonPrimitive(gson.toJson(new Warhead())));
//                                        }
//                                    }
//                                }
//                            }
//                        }
//                    }
//
//                    @Override
//                    public void onCancelled(@NonNull DatabaseError databaseError) {
//
//                    }
//                });
                }
            }
        }

        //Log.i("Cells", String.valueOf(surround_cell_list));
        Log.i("Cellsize", String.valueOf(cell_list.size()));
        Log.i("Cellsize", String.valueOf(all_neighbour_cell_list.size()));
        Log.i("Cellsize", String.valueOf(surround_cell_list.size()));

    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CAM_REQUEST && resultCode == Activity.RESULT_OK) {
            Intent i = new Intent(Maps.this, PhotoActivity.class);
            i.putExtra("photo", imageUri);
            startActivity(i);

        }
    }

    private String removeQuotesAndUnescape(String uncleanJson) {
        String noQuotes = uncleanJson.replaceAll("^\"|\"$", "");

        return StringEscapeUtils.unescapeJava(noQuotes);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()) {
            case R.id.nav_edit_profile:
                startActivity(new Intent(Maps.this, EditProfile.class));
                break;
            case R.id.nav_connections:
                startActivity(new Intent(Maps.this, ConnectionsActivity.class));
                break;
            case R.id.nav_notifications:
                startActivity(new Intent(Maps.this, NotificationsActivity.class));
                break;
            case R.id.nav_help:
                startActivity(new Intent(Maps.this, HelpActivity.class));
                break;
            case R.id.nav_aboutus:
                startActivity(new Intent(Maps.this, AboutUsActivity.class));
                break;

            case R.id.nav_requests: {
                Intent intent = new Intent(Maps.this, RequestsActivity.class);
                intent.putExtra("from", "Maps");
                startActivity(intent);
                break;
            }
        }
        //drawerLayout.closeDrawer(GravityCompat.START);
        return false;
    }

    public class AppLifecycleListener implements LifecycleObserver {

        @OnLifecycleEvent(Lifecycle.Event.ON_START)
        public void onMoveToForeground() {
            // app moved to foreground
        }

        @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
        public void onMoveToBackground() {

            //startService(new Intent(getApplicationContext(), Myservice.class));

        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        mSensorManager.registerListener(this,
                mRotVectSensor,
                SensorManager.SENSOR_STATUS_ACCURACY_LOW);

        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();

        LocalBroadcastManager.getInstance(this).registerReceiver(myReceiver,
                new IntentFilter(LocationUpdatesService.LOCATION_BROADCAST));
//        LocalBroadcastManager.getInstance(this).registerReceiver(myCellReceiver,
//                new IntentFilter(LocationUpdatesService.CELL_BROADCAST));
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.i("spare_fuct_call", "OnStop");
        if (mBound) {
            // Unbind from the service. This signals to the service that this activity is no longer
            // in the foreground, and the service can respond by promoting itself to a foreground
            // service.
            this.unbindService(mServiceConnection);
            mBound = false;
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.i("spare_fuct_call", "onStart");
        //Toast.makeText(Maps.this, "onStart", Toast.LENGTH_SHORT).show();
        requestContactPermission();
        //bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        //drawerLayout.closeDrawers();
        // Bind to the service. If the service is in foreground mode, this signals to the service
        // that since this activity is in the foreground, the service can exit foreground mode.
        this.bindService(new Intent(this, LocationUpdatesService.class), mServiceConnection,
                Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mSensorManager.unregisterListener(this);
        Log.i("spare_fuct_call", "onPause");
        LocalBroadcastManager.getInstance(this).unregisterReceiver(myReceiver);
//        LocalBroadcastManager.getInstance(this).unregisterReceiver(myCellReceiver);
//        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
    }

    @Override public boolean dispatchTouchEvent(MotionEvent event){
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            if (bottomSheetBehavior.getState()==BottomSheetBehavior.STATE_EXPANDED) {

                Rect outRect = new Rect();
                bottomSheet.getGlobalVisibleRect(outRect);

                if(!outRect.contains((int)event.getRawX(), (int)event.getRawY()))
                    bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            }
        }

        return super.dispatchTouchEvent(event);
    }

    private boolean checkPermissions() {
        Log.i("spare_fuct_call", "checkpermission");
        return PackageManager.PERMISSION_GRANTED == checkSelfPermission(
                Manifest.permission.ACCESS_FINE_LOCATION);
    }

//    private boolean checkBackgroundPermissions() {
//        //Log.i("spare_fuct_call", "checkpermission");
//        return PackageManager.PERMISSION_GRANTED == checkSelfPermission(
//                Manifest.permission.ACCESS_BACKGROUND_LOCATION);
//    }

    private void startLocationPermissionRequest() {

        ActivityCompat.requestPermissions(Maps.this,
                new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                REQUEST_PERMISSIONS_REQUEST_CODE);

//            try {
//            ActivityCompat.requestPermissions(Maps.this,
//                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.ACCESS_BACKGROUND_LOCATION},
//                    REQUEST_PERMISSIONS_REQUEST_CODE); }
//            catch (Exception e) {
//                Log.i("Permission both error ",e.toString());
//                ActivityCompat.requestPermissions(Maps.this,
//                        new String[]{Manifest.permission.ACCESS_BACKGROUND_LOCATION},
//                        REQUEST_PERMISSIONS_REQUEST_CODE);
//            }
    }

    private void requestPermissions() {
        Log.i("spare_fuct_call", "requestPermissions");
        boolean shouldProvideRationale =
                shouldShowRequestPermissionRationale(
                        Manifest.permission.ACCESS_FINE_LOCATION);

        // Provide an additional rationale to the user. This would happen if the user denied the
        // request previously, but didn't check the "Don't ask again" checkbox.
        if (shouldProvideRationale) {
            Log.i(TAG, "Displaying permission rationale to provide additional context.");
            map_enabled=(false);
//            Toast.makeText(Maps.this, "chalao", Toast.LENGTH_SHORT).show();
            Snackbar.make(
                    findViewById(R.id.maps_layout),
                    R.string.permission_rationale,
                    Snackbar.LENGTH_INDEFINITE)
                    .setActionTextColor(Color.WHITE)
                    .setAction(R.string.ok, new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            startLocationPermissionRequest();
                            // Request permission
//                            requestPermissions(
//                                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_BACKGROUND_LOCATION},
//                                    REQUEST_PERMISSIONS_REQUEST_CODE);
                        }
                    })
                    .show();
        } else {
            Log.i(TAG, "Requesting permission");
            startLocationPermissionRequest();
            // Request permission. It's possible this can be auto answered if device policy
            // sets the permission in a given state or the user denied the permission
            // previously and checked "Never ask again".
//            requestPermissions(
//                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_BACKGROUND_LOCATION},
//                    REQUEST_PERMISSIONS_REQUEST_CODE);
        }
    }

    /**
     * Callback received when a permissions request has been completed.
     */

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Log.i("spare_fuct_call", "onRequestPermissionResult");

        if (requestCode == PERMISSIONS_REQUEST_READ_CONTACTS) {
            if (grantResults.length <= 0) {
                // If user interaction was interrupted, the permission request is cancelled and you
                // receive empty arrays.
                Log.i(TAG, "User interaction was cancelled.");
            } else if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permission was granted.
//                getallContacts();
//                getregcontacts();
                setquickdrop();
            } else {
                // Permission denied.
                Snackbar.make(
                        findViewById(R.id.maps_layout),
                        "Contact permission needed.",
                        Snackbar.LENGTH_INDEFINITE)
                        .setAction(R.string.settings, new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                // Build intent that displays the App settings screen.
                                Intent intent = new Intent();
                                intent.setAction(
                                        Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                Uri uri = Uri.fromParts("package",
                                        BuildConfig.APPLICATION_ID, null);
                                intent.setData(uri);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                            }
                        })
                        .show();
            }
        }

        //background location results
//        if (requestCode == 35) {
//            if (grantResults.length <= 0) {
//
//                // If user interaction was interrupted, the permission request is cancelled and you
//                // receive empty arrays.
//                Log.i(TAG, "User interaction was cancelled.");
//            } else if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                // Permission was granted.
//                map_enabled=(true);
//                mService.requestLocationUpdates();
//                enableLocationSettings();
//            } else {
//                //requestPermissions();
//                //Toast.makeText(Maps.this, "Denied", Toast.LENGTH_SHORT).show();
//                // Permission denied.
//                map_enabled=(false);
//                Snackbar.make(
//                        findViewById(R.id.maps_layout),
//                        "Please allow background location for full functionality",
//                        Snackbar.LENGTH_INDEFINITE)
//                        .setActionTextColor(Color.WHITE)
//                        .setAction(R.string.settings, new View.OnClickListener() {
//                            @Override
//                            public void onClick(View view) {
//                                // Build intent that displays the App settings screen.
//                                Intent intent = new Intent();
//                                intent.setAction(
//                                        Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
//                                Uri uri = Uri.fromParts("package",
//                                        BuildConfig.APPLICATION_ID, null);
//                                intent.setData(uri);
//                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                                startActivity(intent);
//                            }
//                        })
//                        .show();
//            }
//        }
        //fine location result
        if (requestCode == REQUEST_PERMISSIONS_REQUEST_CODE) {

            if (grantResults.length <= 0) {

                // If user interaction was interrupted, the permission request is cancelled and you
                // receive empty arrays.
                Log.i(TAG, "User interaction was cancelled.");
            } else if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permission was granted.
//                if(!checkBackgroundPermissions()) {
//                    backgroundAlert.show();
//                }


                if(mService != null) {
                    mService.requestLocationUpdates();
                    map_enabled=(true);
                }
                enableLocationSettings();

            } else {
                Snackbar.make(
                        findViewById(R.id.maps_layout),
                        R.string.permission_denied_explanation,
                        Snackbar.LENGTH_INDEFINITE)
                        .setActionTextColor(Color.WHITE)
                        .setAction(R.string.settings, new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                // Build intent that displays the App settings screen.
                                Intent intent = new Intent();
                                intent.setAction(
                                        Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                Uri uri = Uri.fromParts("package",
                                        BuildConfig.APPLICATION_ID, null);
                                intent.setData(uri);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                            }
                        })
                        .show();
            }

            requestContactPermission();

        }
    }

    /**
     * Receiver for broadcasts sent by {@link LocationUpdatesService}.
     */

    private class MyReceiver extends BroadcastReceiver {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(Maps.this, android.R.style.Theme_Material_Light_NoActionBar_Fullscreen);
        AlertDialog alert11;
        boolean dialogFlag = false;
        void createMockDialog(){
            dialogBuilder.setMessage("It looks like you are using mock location. Please turn it off and restart the app.");
            dialogBuilder.setCancelable(false);
            dialogBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    finishAndRemoveTask();
                }
            });
            alert11 = dialogBuilder.create();
            dialogFlag = true;
        }
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.i("spare_fuct_call", "MyReciever onReceive");

            Location location = intent.getParcelableExtra(LocationUpdatesService.EXTRA_LOCATION);
            if (location != null) {
                if (!location.isFromMockProvider()) {
                    if (ActivityCompat.checkSelfPermission(Maps.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                            && ActivityCompat.checkSelfPermission(Maps.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        return;
                    }

                    warlocreal = location;
                    //Log.i("COORDI",location.getLatitude()+" "+location.getLongitude());

                    latl = S2LatLng.fromDegrees(location.getLatitude(), location.getLongitude());
                    cell2 = S2CellId.fromLatLng(latl).parent(18);
                    //Log.i("COORDI", String.valueOf(cell2.id()));

                    geoField = new GeomagneticField(
                            Double.valueOf(location.getLatitude()).floatValue(),
                            Double.valueOf(location.getLongitude()).floatValue(),
                            Double.valueOf(location.getAltitude()).floatValue(),
                            System.currentTimeMillis()
                    );
                    mDeclination = geoField.getDeclination();
//                    Log.i("mDeclination", String.valueOf(mDeclination));

                    if(!data_cell.matches(String.valueOf(cell2.id()))) {
                        populateMarkers(flag);
                        data_cell =String.valueOf(cell2.id());
                    }
                }
                else {
                    if(!dialogFlag) {
                        createMockDialog();
                        alert11.show();
                    }
                }

            }
        }
    }

//    private class MyCellReceiver extends BroadcastReceiver {
//
//        @Override
//        public void onReceive(Context context, Intent intent) {
//
//            populateMarkers(flag);
//
////            ArrayList<QuickdropModel> messageList = new ArrayList<>();
////            ArrayList<QuickdropModel> cellDropList = new ArrayList<>();
////            ArrayList<QuickdropModel> foryouList = new ArrayList<>();
////            ArrayList<QuickdropModel> contactList = new ArrayList<>();
////            ArrayList<String> repeat_ids = new ArrayList<>();
////
////            ArrayList<String> quick_ids = new ArrayList<>();
////
////            for(int i = 1; i < quick_drops.size(); i++) {
////                quick_ids.add(quick_drops.get(i).getUser_id());
////            }
////
////            for(int i = 1; i < currentCellquickList.size(); i++) {
////
////                if (quick_ids.contains(currentCellquickList.get(i).getUser_id()) && quick_drops.get(i).isSeen()) {
////                    quick_drops.get(i).setFlag(2);
////                }
////            }
////
////            for(int i = 1; i < quick_drops.size(); i++) {
////
////                if(!quick_drops.get(i).isSeen() && !repeat_ids.contains(quick_drops.get(i).getUser_id())) {
////                    repeat_ids.add(quick_drops.get(i).getUser_id());
////                    messageList.add(quick_drops.get(i));
////                }
////                if(quick_drops.get(i).getFlag() == 2 && !repeat_ids.contains(quick_drops.get(i).getUser_id())){
////                    repeat_ids.add(quick_drops.get(i).getUser_id());
////                    cellDropList.add(quick_drops.get(i));
////                }
////                if(quick_drops.get(i).getFlag() == 1 && !repeat_ids.contains(quick_drops.get(i).getUser_id())){
////                    repeat_ids.add(quick_drops.get(i).getUser_id());
////                    foryouList.add(quick_drops.get(i));
////                }
////                if(quick_drops.get(i).getFlag() == 0 && !repeat_ids.contains(quick_drops.get(i).getUser_id())){
////                    repeat_ids.add(quick_drops.get(i).getUser_id());
////                    contactList.add(quick_drops.get(i));
////                }
////            }
////            quick_drops.clear();
////            currentCellquickList.clear();
////            quick_drops.add(quick_profile);
////            quick_drops.addAll(messageList);
////            quick_drops.addAll(cellDropList);
////            quick_drops.addAll(foryouList);
////            quick_drops.addAll(contactList);
//
//
//        }
//    }

    private void getCurrentUserDetails() {
        FirebaseDatabase.getInstance().getReference("User").child(id).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                current_profile_pic = String.valueOf(dataSnapshot.child("thumb_image").getValue());
                current_username = String.valueOf(dataSnapshot.child("username").getValue());
                //current_contact = String.valueOf(dataSnapshot.child(id).child("contact").getValue());
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    protected void enableLocationSettings() {
        LocationRequest locationRequest = LocationRequest.create()
                .setInterval(1)
                .setFastestInterval(1)
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);

        LocationServices
                .getSettingsClient(this)
                .checkLocationSettings(builder.build())
                .addOnSuccessListener(this, (LocationSettingsResponse response) -> {
                    // startUpdatingLocation(...);
                })
                .addOnFailureListener(this, ex -> {
                    if (ex instanceof ResolvableApiException) {
                        // Location settings are NOT satisfied,  but this can be fixed  by showing the user a dialog.
                        try {
                            // Show the dialog by calling startResolutionForResult(),  and check the result in onActivityResult().
                            ResolvableApiException resolvable = (ResolvableApiException) ex;
                            resolvable.startResolutionForResult(Maps.this, REQUEST_PERMISSIONS_REQUEST_CODE);
                        } catch (IntentSender.SendIntentException sendEx) {
                            // Ignore the error.
                        }
                    }
                });
    }

    @Override
    public void onBackPressed() {
        this.moveTaskToBack(true);
    }

    public float angleBetweenLines(Point center, Point endLine1, Point endLine2){
        float a = endLine1.x - center.x;
        float b = endLine1.y - center.y;
        float c = endLine2.x - center.x;
        float d = endLine2.y - center.y;

        float atan1 = (float) Math.atan2(a,b);
        float atan2 = (float) Math.atan2(c,d);

        return (float) ((atan1 - atan2) * 180 / Math.PI);
    }

    private Bitmap getBitmap(int drawableRes) {
        Drawable drawable = getResources().getDrawable(drawableRes);
        Canvas canvas = new Canvas();
        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        canvas.setBitmap(bitmap);
        drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
        drawable.draw(canvas);

        return bitmap;
    }

    @Override
    public void onUpdateMapAfterUserInterection(Point touchpoint,Point newTouchpoint) {
        if(zoomedin && !pinched) {
            if (map_enabled) {
                Point centerOfRotation = new Point((int) mapView.getMapboxMap().pixelForCoordinate(loc_point).getX(), (int) mapView.getMapboxMap().pixelForCoordinate(loc_point).getY());

                final float angle = angleBetweenLines(centerOfRotation, touchpoint, newTouchpoint);
                // abs because when we touch somewhere far angle between tp and ntp becomes accordingly large and behaves like spring -ayush
                if (Math.abs(angle) < 5) {
                    new Handler().post(new Runnable() {
                        @Override
                        public void run() {
                            // move the camera (NOT animateCamera() ) to new position with "bearing" updated
                            // Log.i("angle", String.valueOf(angle));
                            flag_compass = false;
                            bearing = mapView.getMapboxMap().getCameraState().getBearing() - angle;
                            cameraPosition = new CameraOptions.Builder()
//                            .anchor(new ScreenCoordinate(centerOfRotation.x,centerOfRotation.y))
                                    .anchor(mapView.getMapboxMap().pixelForCoordinate(loc_point))
                                    .bearing(bearing)
                                    .build();
                            mapView.getMapboxMap().setCamera(cameraPosition);
                            ic_compass.setRotation(-(float) bearing);

//                    Log.i("bearing", String.valueOf(mapView.getMapboxMap().getCameraState().getBearing()));
                        }
                    });
                }
            }
        }
    }

    @Override
    public void onPinchInteraction(float distance) {

        if (zoomedin && !pinched){
                pinched = true;
//            gesturesPlugin = GesturesUtils.getGestures((mapView));
//            gesturesPlugin.setScrollEnabled(true);
                gesturesPlugin.setPinchToZoomEnabled(true);

                mapView.getMapboxMap().setBounds(
                        new CameraBoundsOptions.Builder()
//                            .bounds(new CoordinateBounds(
//                                    mapView.getMapboxMap().coordinateForPixel(new ScreenCoordinate(0, 100)),
//                                    mapView.getMapboxMap().coordinateForPixel(new ScreenCoordinate(getDeviceWidth(), getDeviceHeight()))
//                            ))
                                .minZoom(18.5)
                                .build());

        }
        if(mapView.getMapboxMap().getCameraState().getZoom() <= 18.5){
//            gesturesPlugin.setPinchToZoomEnabled(false);
            pinched = false;
        }
//
        else{
            handler_zoom_back.removeCallbacksAndMessages(null);
            if (zoomedin) {
                camera = CameraAnimationsUtils.getCamera(mapView);
                handler_zoom_back.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        cameraPosition = new CameraOptions.Builder()
                                .center(focus_point)
                                .pitch(50.0)
                                .zoom(18.5)
                                .build();
                        final Cancelable cancelable = camera.flyTo(
                                cameraPosition,
                                new MapAnimationOptions.Builder().duration(1000).build()
                        );
                        pinched = false;
                    }
                }, 2000);
            }
        }



//        float pixels =  100 * this.getResources().getDisplayMetrics().density;
//        float zoom_coff = distance / 10000;
//        new Handler().post(new Runnable() {
//                               @Override
//                               public void run() {
//
//                                   View grad_sky = findViewById(R.id.grad_sky);
//                                   View grad_map = findViewById(R.id.grad_map);
//
//                                   cameraPosition = new CameraOptions.Builder()
//                                           //                        .center(focus_point)
//                                           .anchor(mapView.getMapboxMap().pixelForCoordinate(loc_point))
//                                           .pitch(mapView.getMapboxMap().getCameraState().getPitch()+zoom_coff*37.5)
//                                           .zoom(mapView.getMapboxMap().getCameraState().getZoom()+zoom_coff)
//                                           .build();
//
//                                   if ((mapView.getMapboxMap().getCameraState().getPitch()+zoom_coff*37.5)<75 && (mapView.getMapboxMap().getCameraState().getZoom()+zoom_coff)<20 && (mapView.getMapboxMap().getCameraState().getZoom()+zoom_coff)>18){
//                                       Log.i("grad_sky.getHeight()", String.valueOf(grad_sky.getHeight()));
//                                       mapView.getMapboxMap().setCamera(cameraPosition);
//                                       if ((mapView.getMapboxMap().getCameraState().getPitch())>70)
//                                       {   double pitchdiff = (mapView.getMapboxMap().getCameraState().getPitch()-70);
//                                           grad_sky.setLayoutParams(new LinearLayout.LayoutParams(grad_sky.getWidth(),(int)(pixels*(pitchdiff/5))));
//                                           grad_map.setLayoutParams(new LinearLayout.LayoutParams(grad_map.getWidth(), (int)(pixels*(pitchdiff/5))));
//                                       }
//                                       else{
//                                           grad_sky.setLayoutParams(new LinearLayout.LayoutParams(grad_sky.getWidth(),0));
//                                           grad_map.setLayoutParams(new LinearLayout.LayoutParams(grad_map.getWidth(), 0));
//                                       }
//                                   }
//                                   Log.i("zoom", String.valueOf(mapView.getMapboxMap().getCameraState().getZoom()));
//                                   Log.i("pitch", String.valueOf(mapView.getMapboxMap().getCameraState().getPitch()));
//                               }
//                           }
//        );

    }

    public void requestContactPermission() {
        if (ContextCompat.checkSelfPermission(Maps.this, android.Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(Maps.this,
                    new String[]{android.Manifest.permission.READ_CONTACTS},
                    PERMISSIONS_REQUEST_READ_CONTACTS);
        } else {
            setquickdrop();
//            getallContacts();
//            getregcontacts();
        }
    }

    private void setquickdrop() {

        DatabaseReference foryou_list_reference = FirebaseDatabase.getInstance().getReference("foryou_list").child(id).child("received");

        foryou_list_reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                memory_list.clear();
                quick_drops.clear();
                quick_profile.setFlag(-1);
                quick_drops.add(quick_profile);
                ArrayList<String> repeat_ids = new ArrayList<>();
                repeat_ids.add("no id");
                getallContacts();

                if(snapshot.hasChildren()) {
                    for (DataSnapshot dsp : snapshot.getChildren()) {
                        Warhead warhead = dsp.getValue(Warhead.class);
                        memory_list.add(warhead.getSender());
                        if (!warhead.getIsViewed()) {
                            QuickdropModel quick_drop = new QuickdropModel(warhead.getSender(),warhead.getSenderimage()
                                    ,warhead.getSendername(),1, 0, true);
                            if(!repeat_ids.contains(warhead.getSender())) {
                                repeat_ids.add(warhead.getSender());
                                quick_drops.add(quick_drop);
                            }
                        }
                    }
                }

                getregcontacts(repeat_ids);

                QuickdropModel first_quick = new QuickdropModel(quick_drops.get(0).getUser_id(),quick_drops.get(0).getUser_image()
                        ,quick_drops.get(0).getUser_name(),0, 0, true);

                quick_drops.remove(0);
                repeat_ids.remove(0);

                quick_drops.add(first_quick);
                repeat_ids.add("no id");

                Collections.reverse(repeat_ids);
                Collections.reverse(quick_drops);

                set_quick_seen();

                adapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

    }

    private String getCountryCode(String number) {


        PhoneNumberUtil phoneUtil = PhoneNumberUtil.createInstance(this);;
        try {
            Phonenumber.PhoneNumber NumberProto = phoneUtil.parse(number, "IN");
            if (!phoneUtil.isValidNumber(NumberProto)) {
                //Log.i("Contact","Not Valid");
                //showError(getResources().getString(R.string.wrong_phone_number));
                return number;
            }
            String regionISO = String.valueOf(NumberProto.getCountryCode());
            if(!(regionISO.matches(""))){
                //Log.i("Contact","Valid "+regionISO);
                number = "+"+regionISO+number;
            }


        } catch (NumberParseException e) {
            //Log.i("Contact","Exception: "+e.toString());
            //showError(e.toString());
            return number;
        }

        return number;
    }

    void getallContacts() {
        Cursor phones = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI
                , null,null,null, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME+" ASC");
        while (phones.moveToNext())
        {
            String name=phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
            String phoneNumber = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
            phoneNumber = phoneNumber.replaceAll("[()\\s-]", "");
            if(phoneNumber.length() > 0) {
                if (phoneNumber.charAt(0) != '+') {
                    phoneNumber = getCountryCode(phoneNumber);
                }
                if (phoneNumber.charAt(0) == '0') {
                    phoneNumber = phoneNumber.substring(1);
                }
                if (!phoneNumber.equals(geContactFromStorage())) {
                    //Toast.makeText(Maps.this, geContactFromStorage(), Toast.LENGTH_SHORT).show();
                    Contact_list.add(phoneNumber);
                    Name_list.add(name);
                    Log.i("Contact", name + " " + phoneNumber);
                    SavedContacts.add(new SavedContactsModel(name, phoneNumber));
                }
            }
            //Toast.makeText(this, , Toast.LENGTH_SHORT).show();
        }
        phones.close();
    }

    void getregcontacts(ArrayList<String> repeat_ids) {

        final DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference("User");
        databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Contact_users.clear();
                Reg_user_ids.clear();
                unknown_memory_ids.clear();
                for(DataSnapshot dsp : dataSnapshot.getChildren()) {
                    String contact = String.valueOf(dsp.child("contact").getValue());
//                    User user = dsp.getValue(User.class);

                    String uid,name,username,bio,dob,mobile,profile_url,thumb_url,email,Location,online, acc_creation_time;
                    uid = String.valueOf(dsp.child("id").getValue());
                    name = String.valueOf(dsp.child("name").getValue());
                    username = String.valueOf(dsp.child("username").getValue());
                    bio = String.valueOf(dsp.child("bio").getValue());
                    dob = String.valueOf(dsp.child("birthday").getValue());
                    mobile = String.valueOf(dsp.child("contact").getValue());
                    profile_url = String.valueOf(dsp.child("profilePicURL").getValue());
                    thumb_url = String.valueOf(dsp.child("thumb_image").getValue());
                    email = String.valueOf(dsp.child("email").getValue());
                    Location = String.valueOf(dsp.child("Location").getValue());
                    online = String.valueOf(dsp.child("online").getValue());
                    acc_creation_time = String.valueOf(dsp.child("account_creation_time").getValue());
                    User user = new User(Location, bio, dob, mobile, uid, name, online, profile_url, thumb_url, username,acc_creation_time);

                    if(Contact_list.contains(contact)) {
                        // reg_Contact_list.add(new SavedContactsModel(Name_list.get(Contact_list.indexOf(contact)),contact));
                            Contact_users.add(user);
                            Reg_user_ids.add(user.getId());

                    }
                }
                ArrayList<Integer> remove_indices = new ArrayList<Integer>();
                int deletor = 0;
                for(int i  = 0; i < repeat_ids.size(); i++) {
                    if (!Reg_user_ids.contains(repeat_ids.get(i)) && !repeat_ids.get(i).matches("no id")) {
                        unknown_memory_ids.add(repeat_ids.get(i));
                        remove_indices.add(i);
//                        repeat_ids.remove(i-deletor);
//                        quick_drops.remove(i-deletor);
//                        deletor++;
                    }
                }

                for(int i = 0; i <remove_indices.size(); i++) {
                    int pos = remove_indices.get(i);
                    repeat_ids.remove(pos-deletor);
                    quick_drops.remove(pos-deletor);
                    deletor++;
                }

                for(int i  = 0; i < Contact_users.size(); i++) {

                    User user = Contact_users.get(i);
                    String id = user.getId();
                    String imgurl = user.getThumb_image();
                    String contact_username = user.getUsername();
                    QuickdropModel quick_contact = new QuickdropModel(id, imgurl, contact_username, 0,0,true);
                    Log.i("contact : ","contacts "+quick_drops.size());

                    if (!(repeat_ids.contains(id))) {
                        repeat_ids.add(id);
                        quick_drops.add(quick_contact);
                    }
                    else {
                        //quick_drops.get(repeat_ids.indexOf(id)).setUser_image(user.getThumb_image());
                    }
                }

                adapter.notifyDataSetChanged();
                //getUserfromIds(reg_Contact_id_list);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    void set_quick_seen() {

        DatabaseReference seen_reference = FirebaseDatabase.getInstance().getReference("Chat").child(id);
        seen_reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                id_seen_list.clear();
                int count = 1;
                if(snapshot.hasChildren()) {
                for (DataSnapshot dsp : snapshot.getChildren()) {
                    if(dsp.child("seen").exists() && dsp.child("timestamp").exists()) {
                        String seen_time = String.valueOf(dsp.child("timestamp").getValue());
                        boolean seen = dsp.child("seen").getValue(Boolean.class);
                    for (int i = 0; i < quick_drops.size(); i++) {
                        if (dsp.getKey().equals(quick_drops.get(i).getUser_id())) {
                            if (!seen) {
                                quick_drops.get(i).setSeentime(Double.parseDouble(seen_time));
                                quick_drops.get(i).setSeen(seen);
                                QuickdropModel temp_quick = quick_drops.get(i);
                                quick_drops.remove(i);
                                quick_drops.add(count, temp_quick);
                                count++;
                            }
                        }
                    }

                    if (dsp.child("seen").exists()) {

                        boolean is_seen = (Boolean) dsp.child("seen").getValue();
                        //Toast.makeText(Maps.this, ""+id_seen_list.size(), Toast.LENGTH_SHORT).show();
                        if (!is_seen)
                            id_seen_list.put(dsp.getKey(), is_seen);

                    }
                    arrange_seen();
                }

                }
                adapter.notifyDataSetChanged();
            }
        }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    private void arrange_seen() {
        QuickdropModel first_quick = new QuickdropModel(quick_drops.get(0).getUser_id(),quick_drops.get(0).getUser_image()
                ,quick_drops.get(0).getUser_name(),quick_drops.get(0).getFlag(), quick_drops.get(0).getSeentime(), true);
        quick_drops.remove(0);
        quick_drops.sort((lhs, rhs) -> Double.compare(rhs.getSeentime(), lhs.getSeentime()));
        quick_drops.add(0,first_quick);

    }

    private int get_warhead_count(String id) {
        int count = 0;
        for(int i = 0; i < memory_list.size(); i++) {
            if(memory_list.get(i).matches(id)){
                count++;
            }
        }
        return count;
    }

    private void get_memory_by_you(String bottomsheet_id) {

        DatabaseReference ref = FirebaseDatabase.getInstance().getReference("foryou_list").child(bottomsheet_id).child("received");
        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                int count = 0;
                for(DataSnapshot dsp : dataSnapshot.getChildren()) {
                    String user_id= dsp.child("sender").getValue(String.class);
                    if(id.equals(user_id)) {
                        count++;
                    }
                }
                b_global_count.setText(count+"");
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

    }

    public class MyTask extends AsyncTask<Void, Void, Void> {
        public void onPreExecute() {
            loading_layout.setVisibility(View.VISIBLE);
            //map_loading_dialog.show();
        }
        public Void doInBackground(Void... unused) {
            return null;
        }
    }

}