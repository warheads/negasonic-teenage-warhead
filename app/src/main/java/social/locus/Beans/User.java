package social.locus.Beans;

import java.io.Serializable;

public class User implements Serializable {

    private String location, bio, birthday, contact, id, name, online, profilePicURL, thumb_image, username;
    private String acc_creat_time;

    public User(String location, String bio, String birthday, String contact,
                String id, String name, String online, String profilePicURL, String thumb_image, String username, String acc_creat_time) {
        this.bio=bio;
        this.location = location;
        this.birthday = birthday;
        this.contact = contact;
        this.id = id;
        this.name = name;
        this.online = online;
        this.profilePicURL = profilePicURL;
        this.thumb_image = thumb_image;
        this.username = username;
        this.acc_creat_time=acc_creat_time;
    }

    public String get_acc_creat_time() {
        return acc_creat_time;
    }

    public String getBio() {
        return bio;
    }

    public String getLocation() {
        return location;
    }

    public String getBirthday() {
        return birthday;
    }

    public String getContact() {
        return contact;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getOnline() {
        return online;
    }

    public String getProfilePicURL() {
        return profilePicURL;
    }

    public String getThumb_image() {
        return thumb_image;
    }

    public String getUsername() {
        return username;
    }
}
