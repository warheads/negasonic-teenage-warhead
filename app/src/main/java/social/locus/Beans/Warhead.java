package social.locus.Beans;

import java.io.Serializable;

/**
 * Created by Vikas Mehra on 01-Feb-18.
 */

public class Warhead implements Serializable {

    private String imgUrl,backimgurl,caption,sender,sendername, senderimage,sendtime,type,warid,viewtime,thumb;
    private Double droplat,droplon;
    private Boolean isViewed;
    private int likes;

    public Warhead() {

        this.sendername = "surround";

    }

    public void setBackimgurl(String backimgurl) {
        this.backimgurl = backimgurl;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public void setSendername(String sendername) {
        this.sendername = sendername;
    }

    public void setSenderimage(String senderimage) {
        this.senderimage = senderimage;
    }

    public void setSendtime(String sendtime) {
        this.sendtime = sendtime;
    }

    public void setWarid(String warid) {
        this.warid = warid;
    }

    public void setViewtime(String viewtime) {
        this.viewtime = viewtime;
    }

    public void setThumb(String thumb) {
        this.thumb = thumb;
    }

    public void setDroplat(Double droplat) {
        this.droplat = droplat;
    }

    public void setDroplon(Double droplon) {
        this.droplon = droplon;
    }

    public void setViewed(Boolean viewed) {
        isViewed = viewed;
    }

    public Warhead(String imgUrl, String backimgurl, String caption, String sender, String sendername, String senderimage,
                   String sendtime, Double droplat, Double droplon, String type, String warid,
                   Boolean isViewed, String viewtime, int likes, String thumb) {
        this.imgUrl = imgUrl;
        this.backimgurl = backimgurl;
        this.caption = caption;
        this.sender = sender;
        this.sendername = sendername;
        this.senderimage = senderimage;
        this.sendtime = sendtime;
        this.droplat = droplat;
        this.droplon = droplon;
        this.type = type;
        this.warid = warid;
        this.isViewed = isViewed;
        this.viewtime = viewtime;
        this.likes = likes;
        this.thumb = thumb;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public String getBackimgurl() {
        return backimgurl;
    }

    public String getCaption() {
        return caption;
    }

    public String getSender() {
        return sender;
    }

    public String getSendername() {
        return sendername;
    }

    public String getSenderimage() {
        return senderimage;
    }

    public String getSendtime() {
        return sendtime;
    }

    public Double getDroplat() {
        return droplat;
    }

    public Double getDroplon() {
        return droplon;
    }

    public String getType() {
        return type;
    }

    public String getWarid() {
        return warid;
    }

    public Boolean getIsViewed() {
        return isViewed;
    }

    public String getViewtime() {
        return viewtime;
    }

    public int getLikes() {
        return likes;
    }

    public String getThumb() {
        return thumb;
    }

    public void setLikes(int likes) {
        this.likes = likes;
    }
}
