package social.locus.Beans;

/**
 * Created by Vikas Mehra on 23-Jan-18.
 */

public class Galactus {

    private String name, username, gid, profileurl;

    public Galactus() {
    }

    public Galactus(String name, String username, String gid, String profileurl) {

        this.name = name;
        this.username = username;
        this.gid = gid;
        this.profileurl = profileurl;
    }

    public String getprofileurl() {
        return profileurl;
    }

    public String getName() {
        return name;
    }

    public String getgid() {
        return gid;
    }

    public String getUsername() {
        return username;
    }
}