package social.locus.Beans;

public class QuickdropModel {

    private String user_id, user_image, User_name;
    private int flag;
    private boolean seen;
    private double seentime;

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getUser_image() {
        return user_image;
    }

    public void setUser_image(String user_image) {
        this.user_image = user_image;
    }

    public String getUser_name() {
        return User_name;
    }

    public QuickdropModel() {
    }

    public boolean isSeen() {
        return seen;
    }

    public void setSeen(boolean seen) {
        this.seen = seen;
    }

    public double getSeentime() {
        return seentime;
    }

    public void setSeentime(double seentime) {
        this.seentime = seentime;
    }

    public void setUser_name(String user_name) {
        User_name = user_name;
    }

    public int getFlag() {
        return flag;
    }

    public void setFlag(int flag) {
        this.flag = flag;
    }

    public QuickdropModel(String user_id, String user_image, String user_name, int flag, double seentime, boolean seen) {
        this.user_id = user_id;
        this.user_image = user_image;
        User_name = user_name;
        this.flag = flag;
        this.seen = seen;
        this.seentime = seentime;
    }
}
