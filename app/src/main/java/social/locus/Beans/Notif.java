package social.locus.Beans;


public class Notif {
    private String username,profile,userid,did,war_id,notif_time;

    public Notif() {
    }

    public Notif(String username, String profile, String userid, String did, String war_id, String notif_time) {
        this.username = username;
        this.profile = profile;
        this.userid = userid;
        this.did = did;
        this.war_id = war_id;
        this.notif_time = notif_time;
    }

    public String getUsername() {
        return username;
    }

    public String getUserid() {
        return userid;
    }

    public String getWar_id() {
        return war_id;
    }

    public String getDid() {
        return did;
    }

    public String getProfile() {
        return profile;
    }

    public String getNotif_time() {
        return notif_time;
    }
}
