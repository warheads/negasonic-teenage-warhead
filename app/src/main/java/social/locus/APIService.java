package social.locus;
import social.locus.Notifications.MyResponse;
import social.locus.Notifications.Sender;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface APIService {

        @Headers(

                {
                        "Content-Type:application/json",
                        "Authorization:key=AAAA5E_RE2s:APA91bE5XKRP9_5UIsO-2z9KRDw4qZDw-_Ulr_ZJMo6fwvHKScxrnVD6FDa0rwh4Xyz0J9V7VZMvpf8dTQnDPu6ZEwLeUnedPwz2xiMI_VyC9cpRn3KkS4r8mt0dizdtcEu9fg6ihvC0"
                }

        )

    @POST("fcm/send")
    Call<MyResponse> sendNotification(@Body Sender body);
}
